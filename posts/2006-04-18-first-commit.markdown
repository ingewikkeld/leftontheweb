---
layout: post
title: "First commit! "
date: 2006-04-18 15:51
comments: true
categories: 
  - technology 
---
Well! This evening I made my first commit to the Zend Framework subversion repository. I had already checked out the dutch translation of the documentation before the weekend, so my first commit immediately added quite a bit of changes into the repository. I hope people will appreciate my work. 

At first, I read through the already existing translations, both to get up to speed with the way things are translated, and by the request of the current translators. I fixed some typo's and changed some of the translations to work better or be more proper dutch. This evening, after committing my first batch, I also started work on translating from the original english documentation. Sometimes it's very hard to get a good translation for a word, especially for technical terms. It's very hard to decide whether or not to translate such a term into dutch, or to leave it in english because people will understand that better than a translation. I think so far I managed quite fine. Let's see what this will bring.

In the meantime, release 0.1.3 of the Zend Framework is also available. I've downloaded it to play with it, already. Now I need to find a good balance between translating the documentation, and actually coding with the framework itself.
