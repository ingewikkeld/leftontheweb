---
layout: post
title: "new Electronic Music World design and CMS"
date: 2006-04-23 7:19
comments: true
categories: 
  - technology 
---
I've just introduced the new design and CMS (textpattern) over at my electronic music website <a href="http://www.electronicmusicworld.com/">Electronic Music World</a>. Hopefully, this will make it a bit easier for me to update the site. Until today, it was running a 5 year old custom written CMS which was very much outdated. Because I'm working on some other private projects, I didn't feel like completely re-writing the Electronic Music World code. So instead, I wrote a converter for my content to start working with Textpattern.

I'm happy with it and hope this will make it easier for me to add new content to the site. I've been slacking on the content side of Electronic Music World lately because of some technical difficulties with the site. This should solve that problem :)

Now, another big thing to happen to Electronic Music World: Move the site to DreamHost. But that's for another day. This is enough for today :)
