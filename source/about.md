---
layout: default
title: About

---
# About

Hi, my name is Stefan Koopmanschap. Between having a family life, a healthy love for doing radio and attending conferences and usergroup
meetings, I try to run a company called [Ingewikkeld](http://php.ingewikkeld.net/) and organize [WeCamp](https://weca.mp/). I also host a podcast: [By The Campfire](https://bythecampfire.net/).
I've worked with PHP and several of its open source products since the late 90s, and I still like it for getting the job done.

## Support

While I publish all of the content on this site for free, you can of course support me. You can do that by sending
[a single donation](https://www.paypal.me/skoopmanschap).
