---
layout: post
title: "BitTorrent"
date: 2005-07-26 7:11
comments: true
categories: 
  - music 
  - bittorrent 
---
Don&#39;t you love BitTorrent? I just got myself 20 cd&#39;s worth of material by The Beatles. Most of these I already own on vinyl, but I was too lazy to actually do the whole digitalization myself.  Some great material that I didn&#39;t yet have though, such as 2 cd&#39;s of live recordings at BBC. Nice!
