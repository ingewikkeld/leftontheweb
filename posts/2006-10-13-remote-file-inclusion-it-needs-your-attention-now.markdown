---
layout: post
title: "Remote File Inclusion: It needs your attention NOW!"
date: 2006-10-13 0:57
comments: true
categories: 
  - technology 
---
I just read on <a href="http://www.phpdeveloper.org/news/6489">PHPDeveloper</a> that there is a new security problem that needs urgent attention of any php developer. It's <acronym title="Remote File Inclusion">RFI</acronym>, a way for evil crackers to run their code of choice on your server, exposing such information as passwords, or even enabling them to get shell access to your system and maybe become root.

<a href="http://lwn.net/SubscriberLink/203904/c450f7af16f34584/">LWN</a> has a good article describing the problem, offering simple solutions that every developer should already be using but sadly not everyone actually does. They also link to the code that crackers are actually using, as taken from their logs where attempts to exploit the RFI vulnerability are being seen at a rate of some 1 attempt per second. 
