---
layout: post
title: "Speaking at the International PHP Conference"
date: 2008-08-28 19:52
comments: true
categories: 
  - IPC 
  - germany 
  - conference 
  - refactoring 
  - php 
---
<p>I am quite excited about speaking at the International PHP Conference. It&#39;s one of the biggest events on PHP right now, and the line up of speakers is impressive to say the least. I am very much looking forward to meeting everyone there.</p><p>The session I will be doing at IPC is called &#39;The Power of Refactoring&#39;, and during this session I will support the idea of refactoring for speeding up your PHP development. It is still common that people think refactoring is doing the same thing twice, and that this means it takes more time to refactor than to write everything only once. I want to convince all visitors to my session that refactoring is actually a good idea, and might end up saving you time as well as improving the quality of your work.</p><p>I am currently scheduled for the thursday afternoon. I hope to see you there! </p>
