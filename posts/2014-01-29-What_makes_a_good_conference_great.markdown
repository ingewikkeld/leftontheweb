---
layout: post
title: "What makes a good conference great"
date: 2014-01-29 20:00
comments: true
categories: 
  - php 
  - community 
  - conference 
  - people
---
> TL;DR: The people and the atmosphere

Context
-------
First some context: In the past months, I've been lucky enough to have been at both the [PHPNW conference](http://conference.phpnw.org.uk/) in Manchester, the [TrueNorthPHP conference](http://www.truenorthphp.com/) in <del>Toronto</del>Mississuaga, and [PHPBenelux](http://conference.phpbenelux.eu/) in Antwerp. I've been to those conferences before and hope to be at them again in the future. 

Right after TrueNorthPHP ended, I [tweeted](https://twitter.com/skoop/status/399299887121965056):

> I’ve always said #phpnw is the best PHP conference in the world. #tnphp is a close second. #tnphp is the #phpnw of North America

Quickly thereafter I [made a promise](https://twitter.com/skoop/status/399301170146992128), so here it is. It took a while, but if I promise something, I need to keep that promise.

What makes a good conference?
-----------------------------
For a lot of people, the content makes the conference. And of course, the content is one of the most important things at a conference. You're there to learn, so you need to have some good content, otherwise it's all a bit useless. Well, not completely, but if the main purpose of a conference visit can't be fulfilled, what are you doing there? Additionally, it'll be very hard to get your boss to pay for a conference if there's no useful content.

I've been to a lot of conferences, and the content is usually excellent, but there is a lot of difference in how I experience a conference. This difference has hardly anything to do with the content. Looking at, for instance, Dutch PHP Conference, PHPBenelux, PHPNW, PHP[tek] etc, the content is similar. There's minor differences, but you'll see a lot of overlap in topics (and sometimes also in speakers). It is ultimately the people and with that the atmosphere of a conference that makes the difference for me between a good and a great conference. 

How to get the right people?
----------------------------
Getting the right people, or actually the right mix of people, is not easy. It is a mixture of a number of things:

  * Select the right speakers
  * Keep entry prices low
  * Promote in the right places
  * Create more than a conference
  
Funny enough, I've found that conferences with a big community-aspect to it have a much better feeling for this than commercial conferences with a marketing or profit aim. I think (but this is a guess) that this might be because the more community-involved conferences are run by more technical people, who know what technical people want.

### Select the right speakers ###
The speakers should aim to reflect the full audience that you're trying to reach. And no, I am not talking about gender, race, sexual preference or any of those things. They don't matter. The schedule should reflect the topics you want to cover and the levels of developers you're aiming for. If you're aiming for architect-level visitors, it is no use to put up an intro to Symfony2. If you're aiming for a conference where beginning developers and seniors/architects meet, make sure your schedule reflects this by putting up a mixture of all levels and topics.

### Keep entry prices low ###
Keeping entry prices low is important for getting people in. Some employers don't pay for the tickets of their employees, and self-financing some couple of hundred euro's is hard. Sponsors go a long way in financing conferences, ensuring you can keep the ticket prices low. This is also where community-conferences have an advantage: They don't need to make a profit. Their only agenda is to offer great content to as many people as possible. Commercial conferences of course can't really change that, it's just the nature of the conference. This does mean that community conferences are at a slight advantage here...

### Promote in the right places ###
If you're aiming for an enterprise conference, obviously you advertise in magazines aimed at CTO's and big companies. If you're focussing starters, there's different places to promote. Get those usergroups on board in the promotions, find the right websites and magazines, etc. Usually, finding some key community members will do a lot towards getting in the right people.

### Create more than a conference ###
Organizing a conference is good, but you should create much more than just a conference. It should be a full experience, including social events, a good treatment of speakers, delegates, sponsors and anyone involved. Or perhaps even better, as my friend [Michelangelo](http://www.dragonbe.com/) put it last weekend at PHPBenelux: "We don't want to create an experience, we want to create the Magic Kingdom." He compared PHPBenelux to a Disneyland Park, and he's spot on: From the moment you come to a conference to the moment you leave, you should be part of the conference, whether you're a speaker, delegate or sponsor. 

Creating the magic
------------------
There's more than enough to write about this, but I'll just give a few small pointers that I've seen happen at the earlier mentioned conferences, and that I've also tried to apply in the years that I've been part of the PHPBenelux and [PFCongres](http://www.pfcongres.com/) crew:

### Pick up and drop off speakers at the airport/trainstation ###
This is not hard, but it makes speakers feel really welcome and appreciated. It's that little effort you put in that means speakers don't have to worry about where they are going and how to get there. And while you're driving anyway, if you are aware of delegates also arriving and you have some room in your car, just take the delegate along. This makes the delegates also feel as an important part of the conference.

### Don't seperate speakers from delegates ###
I've been at several conferences where there was a completely seperated speaker room. While it is awesome to have a place to sit down, plug in your laptop and work for a bit, usually speakers also want to mingle with the crowd. Instead of creating a seperate speaker room, why not just create a place to hang out for everyone at the conference, with powersockets and some tables and chairs. Offering a seperate room for speakers to get some peace and quiet is not a bad idea, but don't make that room the only place where people can sit down and do some work.

### Create epic socials ###
A social event at a conference is a place where people want to have fun, chat, laugh etc. Make sure that people get just that out of a social. Make sure there's room for people to sit and chat, avoid loud music, and if possible, arrange some entertainment. Last weekend at PHPBenelux, there was a carnival in the sponsor area (every sponsor had something, for instance arcade games, coin pushers, air hockey, X-box). They even had bumper cars! Last year at php[tek], there was lots of Legos to play with. PFCongres had a gamenight. These are the things people enjoy at a conference. They are the difference between a good conference and an EPIC conference.

Concluding
----------
In short: Every conference is in essence the same: Good content with good speakers. I've not yet been to a single bad conference when it comes to content and speakers. The difference is made in all those little extra's. Spending a little effort on those extra things will make the difference.
