---
layout: post
title: "Open letter to Google"
date: 2005-02-09 2:34
comments: true
categories: 
  - leftontheweb 
---
<a href="http://www.i-marco.nl/">Marco van Hylckama Vlieg</a>, a.k.a. i-marco, has written an open letter to the people at <a href="http://www.google.com/">Google</a>. Topic of the open letter is his, and with him most bloggers', frustration about comment spam, trackback spam and referrer spam. In his <a href="http://www.i-marco.nl/weblog/archive/2005/02/08/open_letter_to_google">letter</a>, he makes some good points about comment spam and the importance of Google in this. Though the nofollow attribute is a nice addition, in the long run it won't make a difference, because by far not all weblogs will be secured with this tool.

I definately agree with marco's points, and I sincerely hope that the Google crew will respond to this open letter. I hope a discussion will be started here about how to stop comment spam, with the most important party in the whole comment spam, Google, openly joining this discussion. After all, Google *is* the main reason for comment spam to happen.
