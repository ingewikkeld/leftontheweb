---
layout: post
title: "Symfony and PHP 5.2.4 may be a nono"
date: 2007-09-24 4:56
comments: true
categories: 
  - symfony 
---
Last week, I automatically updated my PHP installation to the latest version dotdeb was offering me (which usually poses no problems). However, this time around I ended up with two problems:

* Most of my models started throwing errors: Error populating ''x'' object [wrapped: Unable to convert value at column ''x'' to timestamp: 0000-00-00] (or 0000-00-00 00:00:00)
* My PHP CLI broke. No more pear, no more symfony cli.

The first problem has been solved now, luckily, by following the instructions in <a href="http://symfoniac.wordpress.com/2007/09/13/warning-about-php-524-and-creole/">this article</a> (and extending them a bit, see the comments to the article). Since Creole isn't developed actively anymore, bugfixes like this will not be implemented into a new version. So you'll have to fix it yourself.

Now onto tracing the second problem: Why my CLI was killed.

<strong>Update:</strong> The problem for my CLI was the fact that I had the Suhosin patch installed. Not sure why exactly this became a problem (as I've always had that installed in the past without any problems) but I'm just glad I can now continue working again.
