---
layout: post
title: "New conferences for 2023"
date: 2023-05-18 16:00:00
comments: true
categories: 
  - php
  - conferences
  - ddd
social:
    highlight_image: /_posts/images/api_platform_con_stefan_koopmanschap.png
    summary: My schedule is beginning to fill up so I wanted to announce my speaking at DrupalJam in Utrecht, TechTuesday XXL in Tilburg and API Platform Con in Lille. 
    
---

Conferences are happening again and I'm really happy about that. And while it gives me quite a bit of FOMO to see the messages about PHP[tek] and PHPDay, and especially for EndPointCon (as I was supposed to speak there but had to cancel due to a nasty stomach flu) I'm happy to announce some new conferences where I'll be speaking.

## DrupalJam Utrecht

First off, on June 1 I'm heading to Utrecht for [DrupalJam](https://drupaljam.nl/program), where I'll be joining the ranks of speakers amongst such names as Arnoud Engelfriet, Rick Kuipers and many more. I'll be delivering my Domain-Driven Design: The Basics talk there. 

## TechTuesday XXL

Then on Tuesday June 13th I'll be doing that same talk a bit more in the south of the Netherlands in Tilburg during the [TechTuesday XXL](https://www.meetup.com/nl-NL/tilburg-tech-tuesdays/events/291614949/). The regular TechTuesday meetups are always fun, and this XXL edition will be even more fun with, among other people, Pauline Vos also be there. 

## API Platform Con

Then I'll be meeting Pauline again in Lille in september, as we're both joining the ranks of speaking at [API Platform Con](https://api-platform.com/con/2023/speakers). There I'll be doing my PHP's Kitchen Nightmares talk. Lots of fun will be had in Lille, I'm sure.

## See you there?

I'm really looking forward to speaking at these events. Will I see you there?