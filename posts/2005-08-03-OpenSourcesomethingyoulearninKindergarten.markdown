---
layout: post
title: "Open Source, something you learn in Kindergarten"
date: 2005-08-03 3:05
comments: true
categories: 
  - opensource 
---
<a href="http://www.gideonmarken.com/">Gideon Marken</a> is a very cool guy. He is the person behind the fabulous <a href="http://www.artistserver.com/">ArtistServer</a>, and a very experienced web developer. It was Gideon who made <a href="http://www.gideonmarken.com/index.cfm?a=9&amp;bdate=8/2/2005&amp;act=2">a weblog post yesterday</a> that found me intrigued. This was a way to look at Open Source that I&#39;d never before seen.  <blockquote>Those little 4-6yr old minds are always hearing about &#39;sharing,&#39; &#39;waiting in line,&#39; &#39;being polite,&#39; or being given a means to explore the world and create.   If we look at those concepts - we see a foundation for Open Source.  - there&#39;s the sharing - the acknowledgement of others - the consideration of many over one - the support and encouragement of creating</blockquote>  Doesn&#39;t he have a very valid point there? Somehow, when we turn into adults we seem to forget about these things we were taught in Kindergarten about sharing, respecting each other, working together. We become self-centered people only caring about our own carreer (well, with we I speak generally, there are also people who don&#39;t forget about that ;) )  Gideon predicts that people can fight Open Source, but they&#39;ll lose. Sooner or later, Open Source will prevail. I surely hope so, though I&#39;m not as convinced as Gideon. We still live in a capital-centered world. Everything evolves around money, where Open Source doesn&#39;t. I surely hope Gideon is right and will do my best to support the cause.
