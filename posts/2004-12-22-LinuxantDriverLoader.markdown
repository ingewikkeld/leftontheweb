---
layout: post
title: "Linuxant DriverLoader"
date: 2004-12-22 10:24
comments: true
categories: 
  - leftontheweb 
---
I will probably get scolded at from the real Linux people. But after ages of struggling with my wireless network setup, I've finally found a solid, stable way to keep my wireless network running. This solution came in the form of the payware software <a href="http://www.linuxant.com/">Linuxant DriverLoader</a>.

Now, I know that using their software does not give a good signal towards hardware manufacturers. And if I would've had time enough to spend on getting it to work, I would. But all I needed was a good, working wireless network. And I've got that, with little trouble, thanks to Linuxant. I don't care anymore than I have to pay $20 per computer. I don't care that it's not Open Source. I've got a working network, and thats the most important thing for me.
