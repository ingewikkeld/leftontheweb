---
layout: post
title: "New Support Team Leader"
date: 2005-06-01 4:41
comments: true
categories: 
  - leftontheweb 
---
Today, I stepped back as phpBB Support Team Leader. I've written a short announcement including some of the reasons <a href="http://www.phpbb.com/phpBB/viewtopic.php?t=294795">here</a>. I look back with a very positive feeling, and look forward with an even more positive feeling. I rest assured that Techie-Micheal will do a good job as the new Support Team Leader. And I'll still be a part of the Support Team, doing that which I like most: helping people with phpBB.
