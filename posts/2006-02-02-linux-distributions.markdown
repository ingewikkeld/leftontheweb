---
layout: post
title: "Linux distributions"
date: 2006-02-02 15:36
comments: true
categories: 
  - technology 
---
<a href="http://www.slackware.org/">Slackware</a> is a great linux distribution, but better for servers than for desktops in my humble opinion. At the moment, however, I'm running Slackware on my laptop. Though I like to tinker and play with config files etc. lately I've been wanting a slightly more solid, user friendly user experience on my laptop. For my server, I don't care about that, I hardly ever work directly on my server. However, for my laptop I want a bit more.

I used to think <a href="http://www.linspire.com/">Linspire</a> was a good idea, until I recently installed that on a desktop machine with pretty similar specs to my laptop. It was slow as hell. Though it definitely looks very userfriendly, it's just way to slow on slightly older machines.

So I've been looking around. My main demands for this new distribution are:

<li>good hardware support - I don't want to have to fool around with drivers for my intel centrino wireless card anymore</li><li>good graphical interface for configuring the system - hardware settings, wireless/network settings, maybe even samba and printing setup using a nice GUI would be good</li><li>I prefer KDE over Gnome for some reason</li><li>Obviously it should still care about saving resources. This is a laptop after all.</li><li>Preferably, my own partition-system should stay in tact, so I can just move all the data that I want to save to a single partition and only re-install my root partition.</li>

Now, I've been looking at two distributions: <a href="http://www.vectorlinux.com/">Vectorlinux</a> and <a href="http://www.kubuntu.org/">Kubuntu</a>. Vectorlinux is nice because its Slackware-based, and I already know Slackware. Possible downside is that it's so explicitly aimed on being small and fast that I'm afraid that makes it again less easy to configure (though their website actually claims it's easy to configure). Unfortunately, they have no Live CD, so I can't try before installing.

Kubuntu is more aimed at being a simple, graphical distribution. I know it's slightly slower, at least from my previous experience with Ubuntu, but it is mainly aimed at userfriendliness. 

Are there any distributions that I'm missing, that I should look at? Or any opinions on the above-mentioned distributions? I'm open to information here. :)
