---
layout: post
title: "Motivating people to learn"
date: 2016-01-04 13:35:00
comments: true
categories: 
  - php 
  - learning
  - continuouslearning
---

Nearly two months ago I [wrote a piece about Continuous Learning](https://dutchweballiance.nl/techblog/continuous-learning/) on the Dutch Web Alliance blog. In that article, I presented a lot of different ways of learning. All these ways of learning are interesting, but if you can't convince your developers to actually start learning, there is little use in having all these options. So in this article, I want to look at how to to motivate people to learn.

## The core of learning

There's two things that are extremely important in learning new things:

* They want to learn
* You show you want them to learn

Both things are equally important, but the first can be influenced by... you guessed it: the latter.

### They want to learn

Without motivation from your team members, they won't learn anything. You can send them to a training and they'll go, but the chance of them actually picking up on something is small. This sometimes has to do with the form of the training (classroom training can be good, but often tends to be boring), and also with the topic. Sending your developer to a training on a framework when they're much more interested in understanding devops, for instance, will definitely not motivate them. They need to want to learn.

### You show you want them to learn

As I mentioned before, if your developer has a hard time getting motivated to learn, it really helps if you show them that you want them to learn. Not by forcing things on them, but by starting a conversation about education and the topics they want to learn about.

#### The Personal Development Plan

A good first step is to create a Personal Development Plan. This doesn't have to be a huge document, and it doesn't have to be very formal. It should, however, document a plan for the personal development of your team member. Document where the developer currently is, where they want to be and how much time they should take to get there. It is nice if there are some steps in the plan on how to get there. These steps could include education, but also other forms of learning (see [Continuous Learning](https://dutchweballiance.nl/techblog/continuous-learning/)). 

Keep in mind that this document is only there to guide your team member, the details in this document are not set in stone. Along the way they might find out that perhaps the goals was actually not right, or perhaps the steps were incomplete or incorrect. Things may change along the way, so it is a good idea to have one or more moments of evaluation along the way.

#### Invest money

Another very good way to show your developer that you want them to learn is to invest in the learning. Most companies these days have a training budget for every team member that they can use, but a lot of companies I've encountered use this mostly for traditional classroom training sessions or workshops. Keep the conversation open as to what they can spend the budget on: While some people may learn best using a classroom training or workshop, others may learn better by doing experiments. Last [WeCamp](http://weca.mp/) one group of developers started playing with an Arduino and developing something that could talk to the Arduino. Buy your developer an Arduino (or other necessary hardware). Another example: Offering to pay for travel to a usergroup is a small financial investment for your company, but can be the deciding factor between going and not going to said usergroup.

Some people like to learn more about speaking (or knowledge transfer in general, where speaking is just another way to transfer knowledge). Offer them to use the budget to cover any costs for speaking at conferences that the conference doesn't cover. Sure, this is not directly supporting the learning, but those indirect costs can deter someone from making that step into speaking. 

The above is just an example, but long story short: Don't limit the budget to traditional education, but keep the conversation open and look at what is needed for your team member to learn.

#### Invest time

Believe it or not, but [developers are just like humans](https://joind.in/search?keyword=developers+are+just+like+humans). Although a lot of developers tend to spend a lot of free time on programming and hobby projects, this does not mean they automatically want to use their free time to learn new stuff for work. Offer your developer to attend a usergroup, conference or other event during work hours (or allow them to get the free time they sacrifice back in vacation days). This could well convince your developers to spend time on learning. Since they are learning for their work, it is only fair to let that learning *be* their work.

If you have the opportunity, start an 80/20 program. Have your team work 80% of their time for the customer, and allow them to have the other 20% for learning, research&development, contributions to the community and open source. From the moment I hired my [first developer](http://twitter.com/jelrikvh) I implemented this. Don't just let them do everything they want, but discuss possible projects. I can say with a lot of pride that the 20% of both [Mike](http://twitter.com/mvriel) and Jelrik have contributed to:

* The further development of [PHPDocumentor](http://phpdoc.org/)
* The founding of [PHPAmersfoort](http://phpamersfoort.nl)
* The organization of [WeCamp](http://weca.mp)

No, the above is not what Mike and Jelrik learned from it, but working on open source and organizing events has taught them a lot. Not just about technology and programming, but also about planning, communication, even marketing. The 20% time can get your organization a lot more than just education: Our company is listed as sponsor on the phpDocumentor website, mentioned every meetup of PHPAmersfoort as sponsor, and WeCamp is the most epic things ever. 

### Still no motivation?

Over the years I've sometimes also encountered developers that simply did not want to learn. The conversation is still important, but eventually, if that conversation fails, you should think about why you hired the developer and whether the developer is indeed able to fulfill the role you need from them. If they do: don't force them to learn but just let them to their job. If they don't, it is perhaps time to consider parting ways.

### Enable

I love how [Rafael Dohms](http://twitter.com/rdohms) dubbed the term "enablers" instead of "organizers" for the organization of [AmsterdamPHP](http://php.amsterdam), and I think "we" (whether you are the team lead, tech lead, CTO, manager or whatever you want to call yourself) should be enablers to our developers, not just managers. We should always have an open conversation on the ways people learn best, and support them in that. I am pretty sure that no organization can offer everything that a developer needs, but the best way to determine how and what people should learn is by talking to them. And by keeping that conversation going, motivation usually should not be a problem.