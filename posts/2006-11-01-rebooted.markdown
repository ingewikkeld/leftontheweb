---
layout: post
title: "Rebooted!"
date: 2006-11-01 12:06
comments: true
categories: 
  - leftontheweb
---
Yes, the site is rebooted. Slighty later than planned, but you can't have it all. The new design is less standard than the previous one. I did start out with <a href="http://textpattern.org/templates/540/che-guevara">a standard template</a>, but I started altering and customizing it. You'll still recognize the basics, but the colors and some other details have been changed to make it more to my liking.

I hope you like it as well.
