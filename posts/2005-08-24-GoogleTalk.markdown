---
layout: post
title: "Google Talk"
date: 2005-08-24 2:26
comments: true
categories: 
  - google 
  - googletalk 
---
I&#39;m sure it&#39;s going to be the talk of the web for a short while, but <a rel="tag" href="http://www.google.com/">Google</a> indeed is starting their own <a href="http://www.google.com/talk/">Instant Messenger service</a>. They&#39;re using the <a rel="tag" href="http://www.jabber.org">Jabber</a> protocol, which technically means people with Jabber-enabled clients should be able to connect. Thanks to <a href="http://shiflett.org/archive/139">Chris Shiflett</a>, I just did. He has a nice screen shot of the account configuration window for <a rel="tag" href="http://gaim.sf.net/">Gaim</a>, which you can easily use to configure your own Gaim for GoogleTalk usage. Nice! And Google <a href="http://www.google.com/talk/otherclients.html">officially allows it as well</a>. I&#39;m connected using stefan.koopmanschap at [googlee-mailservice] . com ... if you dont have a gmail account yet but would like one, to start using <a rel="tag" href="http://talk.google.com/">GoogleTalk</a>, just let me know ... I have plenty of invites left.
