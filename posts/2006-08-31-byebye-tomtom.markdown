---
layout: post
title: "ByeBye TomTom"
date: 2006-08-31 12:17
comments: true
categories: 
  - personal 
---
In just over a month time, after working for TomTom for exactly one year, I will be leaving the company. It's not that I don't like it there, on the contrary, I love it there. The work is great, the people are great. However, being away from home for about 12 hours each day is no fun when you have a little son of 1.5 years old. I want to spend more time with him.

I found the perfect replacement though, in <a href="http://www.dop.nu/">Dutch Open Projects</a>. A relatively small company in a the town of Leusden, about 15-20 minutes from home. I'll be working there a lot with open source projects such as <a href="http://www.joomla.org/">Joomla</a>, <a href="http://www.sql-ledger.org">SQL-Ledger</a> and <a href="http://www.zimbra.com">Zimbra</a>, integrating several systems into one for clients, working on extending the functionality, and on developing completely new systems. I will also be working on the move from PHP4 to PHP5. And there's other stuff as well. A really interesting and inspiring place to be.

It's a sad thing to say goodbye to TomTom. There are quite a few very talented and inspiring people there. Only recently, I got to work with one of the founders of the company, <a href="http://en.wikipedia.org/wiki/Peter-Frans_Pauwels">Peter-Frans Pauwels</a>. He is awesome to work with. Refreshing insights, and an inspiring man. Then there is the technical lead of our team, Ivo Kendra, who is a brilliant coder and also an inspiration. Ever since I started at TomTom, he's been an example to me. And recently, in the Java team, Wilfred Springer joined. He is the great, enthousiastic talker, who can explain anything and you'll understand it. It is truely a shame that I will not be learning to do Java at TomTom anymore.

Of course, I really enjoy working with all my other colleagues, but these are the guys that really stand out to me at TomTom. I can only hope to one day be anything like them. The time away from home is a reason for me to move on though, and I do think that with Dutch Open Projects I have found a good place to move on, to develop myself even further.
