---
layout: post
title: "Excluding generated files in your project"
date: 2019-10-03 14:10:00
comments: true
categories: 
  - php
  - docker
  - docker-compose
  - volumes
social:
    summary: I kept getting errors with some generated files in my project. Turns out the solution was simple: Create a volume within a volume    
---

So here's one of those things that I need to document, or I'll forget. One of those blogposts that I'll find through a search engine in a year ;)

A project I'm working on is using Symfony with Encore, and while running Encore I kept getting some weird errors that javascript couldn't find a file that was actually there. After some debugging I found out that this was related to the fact that the generated file was located in the volume that ensured my local files were available in the container. 

The solution was actually pretty simple. Create an unmapped volume inside the mapped volume:

```
            - ../project:/var/www/html/project:cached
            - /var/www/html/project/web/js
```

Now when I ran Encore, I had no more errors and everything worked as it should.