---
layout: post
title: "Not that smart"
date: 2004-12-18 8:43
comments: true
categories: 
  - leftontheweb 
---
Now, Bugtraq contains a lot of people that are not that smart. Just for the record, there are also a lot of people there that *are* really smart though ;)

Anyway, I just got pointed to <a href="http://www.securityfocus.com/archive/1/384579">this vulnerability</a> that was recently posted. It seems that in phpBB <strong>1.4.4</strong>, there is a vulnerability in the image bbcode.

<h1 style="color:black">OH NO!</h1>

To explain my ... uhm ... panicked response for those not in the know: phpBB 1.4.4 is a very very very old version of phpBB, and has been unsupported since the release of phpBB 2.0. That was quite a while ago. phpBB 2.2 is already on it's way. Yes, it's THAT long ago.
