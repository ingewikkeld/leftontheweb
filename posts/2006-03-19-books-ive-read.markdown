---
layout: post
title: "Books I've read"
date: 2006-03-19 4:43
comments: true
categories: 
  - books 
---
This weblog is turning more and more into a booklog it seems ;) I'll try to write a bit more about technology and such in the near future, ok?

Anyway, for now, let's take the list of <a href="http://www.bookcrossing.com/mostregistered">50 most registered books on BookCrossing</a> and see what I've read of those:

1. Angels & Demons by Dan Brown

Though I've read them in the wrong order (first Da Vinci Code, then Angels & Demons) I've read them both. I got the special illustrated version of this book, my sister and her boyfriend gave it to me :)

2. The Da Vinci Code by Dan Brown

see #1 ;)

5. The Pelican Brief by John Grisham

A great Grisham book!

7. The Firm by John Grisham

Another great Grisham novel

20. The Client by John Grisham

I used to be a big Grisham fan ;)

25. The Rainmaker by John Grisham

Another one. Grisham seems to be the most-registered author!

35. The Partner by John Grisham

See? And I've skipped a few already that I haven't read!

That's not too much. I guess I don't read the most popular books for BookCrossing. 
