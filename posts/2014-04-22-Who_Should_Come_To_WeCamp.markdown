---
layout: post
title: "WeCamp: Everybody is welcome!"
date: 2014-04-22 15:00
comments: true
categories: 
  - php 
  - wecamp
---
Last month we announced [WeCamp](http://weca.mp/), an event where developers can improve their skills not just by listening to people, but also by actually doing things. Response has been amazing: Many people love the idea, we've already sold quite a few tickets, and we've even got [Magma giving a ticket as a price](http://www.magmadigital.co.uk/win-ticket-wecamp-2014/) (**At the time of publishing of this blogpost there's only a couple of hours left, so if you want to win, be fast!**). Since announcing the event, I've talked to several different people who had the same doubt/question about the event, so I thought I'd clarify things a bit more.

## Who should come to WeCamp?

Everyone! We don't want to exclude anyone and we actually think everyone would be a huge asset to the event. I've had people say "I'm not experienced enough in development to attend", to which I could only respond "untrue!". The idea of WeCamp is that you don't just train your technical skills but also your social skills. For a less experienced developer, the practical experience of working in a team with more experienced developers is priceless. In that same team, however, the most experienced developer gets the additional experience of helping a less experienced developer with the work, gaining a lot of experience and knowledge on how you can help others. This social aspect is easily just as important as gaining technical skills. So both developers come out of this gaining experience.

## But I'm a frontend developer...

So? If you're a frontend developer, have a look at the many websites created by pure backend developers... do they look good? Are they accessible? Built according to W3C standards? Most probably not. You're needed! While we have a focus on PHP, you'll learn to work with PHP developers: How to communicate with them, how they look at frontend work, what tools are available in the PHP ecosystem for frontend. And I know this may sound scary, but you'd perhaps even get some PHP skills as well.

Additionally: WeCamp is not just about technology. We'll go through all stages of a project, starting at thinking of a good idea through planning to development, deployment and perhaps even marketing! There's more than enough to learn throughout this whole process.

## But I wonder about...

If you have any more doubts or questions, please let us know! This is the first time we're organizing WeCamp so we're still very much looking for what works best with a lot of things. If there's any unclarity, or you have comments for improvements, let us know! You can always tweet at us [@wecamp14](http://twitter.com/wecamp14) but you can also shoot us an email: [we-can@weca.mp](mailto:we-can@weca.mp). We're also here to learn, so please help us do just that!
