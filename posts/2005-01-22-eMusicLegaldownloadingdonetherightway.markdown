---
layout: post
title: "eMusic - Legal downloading done the right way"
date: 2005-01-22 4:26
comments: true
categories: 
  - leftontheweb 
---
I've <a href="http://www.electronicmusicworld.com/article.php?id=14&amp;archive=yes">written before</a> about downloading music. Legal downloads are hard to get. Most downloads require the downloading computer to support <acronym title="Digital Rights Management">DRM</acronym> and the only technology that currently supports it enough to be used is Microsoft's Windows Media Player. This poses a big problem for people not using Microsoft's operating system, because both Mac and Linux/Unix can't run this.

But, finally, I have found a system that can be used for legal downloading of a lot of music. No, it won't have most of the music that is released by the "Big 5". Instead, it focusses on providing music by the smaller labels, by those independent labels and artists that deserve our support more than those that get plugged by the Big 5. This system is called <a href="http://www.emusic.com/">eMusic</a>.

Instead of using DRM'ed Windows Media files, you can download high quality MP3 files that can be played on all computers, on all portable music players, everywhere. Quite similar to <a href="http://www.bleep.com/">Bleep</a> but with a slightly different payment system and a much bigger catalog. With eMusic, you pay a monthly fee, and in return you get a certain number of downloads. All tracks you download take 1 away from the download. And you don't have to "pay" twice for the same track: If your download fails, your connection gets lost or whatever, and you download the same track again, that will not take another download credit from your account.

And don't think there is only unknown music on eMusic. They've been able to convince quite a few smaller labels to offer their music through eMusic. Think <a href="http://www.emusic.com/artist/10564/10564324.html">Fleetwood Mac</a>, <a href="http://www.emusic.com/artist/10559/10559600.html">Tom Waits</a>, <a href="http://www.emusic.com/artist/11499/11499834.html">The Future Sound Of London</a>, <a href="http://www.emusic.com/artist/10559/10559693.html">Frank Zappa</a>.

Yes, I really like this system, and I think more people should. It's the perfect legal downloading. No annoying technologies that only work on certain operating systems. And a lot of great music that need our support.

And the best thing is, if you sign up for a free trial account, you'll get your first 50 MP3 download's for free! If you consider signing up, please contact me, because if I refer you to the site and you end up signing up, I'll get 10 free tracks as well :)

This article was also posted on <a href="http://www.electronicmusicworld.com/article.php?id=28">Electronic Music World</a>
