---
layout: post
title: "Stopping my SSH connections from stalling"
date: 2011-03-14 14:37
comments: true
categories: 
  - linux 
  - ssh 
  - timeout 
---
<p>A quick tweet to ServerGrove triggered me to receive <a href="http://ocaoimh.ie/how-to-fix-ssh-timeout-problems/">this link</a> from them with a way to possibly prevent this. I took the server-side version, and added to my <em>/etc/ssh/sshd_config</em>:</p>

<blockquote>ClientAliveInterval 60</blockquote>

<p>I restarted my ssh daemon and connected again. I then went to pick up my son with the connection still open, and when I came back... no stalling anymore. I could still use the connection! So this seems to have solved this problem for me. Hopefully, it will for you as well. Yay for <a href="http://servergrove.com/">ServerGrove</a>!</p>
