---
layout: post
title: Fun in Mannheim!
date: 2024-04-05 10:15:00
comments: true
categories: 
  - php
  - conferences
  - usergroup
  - mannheim
  - sylius
  - syliusdays
  - open source
  - DDD
social:
    highlight_image: /_posts/images/mannheim.png
    summary: I'm looking forward to my trip to Mannheim later this month. I'll be speaking at the local usergroup and at SyliusDays.
    
---

Mannheim is a great place to be. I've been there to keynote the [unKonf](https://unkonf.de/en), I've been there to speak at the usergroup, I've even been there to do merch for [Amanda Palmer](https://amandapalmer.net) at the [Maifeld Derby festival](https://www.maifeld-derby.de). I love going to Mannheim. It's a beautiful, welcoming city with cool people.

As such I am excited to be going there again this month. I'll be there from April 17 to April 19, doing two presentations. First, on April 17th I'll be speaking at the [PHP User Group Metropolregion Rhein-Neckar](https://www.meetup.com/phpug-rhein-neckar/events/298184726/). I'll be doing my DDD talk there. 

Then, on Friday the 19th I'm really excited to be part of another Sylius event: [Sylius Days](https://sylius.com/sylius-days/). I love Sylius and I'm happy to do my talk about doing open source contributions as a business there. With people like Oliver Kossin (who is also speaking at the usergroup on Wednesday), Stephan Hochdoerfer and Łukasz Chruściel there it promises to be an amazing event. 

The great thing is: Both events are free to attend! So [RSVP for the usergroup](https://www.meetup.com/phpug-rhein-neckar/events/298184726) and [Get your free ticket for SyliusDays](https://store.sylius.com/products/syliusdays-24-ticket). I'll see you there!