---
layout: post
title: "American Psycho"
date: 2006-01-28 4:03
comments: true
categories: 
  - books 
---
I guess I was in the mood for reading books-turned-into-movies, because after reading The Bourne Identity I started on American Psycho. Now this, my friends, is one sick book.

And when I mean sick, I mean sick. There were actual passages, for instance the one where Patrick Bateman kills the dog, where I actually started feeling sick.

The book is what the text on the back of the book promises though: A book about 
