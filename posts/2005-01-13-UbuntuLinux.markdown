---
layout: post
title: "Ubuntu Linux"
date: 2005-01-13 12:57
comments: true
categories: 
  - leftontheweb 
---
<a href="http://www.ubuntulinux.org/">Ubuntu Linux</a>. A relatively new distribution in the Linux world, with a Gnome desktop. Being the lover of editting configuration files manually and such, I didn't think it was for me. When my wife wanted linux though, and she disliked Slackware because it wasn't intuitive enough for a new Linux user (can't blame her), Ubuntu was my first thought. I'd received a few installation cd's (you can order those for free!) and thought I'd give it a try.

Wow. I never thought I'd be impressed this much by a graphically oriented distribution. I mean, I also use X, but don't use Gnome or KDE because I don't need all the eyecandy, but this is good. Much better than, for instance, the default installation of Gnome that you get with Slackware, which was mainly what I was basing my opinion of Gnome on.

The menu's are very intuitive, and there's a lot of graphical configuration tools. Perfect for someone who has just left Windows for Linux. Very, very impressive. And maybe even more impressive is the great support. On the <a href="http://www.ubuntuforums.org/index.php">Ubuntu Forums</a> you can already find answers to a lot of questions, simply by searching around a bit. I bumped into some minor issues, but had quickly found all the answers I needed.

Yes, Ubuntu is definately one of the best distributions, especially for people new to linux. There might be some minor flaws, but in general it definately works well and looks good.
