---
layout: post
title: "Drupal just got a whole lot more compatible"
date: 2011-10-25 7:38
comments: true
categories: 
  - php 
  - drupal 
  - Symfony2 
  - symfony 
  - phpBB 
  - opensource 
  - integration 
  - components 
  - PSR-0
---
<p>Drupal is, independant of how you measure it, the leading PHP-based CMS. Whether you look at the amount of websites using it or the size of it, Drupal rocks it all. One thing that has kept me, and I guess many others, so far from seriously working with Drupal is the lack of object orientation (most of Drupal is (or was) procedural code). And while this is simply another approach, and one that has worked well for Drupal all these years, it does keep many people from seriously adopting the software. With Drupal moving now towards reusing existing components and integrating those, just like phpBB last year, Drupal now is opening up to a new community.</p> 

<p>One thing why I'm so excited about this is that on a regular basis I run into projects which have a part that is typically well-suited for a standard CMS, and then another part that (in my view) would be better suited being built as custom code on top of a framework such as Symfony2. So far, I had to make a choice between either one approach because integrating it would take me a lot of time. Now, with Drupal adopting some Symfony2 components, this integration is looking to become a whole lot easier. Given that HttpFoundation is now in Drupal, I'm sure true session integration, for instance, is not that far away anymore. And with that, easily sending requests back and forth between Drupal and custom Symfony2 code will become a whole lot easier.</p>

<p>The adoption of PSR-0, the naming/autoloading standard using by (amongst others) Symfony2 for the core of Drupal also makes other things possible. It will also be a lot easier to start using other PSR-0 libraries into Drupal and Drupal projects. Specifically, I'm thinking about Zend Framework here, which has a lot of very useful components as well. And not just for core, also for projects. Having PSR-0 integrated into Drupal just made development for any Drupal developer a whole lot easier.</p>

<p>I guess the rest of <a href="https://skoop.dev/message/phpBB_and_Symfony_Combining_Communities">my phpBB/Symfony2 blogpost</a> also applies to this situation: There's new opportunities rising up from this, making developers that have worked on Symfony2 available to the Drupal project, and skilled Drupal developers available to the Symfony2 community. Drupal will be building on top of a strong, tested base while for Symfony2, this is another great use case of how Symfony2 and it's components can be used. But also: Both communities should embrace eachother, and start working together to make this integration a success, and perhaps (this is at least my hope) allow Drupal to integrate even more Symfony2 components to make it even easier to combine the two products in a project.</p>

<p>Welcome Drupal community to the Symfony2 community. I am excited to become part of yours!</p>
