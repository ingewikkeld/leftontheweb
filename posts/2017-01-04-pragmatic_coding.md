---
layout: post
title: "Pragmatic coding"
date: 2017-01-04 13:30:00
comments: true
categories: 
  - php 
  - pragmatic
  - simplicity
  - minimalism
  
---

> Can you write me a simple script that fetches some information from an RSS feed and displays the titles? Like, just write me that script in a couple of minutes. I don't care about tests, quality, etc. Just get me the information, quickly.

I'm curious how you would handle this request. A couple of years ago I was asked a similar question. Not in a professional role, but for a hobby project that has nothing to do with PHP. There was a website, a simple website, and it needed some information displayed. Nothing fancy right? I can do that. 

But as I sat down to code this little script, I came to the conclusion that it took me way too much effort to start thinking in a pragmatic, simplistic way. I started thinking...

> I'll want to use a library

and...

> If I want to use a library, I'll need to use composer to install it

and...

> If I install that library then it would be useful to add some other components, to easily manage configuration, dependency injection, etc.

## Stop!

Wait, what is the actual question here? There is a pre-existing website, a codebase that I have nothing to do with so far. It is not built using any of the standards I use in my dayjob, and really, I should not care too much about it. It is a simple website, for a hobby. They ask me to simply add something simple. Should I really want to introduce libraries and components, tools like Composer? 

## Hammertime!

As it turns out, I seemed to have forgotten how to do simple scripting. I was so used to installing a full-stack framework, adding all kinds of libraries, that the simple task of writing a script that would just do one simple thing was getting too hard for me. Instead of just getting a hammer to get a nail in the wall, I wanted to rebuild the whole wall because it was not initially built using the same type of wood I was used to. Do I really want to take on a huge reconstruction project in my house just to hang a painting? Why is it so hard to open my toolbox and grab that hammer?

## Back to the basics

Since that moment I've tried to challenge myself every once in a while by writing simple scripts instead of full applications. For instance to automate simple tasks on my laptop (see: automation in [this blogpost](https://skoop.dev/blog/2009/06/18/Book_Review_The_Productive_Programmer/)). Other challenges were simply to write something I had no use for. Just to get back to the basics of writing scripts instead of applications. 

It wasn't easy, but I've learned a couple of nice lessons.

First of all, PHP as a language actually has a lot of stuff that we don't really need userland code for. Yes, the components and frameworks will make the task easier, but there are so many functions in PHP that can help you achieve a task that you don't really need a library for. RSS, for instance, can easily be parsed using [SimpleXML](http://php.net/simplexml). And I really don't need to have domain objects or entities per RSS item. Need a quick database connection to get or save some information. There really is no need for Doctrine or Propel, [PDO](http://php.net/pdo) will do what you want. And that's just two examples. There's [so much more](http://php.net/manual/en/funcref.php).

Second: Bootstrapping a project using Composer and frameworks works well for big projects, but if all you need to do is write a simple script, the overhead is just way too big. I had somehow forgotten that, or was just ignoring the option of just starting. I'm sure this had to do with the fact that I have been mostly working on big projects in the past 10+ years, but still... why did I forget?

And last but not least: Being pragmatic can be really useful. Sure, we all want to write the absolute best and most beautiful code ever, but sometimes you really shouldn't care. Sometimes you just need to be pragmatic, make it work, and forget about it.

## Build from the basics

One of the "simple scripts" I wrote a couple of years ago ended up becoming bigger. I had written it quickly, just to make it work, but as it grew, I decided to refactor it a bit, add a bit more structure, and introduce some libraries along the way. 

An important lesson I learned from this approach: We don't need the full-stack for everything. Components can by itself be really useful. Not every project I do automatically gets a full-stack framework installation these days. You can easily build your own framework for your project using components. I even have [a talk about that](https://nomadphp.com/nomadphp-2017-01-eu/) these days, and I plan to write more specifically about that at some point. 

## Challenge yourself

So let's go back to the original question:

> Can you write me a simple script that fetches some information from an RSS feed and displays the titles? Like, just write me that script in a couple of minutes. I don't care about tests, quality, etc. Just get me the information, quickly.

I'd say: go for it! Challenge yourself to do this, without libraries, in an hour. You can do it.