---
layout: post
title: "Paradise here I come"
date: 2006-04-06 18:27
comments: true
categories: 
  - music 
---
I only have to survive <strike>tomorrow</strike> today at work and a free saturday. And then it's <a href="https://skoop.dev/article/195/a-dream-come-true">time</a>!  I'm very excited about the concert, having seen Live in Paradiso in 1997, which was a very intense experience for me.

But <strike>tomorrow</strike> this evening we have an apetizer already! Ed's brother Adam Kowalczyk will be playing in Utrecht in Stairway to Heaven! Nice! For those that don't know, aside from playing in Live, Adam <a href="http://www.adamandtheweight.com/">has his own band</a> making quite nice music.

Yeah, it's gonna be a good weekend.
