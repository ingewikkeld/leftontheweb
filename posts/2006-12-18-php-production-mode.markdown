---
layout: post
title: "PHP Production Mode"
date: 2006-12-18 15:06
comments: true
categories: 
  - technology 
---
<a href="http://php100.wordpress.com/2006/12/17/production-mode/">PHP 10.0</a> posted about an idea for a php.ini configuration option that would toggle if PHP is running in "production mode". This would then automatically set some settings, such as disabling display_errors, disallowing phpinfo(), remove or strip expose_php, and ensure not-bad values of memory_limit and max_execution_time. Yeah, nice idea, but I don't think it will work.

First of all, as I also commented on the article, if someone would configure memory_limit and max_execution_time to 0 (unlimited) ... how would you decide what the correct value was? Or would you simply stop all PHP execution until they fix the configuration? Both don't sound like a good option.

But also, the definition of "production mode" is probably different. A lot of "experts" would want control over their configuration even in production mode. Of course, they could just set the configuration option to "Off", but that makes the semantics of your php.ini incorrect.

I'd rather keep the responsibility of the configuration of php with the server admin. And yes, there will be badly configured servers, and these servers may even be abused to bother other people, but I'd rather have it this way than restrict and confuse in an attempt to "help".
