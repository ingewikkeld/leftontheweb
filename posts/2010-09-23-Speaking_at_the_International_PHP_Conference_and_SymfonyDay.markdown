---
layout: post
title: "Speaking at the International PHP Conference and SymfonyDay"
date: 2010-09-23 18:35
comments: true
categories: 
  - symfony 
  - php 
  - sfdaycgn 
  - ipc 
  - ipc10 
  - documentation 
  - zendframework 
  - conferences 
---
<p>SymfonyDay is becoming a nice traditional, even though it's only going into it's second year this year. With an <a href="http://www.symfonyday.com/en/program.html">impressive schedule</a> it's going to be an information-filled day for the attendees. Just like last year, I'll be doing a workshop this year. This unfortunately means I will be missing the main conference track again which is a shame. For those interested in getting a ticket: <a href="http://www.phpdeveloper.org/news/15178">Get yours with a discount now!</a></p>

<p>Then I have half a day at home only to leave for Mainz again on the 11th for International PHP Conference, who also have an <a href="http://entwickler.com/konferenzen/planer/ipc_webtech_2010_zeitplaner.html">awesome schedule</a>. Slightly unexpected but nevertheless very cool, today I have been added to the schedule with two of my talks: "Would you like docs with that?" will be on documentation: Why should you document? How should you document? And what tools are available for documentation? This and more gets answered.</p>

<p>The documentation talk is on Wednesday, but my first speaking slot is on Tuesday. On Tuesday I will be speaking about integrating symfony and Zend Framework. I will show how easy it is NOT to restrict to a single framework, and explain why you should definitely not do so.</p>

<p>If you're going to either one of those conferences (or maybe to both?) I will see you there! Come and say hi!</p>
