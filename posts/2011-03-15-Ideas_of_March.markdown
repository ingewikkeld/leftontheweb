---
layout: post
title: "Ideas of March"
date: 2011-03-15 11:50
comments: true
categories: 
  - php 
  - symfony 
  - symfonylive 
  - blogging 
  - phptek 
  - ideasofmarch 
---
<p>But with the growth of Twitter, Facebook and other social media, it seems many people (to some extent even me) have put blogging on the backburner. And while Twitter is a great way to quickly ask questions and get solution to problems, it doesn't persist this knowledge for later re-use. Twitter does not keep a long archive of tweets, and even if it would, figuring out the conversations is very hard.</p>

<p>So it would be good to save knowledge for later. So that Google can find it. So that the rest of the community can find it. Like I did yesterday with my <a href="https://skoop.dev/message/Stopping_my_SSH_connections_from_stalling">blogpost about stalling SSH connections</a>. Not every blogpost you write needs to be an elaborate journalistic masterpiece.</p>

<p>But even I have written less on my blog in the past year. I've always meant to write more and I have a huge list of topics I need to write about. I just need to actually do it. Chris Shiflett has now <a href="http://shiflett.org/blog/2011/mar/ideas-of-march">given me a good reason to actually do so</a>. His Ideas of March initiative is a very good one, and is a good reason for me to pick up on the blogging again. So yes, I hereby pledge to start blogging more again.</p>

<p>Will you join us? You're definitely in good company. Not just Chris is doing it, but also I just noticed <a href="http://blog.phpdeveloper.org/?p=340">Chris Cornutt</a> has joined the initiative and <a href="http://www.lornajane.net/posts/2011/Ideas-of-March">Lorna Mitchell</a> has also joined. And I'm sure many more will join! So will you?</p>
