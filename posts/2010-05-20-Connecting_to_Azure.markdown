---
layout: post
title: "Connecting to Azure"
date: 2010-05-20 14:36
comments: true
categories: 
  - winphp 
  - php 
  - twig 
  - symfony 
  - mediterra 
  - azure 
  - phpazure 
---
<p>I was a bit afraid that it would be quite hard to connect to Azure Storage for my app, especially with the deadline looming in less than two weeks. My anxiety turned out to be without reason, mostly thanks to the great work of PHPAzure in abstracting away all the complicated stuff. Really, it is all about knowing which classes to use, which calls to make, and how to set it up. The PHPAzure documentation is a bit minimal, but with a bit of digging and trial-and-error it is actually quite easy to connect to Azure Storage.</p><p>What didn&#39;t help was a bit of confusion on my side on what I should use. I had mistaken SQLAzure and Azure Table Storage for some reason, and was trying to connect to Table Storage while having set up SQLAzure. Obviously that didn&#39;t work but after correcting that situation, I ended up having it set up correctly, all of it, and being able to connect to Azure Table Storage.</p><p>One open topic until yesterday was also my way of handling layout. I&#39;ve decided that at least for page content I now use&nbsp;<a href="http://www.twig-project.org/" target="_blank">Twig</a>. I might switch the page layout to Twig as well in the future so that all the layout-logic is done by a single system, but that&#39;s for later.&nbsp;</p><p>I did a&nbsp;<a href="http://github.com/skoop/MediTerra" target="_blank">push</a>&nbsp;yesterday which will allow people to actually list, create and delete tables in Azure Storage. I don&#39;t think Azure allows for table edits (which would make sense since the name is the identifier in most cases, and it&#39;s the only thing you would be able to edit). Next up will be to list entities in the tables. I have to research a bit more, but as I see it right now, at least in the first version for entities you will be able to list and delete. Once that is done, I will be moving on to Blob storage, and I&#39;m really looking forward to starting on that one.&nbsp;</p>
