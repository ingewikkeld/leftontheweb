---
layout: post
title: "New Paypal alternative coming"
date: 2005-05-10 6:31
comments: true
categories: 
  - leftontheweb 
---
It seems that a new alternative to <a href="http://www.paypal.com/">Paypal</a> will be launching later this year. It is called <a href="http://greenzap.com/koopmanschap">GreenZap</a>. From the description, it looks very similar to Paypal. It is always good to have an alternative IMHO.

I've already contacted GreenZap to see if there is an API that developers can use to integrate GreenZap into their site. Should be interesting.

Oh, and if you <a href="http://greenzap.com/koopmanschap">pre-register now</a>, you'll get $25 into your account for free. A nice gesture from the people at GreenZap :)
