---
layout: post
title: "How to remember 9/11"
date: 2006-09-09 11:12
comments: true
categories: 
  - world 
---
In the past week already, the media have been paying a lot of attention to what happened 5 years ago this year on the 11th of september. The attacks on the World Trade Center in New York, the attack on the Pentagon, and the airplane that went down, supposedly on it's way to the White House.

Here is my tip on how to remember 9/11 without following the official story, two DVD's about what happened, each giving a completely different view.

<a href="http://www.amazon.com/exec/obidos/ASIN/B00006LSE8/fantasylibrary-20">11|9 by Jules & Gedeon Naudet and James Hanlon</a>
What was supposed to be a documentary about the NY fire department turned into a documentary on what happened on that faithful September day. A split view, one from outside the WTC, the other from within. Shocking and real.

<a href="http://www.lc911.com/lc911/catalog/">Loose Change by Dylan Avery, Korey Rowe, Jason Bermas</a> [<a href="http://video.google.com/videoplay?docid=7866929448192753501&hl=en">view online</a>]
Less documentary, more conspiracy theory. And even though I think some "facts" might be farfetched, I have the feeling that this movie contains more of the truth than the official statement by the US government and whatever was said in the media. A scary thought, I know, but I really feel this way. Don't take it 100% serious, but at least 90%. 
