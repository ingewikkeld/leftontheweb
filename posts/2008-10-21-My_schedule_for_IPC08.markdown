---
layout: post
title: "My schedule for IPC'08"
date: 2008-10-21 19:51
comments: true
categories: 
  - php 
  - ipc 
  - conference 
  - ipc08 
---
<p><strong>Monday october 27</strong></p><p>Monday is travel-day for me. I will be travelling by train first from Utrecht to Frankfurt, and then onwards to Mainz. There is a social in the evening though which I plan on attending.</p><p><strong>Tuesday october 28</strong></p><p>After the opening ceremony, this will be my schedule:</p><p>- Decouple your PHP code for reusability (Fabien Potencier)<br />- <strike>The PHP Revolution in Business Applications (John David Roberts)</strike> PHP Design Patterns Part 2: Enterprise Patterns (Stefan Priebsch)<br />- Running Asynchronous Queries using ext/mysqli and mysqlnd (Ulf Wendel)<br />- PHP QA: PHP Test Coverage (Zoe Slattery)<br /> - Debugging With Xdebug (Derick Rethans)</p><p><strong>Wednesday october 29</strong></p><p>- Seven Steps to Better PHP Code (Stefan Priebsch)<br />- Suhosin - Catching Vulnerabilities before they hit you (Stefan Esser)<br />- <strike>A different perspective on web-application security (Markus Wagner)</strike><br />- Lesser Known Security Problems in PHP Applicatios (Stefan Esser)</p><p><strong>Thursday october 30</strong></p><p>- Document based databases: CouchDB (Kore Nordmann, Jan Lehnardt)<br />- <strike>PECL Exploration (Scott MacVicar)</strike> The Power of Refactoring (Stefan Koopmanschap)<br />- <strike>Performance Tuning MySQL (Morgen Tocker)</strike><br />- Zend Framework: Past, Present and the Future (Gaylord Aulke)<br />- SQLite3 (Scott MacVicar)</p><p><strong>Friday october 31</strong></p><p>Unfortunately the enterprise-day is fully in german, but since I&#39;m not travelling back until later in the day, I might just take a look. I know a bit of german, and unless they are speaking very fast, I can probably follow it a bit. I can&#39;t hurt to try at least :) </p>
