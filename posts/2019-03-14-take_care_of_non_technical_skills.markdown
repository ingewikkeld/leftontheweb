---
layout: post
title: "Take care of non-technical skills"
date: 2019-03-14 11:15:00
comments: true
categories: 
  - developers
  - personal development
  - wecamp
social:
    highlight_image: /_posts/images/wecamp-personal-development.jpg
    summary: We're all very focussed on improving the technical knowledge and skills of our developers, but should we not focus more on the non-technical skills?
    
---

*Full disclosure*: I am one of the founders and current organizers of [WeCamp](https://weca.mp/), an event that has a focus on not just technical skills but also personal skills.

In my 20+ years of professional experience in the PHP/software development world, I've worked at many companies and been into many companies as a consultant or freelance developer. Many of the companies I've come in touch with had programs set up for training of their developers. Most of those programs focused on improving technical skills. This makes a lot of sense, because in the current tech world, things change so fast that you need [continuous learning](https://skoop.dev/blog/categories/continuouslearning/) to improve. And there is nothing wrong with that.

In recent years, I've seen the focus of training shift a bit from mostly PHP-related subjects to the whole ecosphere of software and tooling around PHP. This is a great shift, because PHP developers don't just write PHP. They use tools like ElasticSearch and memcache, Git and continuous integration, AWS and Azure, and numerous other products that you don't instantly know how to use. Performance, security, quality, it's topics that get more and more attention and rightfully so.

With a few exceptions, however, I've found that many companies still seem to ignore another important part: personal development. I'm talking about things like communication skills, planning skills, a focus on personal happiness. About knowing where you want to go in your life and what to focus on. The human side of the developer. Because, despite what many recruiters would like you to think, a developer is more than just a resource. [Developers are just like humans](https://www.youtube.com/watch?v=onktCMKQkfI).

I've heard managers complain about developers not having good communication skills, but I've hardly ever seen those same managers look for ways to improve those skills for their developers. I've heard managers complain about the lack of planning skills, or the fact that their developers have a hard time structuring their work day, but I've often seen those same managers only consider technical training for those same developers. And yet, the first non-sponsored link when I [search the web for planning skills training](https://www.startpage.com/do/dsearch?query=planning+skills+training&cat=web&pl=ext-ff&language=english) is an [effective planning skills training](http://aztechtraining.com/course/effective-planning-skills). Same for [searching for communication skills training](https://www.startpage.com/do/dsearch?query=communication+skills+training&cat=web&pl=ext-ff&language=english). The first result is a [learning tree training](https://www.learningtree.com/courses/292/communication-skills-training-how-to-improve-communication-skills/). And that's just the first results. Go down the results and you'll find a lot more.

### One way to focus on more than just technology

As mentioned in my full disclosure at the start, I am one of the founders and current organizers of [WeCamp](https://weca.mp/), a 5-day event focussing on improving both technical and non-technical skills that are essential to software development. We've received a lot of positive feedback on the key take-aways of the event being more than just technical skills. I am very proud of that. When we get feedback such as:

> To developers, I'd say that the experience is unrivalled by anything in the market today. The coach's focus on your personal development is guaranteed to push you on exactly the points that need improving.

 this means we've done our job. We push people to reflect their current position and where they're heading. We push them to evaluate if their current heading is what they really want. But we also help them set goals and achieve those goals. Whether this is about new tech they want to learn or non-tech skills they want to improve. Actually, when we asked what was the best thing about WeCamp 2017 in the evaluation questionnaire, one of the attendees responded with:

> The blend of technical and personal development.

In that same questionnaire, when asked about why people would recommend WeCamp, we got things like:

> Great learning and life experience and pushes you to get out of your comfort zone in a positive way.

I know I am biased because I'm very much involved in this event, but I really believe that by creating the safe space that we create for people to reflect on their life and career and by getting developers our of their comfort zone, we add a value that not many other events could. 

### Interested?

If you or your developers are interested in WeCamp, please check out [our website](https://weca.mp/). If you have any questions, please do feel free to contact me. 