---
layout: post
title: "The Power of Refactoring slides from PHPNW"
date: 2008-11-24 10:17
comments: true
categories: 
  - conference 
  - refactoring 
  - php 
  - phpnw 
  - phpnw08 
  - slides 
---
<p>&nbsp;</p><div id="__ss_782271" style="width: 425px; text-align: left"><a style="margin: 12px 0pt 3px; font-family: Helvetica,Arial,Sans-serif; font-style: normal; font-variant: normal; font-weight: normal; font-size: 14px; line-height: normal; font-size-adjust: none; font-stretch: normal; display: block; text-decoration: underline" href="http://www.slideshare.net/skoop/the-power-of-refactoring-phpnw-presentation?type=powerpoint" title="The Power Of Refactoring (PHPNW)">The Power Of Refactoring (PHPNW)</a><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="425" height="355"><param name="movie" value="http://static.slideshare.net/swf/ssplayer2.swf?doc=thepowerofrefactoringphpnwfinal-1227519125930991-8&amp;stripped_title=the-power-of-refactoring-phpnw-presentation" /><param name="quality" value="high" /><param name="menu" value="false" /><param name="wmode" value="" /><embed src="http://static.slideshare.net/swf/ssplayer2.swf?doc=thepowerofrefactoringphpnwfinal-1227519125930991-8&amp;stripped_title=the-power-of-refactoring-phpnw-presentation" wmode="" quality="high" menu="false" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="425" height="355"></embed></object><div style="font-size: 11px; font-family: tahoma,arial; height: 26px; padding-top: 2px">View SlideShare <a style="text-decoration: underline" href="http://www.slideshare.net/skoop/the-power-of-refactoring-phpnw-presentation?type=powerpoint" title="View The Power Of Refactoring (PHPNW) on SlideShare">presentation</a> or <a style="text-decoration: underline" href="http://www.slideshare.net/upload?type=powerpoint">Upload</a> your own. (tags: <a style="text-decoration: underline" href="http://slideshare.net/tag/refactoring">refactoring</a> <a style="text-decoration: underline" href="http://slideshare.net/tag/phpnw08">phpnw08</a>)</div></div><p>&nbsp;</p>
