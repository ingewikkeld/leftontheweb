---
layout: post
title: "Using git-svn with Symfony2's deps installer"
date: 2012-04-03 11:32
comments: true
categories: 
  - git 
  - Symfony2 
  - deps 
  - filter-branch 
---
<p>Googling around a bit, I found the solution which I'm going to share here with you. It involves a couple of steps.
<ul>
    <li>Remove the .git directories from the external bundles</li>
    <li>Run <pre>git filter-branch --index-filter 'git rm -r -f --cached --ignore-unmatch vendor/bundles/Liip/MonitorBundle' -f</pre> on each of the external bundle directories where you removed the .git directory. This command needs to be run from the root of your git project!</li>
    <li>Git svn dcommit your changes again</li>
</ul>
Now it should all work again.</p>

<p>Some links I used while researching this are:<br />
<ul>
    <li><a href="http://stackoverflow.com/questions/5016324/git-svn-dcommit-not-work">SO: git svn dcommit not work</a></li>
    <li><a href="http://ignoredbydinosaurs.com/2011/06/quick-trip-panic-room">A quick trip to the panic room</a></li>
    <li><a href="http://stackoverflow.com/questions/2982055/detach-many-subdirectories-into-a-new-separate-git-repository">Detach many subdirectories into a new, separate Git repository</a></li>
    <li><a href="http://stackoverflow.com/questions/8050687/how-do-i-enable-shell-options-in-git">How do I enable shell options in git?</a></li>
</ul>
</p>
