---
layout: post
title: "International PHP Conference 2010"
date: 2010-10-14 19:03
comments: true
categories: 
  - ipc10 
  - ipc 
  - internationalphpconference 
  - php 
  - symfony 
  - Symfony2 
  - conferences 
---
<p>Our morning started early as we were driving down to Mainz. We arrived as the opening keynote by Zeev Suraski ended, but right in time for the first regular session. My session of choice was Ian Barber's talk on debugging. Ian gave a good overview of tools and strategies for debugging and described some really familiar situations. Though perhaps not always new information, still a good reminder on some solutions and approaches.</p>

<p>Next up was Liip's Jordi Boggiano, who was speaking on sharing knowledge and developer quality lifecycle. It is a wonder I didn't get a sore neck from all the nodding I did during Jordi's presentation, I could've given the same talk so much did I agree with him on how to share knowledge and inspire other developers to expand their own knowledge. The information itself was nothing new to me though, but it triggered a very nice discussion between the attendees on how to get people to indeed widen their knowledge and how this can be accomodated. </p>

<p>After lunch I went to sit in on Ben Ramsey speaking on AtomPub. To me, Atom was only a content syndication format just like RSS, and I was not really aware of the whole AtomPub technology that actually allows you to use the Atom format to publish data as well. This talk inspired to have a look at AtomPub for future reference, as I am pretty sure this is a technology that I will be able to use at some point in the (near) future.</p>

<p>A short break later, I was in the room for David Zuelke's REST talk. David is always a good speaker and REST is an interesting topic, so I couldn't be making a bad choice in this session. I didn't, as David presented the REST structure. It was especially made clear by the examples of the (bad) Twitter REST interface and how this could actually be improved. Made me decide I should think a bit more on the next REST interface I build, as I definitely made some mistakes in the past.</p>

<p>I decided to close the day with a talk by Tobias Schlitt and Kore Nordmann on extreme uses of PHPUnit. The speakers used examples to show how you could use PHPUnit to test some hard to test situations. I must admit though that after such a long day it was a bit hard to concentrate, so it was sometimes hard to follow everything. </p>

<p>Tuesday morning we came in a bit late but I sneaked into Stefan Priebsch' talk on refactoring anyway. I did a talk on refactoring some time ago at several conferences and I must say Stefan actually did a slightly better job in explaining the concept and some situations. I definitely think most of the attendees will have gathered some knowledge and I hope it has inspired them to take a look at their own code to see how they can improve it.</p>

<p>I then attended Sebastian Bergmann's talk on Agility and Quality. An interesting talk where I learned about some strategies of testing and launching new features in applications. Though these could be applied to any project, I think they will be especially useful in big applications and the next big application I'll work on and am involved in the testing and deployment, I will have to research the topics a bit more and see if I can apply them to the project.</p>

<p>I stayed in the "Agile track" for Thorsten Rinne speaking on Continuous Improvement. Thorsten gave a good (but slightly short) overview of the different tools that can be used to improve software. Some were already known to me, but tools like Cinder and Padawan were new to me and may be applied at some point in the future.</p>

<p>After lunch and the keynote (which I skipped to make sure I was prepared for my first talk of the conference) I presented my "Integrating symfony and Zend Framework" talk. I've done this a couple of times before but had changed some of the examples. I think my talk went well and the examples actually improved a bit. I decided to also open source the autoloader I presented in this talk on <a href="http://github.com/skoop/Stefan-Koopmanschap-s-Toolbox/tree/master/sfZfAutoloader/">Github</a>.</p>

<p>A nice initiative was made by Jordi Boggiano who did an ad-hoc session on Symfony2 in a free room during the last slot. Jordi did a nice presentation on some of the new features of Symfony2, that will have informed many of those present of the nice things that are coming once Symfony2 hits stable. After this session, it was time to chill for a bit, only to go back into one of the conference rooms with a beer in hand for the PHP Night Talk, a nice discussion with a panel of 4 on PHP and related topics, which was made even more interesting by the Twitterwall which was projected behind the panel, where some of the remarks made there (on and off-topic) added some fun to the discussion. After that, it was time for the casino party and the earlier mentioned waffles to celebrate the tenth anniversary of the conference. </p>

<p>After two days of conference (and of course being at SymfonyDay the days before), I was quite overloaded with information. Even though I had planned to attend several talks, I ended up not attending many talks on wednesday. After doing my talk "Would you like docs with that?" (which I was not really satisfied with this time) I went to attend Derick Rethans' talk on geolocation and maps with PHP (which he will also deliver at the <a href="http://conference.phpbenelux.eu/">PHPBenelux Conference</a> in January). An interesting talk which triggered me put it on my TODO list to check back into OpenStreetMap, which seems to have improved a lot since I last looked at it.<p>

<p>Some speaker interviews for the PHPBenelux Conference later, it was time to head back home after three intensive but awesome days. I hope to be attending International PHP Conference again next year, for sure!</p>
