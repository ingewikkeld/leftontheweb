---
layout: post
title: "Now a Sensio partner"
date: 2011-01-07 8:17
comments: true
categories: 
  - symfony 
  - php 
  - sensio 
  - partnership 
  - ingewikkeld 
  - training 
  - Symfony2 
  - doctrine 
---
<p>With this partnership, a long standing wish of mine is finally fulfilled. I've been wanting to offer the Sensio training portfolio on symfony and Doctrine for some time now, and am happy to be able to do this now. The current portfolio includes symfony 1.4 & doctrine but of course you can expect new Symfony2 courses to be coming in the near future as well! For those that can not wait for those Symfony2 courses, there is already a Symfony2 workshop <a href="http://trainings.sensiolabs.com/en/training/symfony2">scheduled for March 2nd in Paris</a>, right before the <a href="http://www.symfony-live.com/">Symfony Live</a> conference takes place in the same city.</p>

<p>So as of this moment, you can contact me for in-house trainings for your development team. But I will also be organizing classroom training sessions where you can simply book a seat to attend the course. I am currently looking at organizing the first classroom sessions in March or April. If you have questions or suggestions, do let me know!</p>

<p>Now, for champagne :)</p>
