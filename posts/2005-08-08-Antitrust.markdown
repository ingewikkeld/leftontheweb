---
layout: post
title: "Antitrust"
date: 2005-08-08 10:46
comments: true
categories: 
  - movie 
  - hackers 
---
Last weekend, I was just zapping around TV channels when all of a sudden I came by the opening credits of a movie. Now this happens often of course, but this caught my attention: The opening credits were surrounded by HTML tags!  It happened to be the opening credits to <a rel="tag" href="http://www.imdb.com/title/tt0218817/">Antitrust</a>, which ended up being quite a good hacker-like movie. They actually used linux commandline stuff, though using a command like &#39;hideall&#39; which would then actually hide the files from viewing by others is maybe not too realistic, but it worked well enough in the movie of course. (There are more examples of stuff like this, such as being able to connect to sattelites just by typing in their ip number ;) )  Yes, this was actually quite an enjoyable movie. From the comments over at imdb I gather that the trailers were quite different, and that the DVD of this movie might actually contain some very nice other material as well. The DVD is <a href="http://www.play.com/play247.asp?pa=srmr&amp;page=title&amp;r=R2&amp;title=94344">available already</a>, so I might just want to get that some time in the (near) future.
