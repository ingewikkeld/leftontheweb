---
layout: post
title: "Tweak tweak"
date: 2006-05-25 16:14
comments: true
categories: 
---
As you may or may not have noticed, I've been tweaking the site a bit. OK, so the tags plugin is still not working, but some other things are working again. For instance, the Recent Music listing in the sidebar, as well as the <a href="https://skoop.dev/AudioFlickrScrobbler">AudioFlickrScrobbler</a>. And I've changed the 'Author' info a bit, seperated it into it's own page where I can give you slightly more information you don't want or need ;)

Now, to get those tags working again... ;)
