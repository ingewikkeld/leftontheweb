---
layout: post
title: "Ingewikkeld presents: Comference Summer Sessions"
date: 2021-06-18 15:00:00
comments: true
categories: 
  - php
  - events
  - conferences
  - comference
  - summer sessions
social:
    summary: We're doing another thing. Three 1.5 hour sessions about different topics in which a panel will discuss the subject at hand and answer questions asked by attendees. The accompanying podcast will give you a nice reminder of the things that were discussed.
    
---

I am very happy to announce a new initiative from [Ingewikkeld](https://ingewikkeld.dev/): [Comference Summer Sessions](https://comference.online/summer-sessions/). In three interactive panel discussions we'll be talking about three topics we feel are very important in development:

* Open Source in your company
* Facilitating company cultural change
* The role of the Product Owner in a development team

Each session will last about 1.5 hours, in which the panel will discuss the subject at hand. Through our Comference Discord, you can also ask questions and remarks that the panel can talk about. 

After each session, the panelists will also record a 30-minute podcast in which the panel summarizes the most important lessons from the interactive session. So if you miss the stream or if you want to be reminded of what was talked about, the podcast is there to help you out.

For the first session, on Open Source in your company, the panel is already known:

* Host: Jaap van Otterdijk (Ingewikkeld)
* Sebastian Bergmann (PHPUnit, ThePHP.cc)
* Stefan Koopmanschap (Ingewikkeld)
* Erik Baars (ProActive)

I am very excited about this new series of events. The sessions are free to attend (we're streaming them live on YouTube). SO block your calendars for the three dates:

* Tuesday, June 29
* Tuesday, July 27
* Tuesday, August 24

I hope to see you there!