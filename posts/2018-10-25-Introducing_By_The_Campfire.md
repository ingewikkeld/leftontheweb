---
layout: post
title: "Introducing By The Campfire"
date: 2018-10-25 17:00:00
comments: true
categories: 
  - podcast
  - by the campfire
  - rick kuipers
  - conversation
social:
    highlight_image: /_posts/images/bythecampfire-rick.jpg
    summary: Finally the first episode of my podcast is published! I proudly present By The Campfire.
    
---

It's been over a year that I first decided to start a podcast. Inspired by the Dutch podcast [Wilde Haren De Podcast](https://www.patreon.com/WildeHarendepodcast) in which the host Vincent Patty has interesting conversations with his guests which offer a lot of information about the person as well as the topics they're interested in, I decided I wanted to do something similar with guests from my general area of interest. I first started working on a website, made a list of people I'd like to have on as a guest, and then... procrastinated for way too long.

A few weeks ago I finally got off my butt and made a serious attempt at scheduling some dates. And with pride I can say that the [first episode is now published](https://bythecampfire.net/conversation/001-how-fortnite-dances-can-help-your-speaking-career)! In it, I talk with the awesome [Rick Kuipers](https://twitter.com/rskuipers) on a variety of subjects ranging from Fortnite to dancing, from speaking at conferences to freelancing and from chess to self-steering teams. 

Everyone, I'd like to introduce [By The Campfire](https://bythecampfire.net/). Because the best conversations are had in situations where there is very little distraction, like when you're sitting by the campfire. And yes, this idea is totally inspired by the campfire talks I've had at [WeCamp](http://weca.mp) over the years. 

I'll be publishing the podcast on an irregular basis (depending on when I can schedule to meet with someone interested in being a guest on the podcast). You can subscribe to the podcast in several services already, although I am still awaiting approval from Apple and Spotify. I've got a list of all places where we've been approved on [the About page](https://bythecampfire.net/about). And of course, you can simply listen [on the website](https://bythecampfire.net/conversation/001-how-fortnite-dances-can-help-your-speaking-career).

Some thank yous are required. For instance to Stephan Hochdoerfer and the Ingewikkeld crew, who helped with finding the right name for the podcast. Also to someone I can't remember who exactly who came up with the idea of asking my guest to come up with a title for the episode, which results in a title such as [How Fortnite Dances Can Help Your Speaking Career](https://bythecampfire.net/conversation/001-how-fortnite-dances-can-help-your-speaking-career). To my wife [Marjolein](https://twitter.com/TearSong) who graciously let me borrow her Zoom H4n recorder. And of course to [Rick](https://twitter.com/rskuipers) for being my first guest.
