---
layout: post
title: "Zend Studio for Eclipse"
date: 2008-02-12 22:49
comments: true
categories: 
  - zend 
  - zendstudio 
  - ide 
  - software 
  - eclipse 
  - php 
---
<p>I have already tried <a href="http://www.zend.com/en/products/studio/" target="_blank">Zend Studio for Eclipse</a>  when it was still called Neon, which was the public beta of this software. I was not impressed. Aside from the fact that it was built on a platform I was far from a fan of, some key features were not yet working, things like code hinting and code completion for included libraries.</p><p>So when the stable version came out, I was reluctant to give it another try. Given my bad experiences with Eclipse and the early beta, I postponed it, and actually kept postponing it. But eventually, after some comments from others, I decided to give it a try.</p><p>But man, how wrong I was in waiting this long! Sure, there are still some things in the Eclipse platform that I dislike, but overall, everything that I didn&#39;t like about the beta was fixed, and the IDE as it stands here right now is not just highly usable, it seems faster and less heavy (which is a new for Eclipse in my experience) than the previous versions of Zend Studio. And it brings some of the great advantages of the Eclipse platform (yes, they are there!) such as the great plugin infrastructure combined with the enormous amount of available plugins. I&#39;m sure there&#39;s a few features I don&#39;t use as often as others that I haven&#39;t encountered yet, so over time my opinion may change, but my first impression is really positive, and I&#39;m quite surprised about that. When they first announced the new version would be based on Eclipse I&#39;d have never thought I&#39;d say this but: I highly recommend Zend Studio for Eclipse as a PHP IDE! </p><p><em>Full disclosure: I am employed by a Zend Partner, but this hardly changes my opinion on things. I still prefer symfony over the Zend Framework and until recently preferred Zend Studio 5.5 over Zend Studio for Eclipse ;) </em></p>
