---
layout: post
title: "Continuous Integration in PHP"
date: 2007-04-10 2:13
comments: true
categories: 
  - technology 
---
PHP seems to be maturing lately from a scripting language to (nearly) a real programming language. Part of this process is the slow but certain adaption of development methodologies such as Agile, more and more unit testing, more and more usage of frameworks as opposed to writing everything from scratch every time, and now also Continuous Integration.

Through <a href="http://sebastian-bergmann.de/archives/669-Xinc-is-not-CruiseControl.html">Sebastian Bergmann's weblog</a> I found out about a nice project which is currently in Alpha state called <a href="http://sourceforge.net/projects/xinc">Xinc is not CruiseControl</a>, a framework for doing Continuous Integration testing. It is based on PHPUnit and Phing, and supports Subversion. Very, very cool! 

Unfortunately, I've always found working with PHPUnit a bit tedious. Now that I'm using Symfony, which has built-in support for SimpleTest, I am more likely to be writing actual tests. Maybe I'll see if I can somehow integrate SimpleTest, or maybe even Symfony, into the Xinc system so that I can use it to do my testing as well.
