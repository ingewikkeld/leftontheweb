---
layout: post
title: "March 2024 conferences"
date: 2024-01-19 14:30:00
comments: true
categories: 
  - php
  - conferences
  - talks
  - webcampvenlo
  - dutchphpconference
  - dpc
social:
    highlight_image: /_posts/images/customeralwaysright.png
    highlight_image_credits:
        name: Mike van Riel
        url: https://mikevanriel.com
    summary: Conference season has started again and the first two conferences of the year have already been scheduled. Where can you see me in March? And which talks will I be doing?
    
---

I hope 2024 has started out good for you, and I certainly hope you are planning to attend some conferences. I'm happy to announce two awesome places where I'll be doing talks

## Web Camp Venlo

First I'll head to the South of the Netherlands, to Venlo to be exact. There, [Web Camp Venlo](https://www.webcampvenlo.nl) will take place from February 29 to March 2. They've got some interesting workshops on their first day, but I'll be there on day 2 (March 1st) to deliver my Domain-Driven Design talk. If you missed that at SymfonyCon, this is a great opportunity to catch this talk. There's very interesting talks on the schedule including on Keycloak (by Annemieke Staal), mental health and burn0out (by Jeroen Baten) and more. Tickets are [available now](https://www.webcampvenlo.nl/tickets). See you there?

## Dutch PHP Conference

After several online-only editions due to you know what I am extremely happy that the [Dutch PHP Conference](https://phpconference.nl) is organizing a physical conference again this year. They're teaming up with AppDevCon and WebDevCon, making this a must-attend event if you're in the Netherlands. They have 2 training days, a tutorial day and a conference day with an amazing amount of talks. I'm excited to be doing my Sustainable open source contributions in your business talk there, next to interesting talks on sustainability (by Michelle Sanver), Git (by Juliette Reinders Folmer), agile architecture decisions (by Nic Wortel) and breaking production (by Sofia Lescano). Tickets are [available now](https://egeniq.paydro.com/dpc24). I hope to see you in Amsterdam!