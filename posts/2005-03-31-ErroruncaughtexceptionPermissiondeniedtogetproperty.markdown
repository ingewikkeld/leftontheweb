---
layout: post
title: "Error: uncaught exception: Permission denied to get property"
date: 2005-03-31 13:39
comments: true
categories: 
  - leftontheweb 
---
OK, I ran into this problem today and luckily Googling answered my question. I thought it would be nice to share :)

I was working on a system where I had to open a new window, where the user would select a certain item from a list, and when clicking on that item, the item would be selected in the original window. Problem: I got an error message:

<blockquote>Error: uncaught exception: Permission denied to get property Window.miloform</blockquote>

Now, thanks to the <a href="http://www.mozilla.org/docs/dom/domref/dom_window_ref76.html">Gecko DOM reference</a> I found the answer to my problem:

<blockquote><strong>I can not access the properties of the new secondary window. I always get a javascript error in the console saying "Error: uncaught exception: Permission denied to get property &lt;property_name or method_name&gt;". Why is that?</strong>
It is because of the cross-domain script security restriction (also referred as the "Same Origin Policy"). A script loaded in a window (or frame) from a distinct origin (domain name) cannot get nor set properties of another window (or frame) or the properties of any of its HTML objects coming from another distinct origin (domain name). Before executing a script, the browser in the main window verifies that the secondary window has the same domain name.</blockquote>

It turns out that the client I am building this for has two domain names, and the new window was opened from a different domain. Yes, that would be a problem, knowing the above.
