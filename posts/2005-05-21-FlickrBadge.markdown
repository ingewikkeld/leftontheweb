---
layout: post
title: "Flickr Badge"
date: 2005-05-21 12:44
comments: true
categories: 
  - leftontheweb 
---
Not sure if this a new feature of <a href="http://www.flickr.com/" rel="tag">Flickr</a> but they've now got some fun Flickr Badge system. You can see what it is in the sidebar on the right. You see a random selection of 3 photos from my Flickr account. It's quite customizable too.
