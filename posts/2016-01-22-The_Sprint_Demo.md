---
layout: post
title: "The Sprint Demo"
date: 2016-01-26 20:15:00
comments: true
categories: 
  - php 
  - scrum
  - sprint
  - demo
---

The new year also brought a new customer for me: At the start of January I started at the Digital Solutions Center of Schiphol Airport. They have an awesome project going on, and I operate in one of the two scrum teams there.

When you're starting with a new customer that has adapted Scrum (or any form of agile), it is always interesting to see *how* they adopted it. Interpretation and implementation of scrum and other agile methodologies differs greatly amongst different customers. I've had customers where it worked really well, and I've seen customers where scrum was only an excuse to be able to switch priorities or scope whenever they felt like it. No, I won't mention names ;)

Overall Schiphol gets agile. Not just the developers, but throughout the organization you'll see scrum boards in the hallways or in offices. It is awesome to see this work not just inside development teams. But there's one thing I've been most impressed with from the start: The sprint demo.

To give you some context: Just about every scrum/agile project I've worked on in the past had the same form of demo: One person would start in front of a crowd of stakeholders, team members and other interested people and run through the features that were delivered in the most recent sprint. The meetings were usually boring and one-way traffic: There was little interaction because of the way the meeting was set up.

In some projects, this was recognized and stakeholders were encouraged to speak up and give feedback. So now we got feedback, which was good, but the meetings could still sometimes be really boring, or break into useless discussions between stakeholders about implementation details or even worse: internal policy decisions.

![Sprint Demo, photo by Paul Nieuwdorp](/_posts/images/demo1.jpg) 

Schiphol got this and took on a new style of sprint demo. Instead of having a single person present the new stories that were delivered, stakeholders are split up in smaller groups. Several team members work together (or split the group up even more) to present the stories that were completed. Some of the team members present the features while others are standing there with sticky notes to write down any feedback from the stakeholders. Stakeholders are also encouraged to take the mobile devices that are on tables throughout the area to test all of the delivered mobile stories. Throughout the demo, the groups switch to other screens/devices to see other new features. At the end of the demo most people have seen all new features and have been able to give feedback on them.

![Sprint Demo, photo by Paul Nieuwdorp](/_posts/images/demo2.jpg)

One of the most important reasons to take an agile approach is to have a good, short feedback cycle: To gather feedback from the stakeholders, the business owners, and possibly even your users. By taking this more interactive approach, it is much easier to get good feedback from all parties involved. But it also ensures a good, informal contact between the development team(s) and the stakeholders. Because the groups are smaller and the whole setup is not about one-way communication, people (even team members) feel free to offer their comments, to give that feedback that you as a team need to improve the product and to ensure you build the software according the expectations and wishes of the stakeholders. And that is eventually what the agile approach is about: Building the things that people want, not what they think they want. Because the groups are smaller, there is also less useless discussion. If a discussion happens, it is usally about the new feature and not about internal policies or decisions. 

I've found this form of sprint demo to be much more in the spirit of scrum and agile, and I've been impressed at the way both teams handle the demo, and the way it is received by the stakeholders. The feedback we get is valuable and the time we spend on the demo feels well spent. 

So have a look at your sprint demo. See how it works right now, and have a good look at how you can improve. The above may not work for every organization, but I sure am impressed how well it works within Schiphol. 