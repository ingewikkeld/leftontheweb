---
layout: post
title: "Would you like docs with that?"
date: 2011-02-15 19:41
comments: true
categories: 
  - php 
  - webinar 
  - documentation 
  - symfony 
---
<p>This thursday, February 17th, I'm doing the presentation as a webinar in the <a href="http://www.zend.com/en/company/events/">Zend Webinar</a> series. At 9AM PST (or 5PM GMT, 6PM CET) I'll be explaining why documentation is important, and what types of documentation you can use for your project. The webinar should last for about an hour (unless there's many questions), and should be interesting for most developers. I know, the topic doesn't sound really sexy to most developers but it <em>is</em> really important.

<p>So feel free to <a href="http://www.zend.com/en/company/news/event/775_webinar-would-you-like-docs-with-that">register for the webinar</a> and join me on thursday.</p>
