---
layout: post
title: "Firefox 1.5, the first beta"
date: 2005-09-19 18:46
comments: true
categories: 
  - technology 
---
Well, I've upgraded recently to the first beta of Firefox' new version 1.5. So far, I seem to notice little difference (which is a good thing and a bad thing at the same time). It's good, because that means the surfing experience of Joe Regular won't change much. It's bad, because that also means that if any new features were added, in my every day use so far I haven't been able to find them.

What I did find however, was that after my upgrade to 1.5b1, most of my extensions are disabled due to apparent incompatibility. Though I suspect most are not incompatible at all, they're not yet edited to actually support the new version number. Which is a shame.

The only true difference in this new version that I found is the layout of the preferences, with the main options laid out horizontally instead of vertically. 

I'm sure there's more but right now I can't find it. The fact that Firefox keeps having a regular, solid browsing experience is a good thing but unfortunately there seems to be little new about this version of our beloved browser.
