---
layout: post
title: "Memorial and liberation"
date: 2006-05-04 14:23
comments: true
categories: 
  - personal 
---
Every year on May 4th, between 8PM and 8:02PM, The Netherlands remembers those who have fallen in war situations and during peace missions. Today was no different. Two minutes of silence was too much, however, for a radio station (I am not sure which one) that was on while visiting my in-laws. At just before 8, they played a statement about freedom and remembering, but instead of the usual 2 minutes of silence, this radio station chose to play Imagine by John Lennon. And even though I have not personally lost anyone in the second world war or after that, and I have not been there to experience the war, I was offended. I truely felt offended that this radio station did not respect the two minutes of silence that most of the country respects (even trains and busses stop for two minutes).

Our government is no better though. Even though every year in the week before May 5th (Liberation Day) they all speak of how it's important that we remember what happened during the second world war and that it should not happen again, Liberation Day is no national holiday anymore. Only once every 5 years, Liberation Day is a national holiday. Now, isn't that hypocritical? And now they're surprised that our youngest generation isn't really interested anymore in what happened during the war. That they don't know enough about it anymore.

I, for one, took my two minutes of silence, remembering the second world war and those who have fallen. And tomorrow, I will remember the fact that we are now a free country, and that we should rejoice this freedom because it's not always been this way.
