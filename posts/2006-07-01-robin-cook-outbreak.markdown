---
layout: post
title: "Robin Cook - Outbreak"
date: 2006-07-01 3:44
comments: true
categories: 
  - books 
---
I can not seem to find out if the movie <a href="http://www.imdb.com/title/tt0114069/">Outbreak</a> was or was not based on this book. There are too many similarities to say it's coincidence, but there are too many differences to truely be the movie of the book. So maybe they just got inspired.

I had little expectations before starting this book. I had heard some positive things about Cook in general, and this book was sent to me as a gift (a <acronym title="Random Act of BookCrossing Kindness">RABCK</acronym>) and recently, I felt like picking it up to read it. A good choice it was. Where it usually takes me 2-4 weeks to finish a book, this book only took me about a week. It is a very exciting book, full of tension. It is written very well and very easy to read. I definitely can recommend this book to anyone who likes thrillers.
