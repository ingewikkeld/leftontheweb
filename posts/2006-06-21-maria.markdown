---
layout: post
title: "Maria"
date: 2006-06-21 22:26
comments: true
categories: 
  - personal 
---
Every once in a while, you meet someone that makes one hell of an impression on you. During my volunteer work, I met someone like that. Her name was Maria, and she was employee of the elderly home where we do our volunteer work. She was responsible for organizing activities for the residents. But she did so much more. The 
