---
layout: post
title: "public static vs static public"
date: 2009-01-26 21:55
comments: true
categories: 
  - php 
  - static 
  - public 
  - symfony 
---
<p>Even in current literature on PHP 5 object oriented development, you&#39;ll mostly see mentions of the &quot;public static&quot; order. And it isn&#39;t strange, I can understand that some people prefer the PPP at the start to give clarity on the access rules for methods.</p><p>While looking around inside the symfony code, I first encountered the &quot;static public&quot; order of keywords. At first, I thought this strange, but the more I look at it, the more beautiful I think it is. It gives a great overview of which methods are static and which aren&#39;t.&nbsp; </p><p>In the end it is of course a personal preference, however I&#39;ve found my preference shifting towards the latter over the past months. I decided to check which was preferred most in my twitterverse, and here&#39;s the results:</p><p><img src="/images/twtpoll_staticpublic.png" border="0" alt=" " title="Public static vs Static Public" width="529" height="252" /></p><p>&nbsp;</p><p>As you can see, a big majority still prefers &quot;public static&quot;. However, even more important I think is that the &quot;static public&quot; had more ground than I expected (or perhaps it&#39;s that a lot of people that follow me come from the symfony world). Having 2 votes for the option &quot;I don&#39;t use static methods&quot; was perhaps also a small surprise, but I guess it depends a lot on what you work on as well.</p><p>If you haven&#39;t voted yet, feel free to <a href="http://twtpoll.com/r/al5xq4" target="_blank">give your opinion</a>  in my poll.&nbsp; </p>
