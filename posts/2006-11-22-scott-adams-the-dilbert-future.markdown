---
layout: post
title: "Scott Adams - The Dilbert Future"
date: 2006-11-22 10:56
comments: true
categories: 
  - books 
---
For those who like the <a href="http://www.dilbert.com/">Dilbert comics</a>, this book will be perfect. It combines the Dilbert comics with great and completely prejudiced information, views and opinions of Scott Adams, written in a similarly funny way. The book closes with a more serious part, which I found very interesting. All in all, this was a great read.
