---
layout: post
title: "Talk of the town"
date: 2005-09-09 14:14
comments: true
categories: 
  - google 
  - cerf 
---
News is spreading fast on the Internet, especially with news like this. <a href="http://www.nytimes.com/aponline/technology/AP-Google-Cerf.html">Vinton Cerf</a>, who is widely creditted with inventing TCP/IP, the protocol used for the Internet, <a href="http://www.nytimes.com/aponline/technology/AP-Google-Cerf.html">joins the ranks of Google</a>. Google just became a whole lot mightier. I say: wow. WOW.
