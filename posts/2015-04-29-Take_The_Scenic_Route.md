---
layout: post
title: "Take The Scenic Route"
date: 2015-04-29 13:00
comments: true
categories: 
  - php 
  - outsidethebox 
  - relax 
  - mindgames
---

Yesterday was a really hard day at work. Lots of meetings, tough topics, lots of stress, lots of frustration. So when I went home, I didn't take my usual route, but decided to exit the highway a bit earlier than usual and drive through the forest.

## Relax And Clear Your Mind

Driving through nature, whether it is a forest, a moor or meadows, has a calming effect on me. It allows me to clear my mind and just enjoy the moment. It also takes you away from the daily routines you have, take a distance from what you see every day and have some variation.

## This Also Works For Programming

And while the above is a real thing, it can work just as well as an analogy for being stuck in a programming problem. Sometimes, you just need to step back and take the scenic route. Try a different solution than the one you're stuck on, find another way. Modern version control lets us easily stash one solution to work on a different solution. You could even stash your second solution and work on a third. Eventually, you will find the right one. It could even be your first solution, but you might not realize it until you've tried the other solution(s).

## Get Off Your Route

So when you're stuck, try the scenic route. This means not trying to solve the problem using the same approach you've used before, but try a different approach. Completely break with your current path of choice and think of other ways you could solve this problem.

## Also: Get Off Your Route

Programming doesn't stop the minute you step into your car, onto your bike, into your bus or train. So analogies aside, take a different route home every once in a while. This might just force your brain to also take a different route, see a different approach, consider a different  way of handling the same issue. And if that doesn't work, at least you've enjoyed the scenic route home.
