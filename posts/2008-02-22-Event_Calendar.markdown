---
layout: post
title: "Event Calendar"
date: 2008-02-22 22:37
comments: true
categories: 
  - php 
  - symfony 
  - event 
  - leftontheweb 
  - conference 
---
<p>As you can see in this new event calendar, aside from the Dutch PHP Conference, I will also be speaking during the Nllgg dag (regular event of the Dutch Linux User Group), where I will give a short introduction into the symfony framework, and I will be speaking on another very cool event, the pfCongrez, where I will first go into PHP frameworks in general, and then dive into symfony a bit deeper to show people how it works and what you can do with it.</p><p>Aside from these events, there are some other possible speaking arrangements for me, but nothing is sure yet on these, so as news about those comes up I&#39;ll post them in the calendar.&nbsp;</p>
