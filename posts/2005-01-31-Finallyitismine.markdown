---
layout: post
title: "Finally it is mine!"
date: 2005-01-31 4:10
comments: true
categories: 
  - leftontheweb 
---
There is a short list of records that I am still looking for. And every once in a while, I manage to win one on eBay. My latest win this way is the classic record <a href="http://www.discogs.com/release/50368">Ramirez - Hablando</a>. A true classic for people who were into the house scene between 1990 and 1995. And since that was the period I was active as house DJ, this record is definately a classic for me. Of course, I was stupid enough not to buy it when it came out. But I have it now! I am a happy man :)
