---
layout: post
title: "My A/R/T"
date: 2006-07-14 13:23
comments: true
categories: 
  - meta 
---
Well, it seems I'm an A/R/T-ist now. After the earlier call for writers, I sent the nice people at <a href="http://www.phparch.com/">PHP|Architect</a> an e-mail telling them I was interested in writing an occasional article for them. Today, <a href="http://hades.phparch.com/ceres/public/article/index.php/art::oss::how_can_i_help_you">my first article</a> in what is meant to be a monthly series of articles covering a variety of topics related to PHP has been published. The topic of the article, supporting open source projects, is something I've been wanting to write about for quite a while. I never really got around to really take the time to put down my ideas for this article. I felt A/R/T is a good platform for this article, and so took the time and attention to actually write the article. I am quite happy with it myself. Now... what will my second A/R/Ticle be about?
