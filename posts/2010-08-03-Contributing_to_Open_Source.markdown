---
layout: post
title: "Contributing to Open Source"
date: 2010-08-03 20:23
comments: true
categories: 
  - opensource 
  - contribute 
  - php 
  - symfony 
  - community 
---
<p>Not too long ago I was the "external expert" on the graduation project of my then-colleague <a href="http://twitter.com/baarserik">Erik Baars</a>. His topic was "contributing to Open Source" and parts of this blogpost owe some credits to Erik for inspiring me to write about this topic. Just as a FYI: Erik Baars graduated on the topic, so you can always bother him on Twitter if you want to :)</p>

<h2>Finding the right project</h2>
<p>First and foremost, it is important to find the right project to contribute to. This in itself is hard for some people. There are so many Open Source projects out there, big and small, in many different languages, that it is very hard to find the right project. Or is it? Of course you can just pick a random project you think is interested, and see how you can contribute to that project. But what helps a lot is to find a project that you work with on a daily basis, or that you think you will work with on a daily basis once it's more mature. That way, you won't just look at the project from the developers point of view, but also from the users point of view, which is important when contributing to a project.</p>

<p>You don't have to limit your contributions to a single project either. There is no limit on the amount of projects you can contribute to. And as we will see later on, contributing doesn't have to be a full-time job either. Your contributions can take perhaps only 5 minutes. So depending on the way you want to contribute to Open Source and the amount of time you're willing to spend on it, your contributions could all be to the same project, or you could contribute to hundreds of projects in some way or another.</p>

<h2>Disclaimer</h2>
<p>I have found that contributing to Open Source is something that can be highly addictive, and you can end up spending most of your time to contributing to Open Source in one way or another. Depending on your point of view, there might be nothing wrong with this, or this could be a big problem. Consider this before starting though ;)</p>

<h2>How to contribute to Open Source</h2>
<p>There are many ways to contribute to a project. Some are more obvious than others. Let's first make a small list, and then dive deeper into them:
<ul>
<li>Core code/patches</li>
<li>Plugins</li>
<li>Documentation</li>
<li>Promotion</li>
<li>Deliver presentations</li>
<li>Support</li>
</ul>
There may be even more ways to contribute than this, so feel free to comment on this message with your ways of contributing to an Open Source project</p>

<h3>Core code/patches</h3>
<p>For most developers, the ultimate way to contributing to Open Source is to contribute code into the core of a project. This can be done in several ways. Most of the times, it starts with contributing patches to the core team of a project. This can be in the form of patches that are attached to tickets in the issue tracker of the project (this may be a bugfix, or the implementation of a new feature). Usually it is good practice to discuss your proposed solution on the developer mailinglist of the project to see if your approach is the right one, before spending time on it, but of course you're free to submit patches as soon as you've written the code. Thanks to Distributed Version Control Systems and sites like <a href="http://github.com/">Github</a>, this process is made even easier. To take Github as the main example, they have a system of pull requests, where you can fork a project repository, push your changes to your own fork and then use the "Pull request" option to ask the maintainer of the code to integrate your changes into the main repository.</p>

<p>In many projects, regular contributors of good patches are invited into the project's core team (a so-called <a href="http://en.wikipedia.org/wiki/Meritocracy">Meritocracy</a>). Other projects have a proposal-system where you can propose your own additions and become the core contributor and maintainer of this specific part of the code.</p>

<h3>Plugins</h3>
<p>Another way for developers to contribute code to their favorite project is to write plugins for the project. This allows a developer to publish code for their favorite project without the need of actually getting it into the core. Sometimes this is because the code does not belong in the core, in other projects the process of getting code into the core is tedious and long, and releasing something as a plugin allows users to use it much faster and saves the developer from the process of getting it into core. Always check for your contribution what is the best way: Sometimes the project core is *the* place for it to reside, and releasing it as a plugin is a shame of the effort of writing the code.</p>

<p>Some examples of projects for which writing plugins would make much sense: <a href="http://www.symfony-project.org/plugins">Symfony plugins</a> are very easy to install and use and can save users of the framework a lot of time, or browser extensions such as <a href="https://addons.mozilla.org/en-US/firefox/?browse=featured">Firefox Add-ons</a> are perfect for sharing your useful code with the rest of the world.</p>

<h3>Documentation</h3>
<p>The most underrated part of any project, be it open or closed source, is documentation. Documentation in any form, that is. And there are many forms of documentation: User manuals, reference guides, blog posts. If in some way or form, your text contributes to the users being able to find their way around the project more easy, your text can be seen as documentation.</p>

<p>First: User manuals. This is usually documentation written by the core team, but more often than not the documentation is either available in a repository that is open for changes (though often you have to be approved first). At least, you can always send in patches for improvements to the documentation or send your idea for changes to a mailinglist. Documentation is often one of the last activities for Open Source projects (because they usually focus on coding) so may very well be missing for (parts of) the Open Source project, so any help is very much appreciated. And your help will not just help the project, but also the rest of the community of that project, because it makes it much easier to use the product. And if that is not yet enough reason to help with the documentation, it is also very interesting for you: Writing (a part of) the user manual will force you to understand the application more than the average user, making you understand the system even more than usual. It may even help you find new features that you didn't know before.</p>

<p>Reference guides are usually automatically generated by the project, or sometimes they are enriched afterwards by users. But there are many other forms of "reference", such as code snippet systems and complete reference books. Most of the times, there is room for user contributions in the form of comments, patches, and code snippets, which are usually contributions that don't take much time to write. If you're interested in more work, then sometimes it is possible to contribute to the project of writing a book or more verbose reference guide.</p>

<p>Another way of documenting which some people don't really consider documentation but definitely is, is writing a blog post. You can write blogposts about new technologies that you find, solutions to annoying problems you encountered, cool new ways of using an Open Source project, and many more topics. All of these will usually contribute to the project either by promoting the project or helping users find the solution to their problem or inspiration for something new they want to do. So even though these blogposts are usually not published on the project website (except perhaps a blog aggregator/planet), it can still be considered a contribution to the project because your blog will be findable through search engines and might even be linked on the project website.</p>

<p>If you are good at writing, you could even write an article for a magazine (either print or digital). Most magazines have ways of submitting article ideas for publishing in the magazine, so if you are interested in that, find a suitable magazine and submit your idea for an article for consideration to the editors of the magazine.</p>

<h3>Promotion</h3>
<p>Promoting a project is always a good way of contributing. There are many ways to do that, including writing blog posts, convincing your manager the Open Source project should be used for work, visiting local (user group) meetings, organizing local introduction nights or training sessions, speaking at user group conferences or conferences, anything basically that points some more attention to the project. Always do this in a friendly way and respect it when people choose not to use the project, either because they don't need it or they choose another (competing) product. In the end it's always their choice, but it is good to have heard your opinion/story.</p>

<p>In a more practical way as a direct contribution, some projects have a section on their website where the project publishes case studies of the use of the project in real-world situations. If you have a good example for the use of the project and the project has such as section on their website, do not hesitate to contact the project team to see if they want to publish your case study. Usually they will be happy to work with you on getting the case study published on their site.</p>

<h3>Deliver presentations</h3>
<p>If you feel comfortable speaking to a group of people, you could consider delivering presentations on your favorite project at (user group) meetings and bar camps or conferences. When you're just starting off as a speaker, it is usually a good idea to either get in touch with your local usergroup to see if they have a meeting scheduled for which they are still looking for speakers, or to visit a bar camp. These events are a very informal environment in which a starting speaker can get some practice and often also get very good feedback from attendees. First, think of a good subject to speak on, work this out into at least a basic summary of which topics you want to cover and then contact those who organize the meeting/bar camp to see if you can speak there.</p>

<p>Once you have some speaking experiences in front of smaller groups of people, then start submitting your proposals to conferences. Submitting to conferences local to you has the most chance of you being selected because it's cheaper for the organization to get you in than to fly someone from the other side of the world. Of course this does not mean you should not submit to conferences further away, but the chances are smaller to get accepted there because of the high cost involved in getting you to the conference. In the Open Source world, conferences usually don't pay their speakers a fee for speaking (aside from reimbursements for travel and hotel, which is usually arranged for speakers). As soon as you've spoken at a few conferences and you've made a bit of a name in the speaking world, chances will be bigger that you get accepted at confererences across the ocean. If you want to get accepted for such conferences, it is usually a smart move to submit multiple proposals. Because the price for overseas speakers is higher than for local speakers, conference organizers usually try to get overseas speakers for at least two talks because the price per talk is lower in that way. Personally, I usually submit between 4 and 7 proposals for most conferences, to give the conference organizers a good range of topics to choose from, and also to get the chance to be accepted for more than one talk.</p>

<p>Where user group meetings and bar camps are informal and usually free or cheap to attend, be aware that in the case of conferences, people usually pay to attend the conference. Their expectations will be higher, they will expect more quality from your talk. So make sure you come well-prepared and know your topic well.</p>

<h3>Support</h3>
<p>Whether you're just starting out with a project or you've used it for years, support is one of the easiest ways of contributing to a project. As soon as you have at least some basic experience with the project, you can answer questions of other (starting) users on the mailinglist, in the IRC channel and/or on project forums. When someone asks a question that you know the answer to (or know where to find it), just respond with your answer. Be clear, be friendly and make sure you understand the question before answering, and people will appreciate your response a lot. It will help them out in solving a problem or finding a good solution to a challenge they were facing, so your response will be very welcome. Even if it doesn't fully solve their problem it might just give them a new insight into where the solution may lie.</p>

<h2>It's not that hard</h2>
<p>So contributing to Open Source is not that hard. It may only take you a couple of minutes a week, or even a month. It may however also completely get you involved into a project, sometimes you may even be invited to join the core team of the project. Just make sure that whatever you do, do it in a friendly and positive way. This is the way you want to be treated, so make sure you also treat others the same way. After all, they're probably also contributors to the same project you work on.</p>
