---
layout: post
title: "Still looking for that perfect MP3 player?"
date: 2005-01-23 7:18
comments: true
categories: 
  - leftontheweb 
---
If you are looking for an mp3 player, and also look to refresh your breath, then check <a href="http://web.media.mit.edu/%7Eladyada/make/minty/index.html">this one</a>. A very cheap way of getting your own nice mp3 player. And a fresh breath. (<a href="http://www.metafilter.net/mefi/38893">metafilter</a>)
