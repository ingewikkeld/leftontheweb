---
layout: post
title: "Hard to understand?"
date: 2005-01-15 4:36
comments: true
categories: 
  - leftontheweb 
---
When you see the text:

<span style="color:Red;font-weight:bold;">support only on the support forum</span>

What do you think? Let's send a Private Message to this guy asking for support? It seems some people do, because I get at least two PM's every day asking me for support on their problem. Amazing, isn't it?
