---
layout: post
title: "Make them pay"
date: 2011-07-06 7:00
comments: true
categories: 
  - php 
  - python 
  - community 
  - conference 
  - speakers 
  - joomladays 
---
<h2>Everybody pays</h2>
<p>The concept of "Everybody pays" is not new. It is actually quite normal <a href="http://jessenoller.com/2011/05/25/pycon-everybody-pays/">in the Python world</a> for community-organized conferences. It also depends on the conference. Nobody expects to get reimbursed for a barcamp or unconference. It's "part of the deal" when going to such events. In the case of community conferences it is in the PHP world different. Some conferences reimburse part of the cost, others pay the full cost, some of them (like PHPBenelux) try to spoil the speakers to death ;)</p>

<p>Most important thing to note, independent of which approach a conference takes, you can usually find information on how a conference is organized and what is reimbursed on the Call for Papers page of the conference, so you know what to expect when you submit your proposals. When a conference doesn't mention anything, you can ask but I usually assume that means they pay for at least hotel and travel, as that seems the most common deal in the PHP world.</p>

<h2>Speakers are always important</h2>
<p>Now I want to make something very clear: Speakers are always important. As a conference organizer having been involved now in all kinds of different events, I know that speakers are important for a couple of things. First of all, they determine the content of your conference, which in turn determines how interesting your conference will be. Second of all, who you invite also determines how many delegates you will get. A good mixture of "big" names and new speakers is important to get to the amount of delegates you're aiming for. So, speakers are very important. This does however not mean that you can always pay for all expenses. This highly depends on how many sponsors you get, how high your ticket price is, etc.</p>

<p>It is important to realize though that speakers put a lot of effort into preparing their talk, and make quite a few expenses to get to your conference, whether you pay their expenses or not. So it has always been my focus to try and reimburse as much as possible for speakers, and I think most conferences do. Eventually, though, you need to make sure you don't lose money when organizing a conference, so choices are necessary.</p> 

<p>Take <a href="http://conference.phpnw.org.uk/">PHPNW</a> as a good example. This is a conference that pays for hotel and offers a free conference ticket, but speakers have to pay for their own travel. Has that ever made me feel unwelcome? On the contrary! It is probably the only conference where I go whether I get accepted or not (I have just been lucky to have been accepted to often). I had already bought my ticket for this year's edition before I knew I was accepted, because I already knew I would be going anyway. PHPNW is probably one of the best conferences to be a speaker at, even if they don't pay for all of your expenses as a speaker. So it definitely is no requirement to pay for everything to be seen as a good conference for speakers.</p>

<h2>Would you pay for your own ticket?</h2>
<p>Another example: Earlier this year I sent in a proposal to the Dutch Joomla Days. A conference outside of my normal scope because I usually don't work with Joomla!, but recently I've come to know quite a few people there and the community is very nice. I was accepted into the conference, when I got an e-mail from one of the organizers asking me "Would you be willing to purchase a ticket for the conference?" It was in no way required for me to get a ticket, I would get access to the conference (well, at least the conference day I was speaking at), but they still asked, because it would help their budget. Fair enough, so I ended up paying for my day ticket.</p>

<h2>This is where you can go wrong...</h2>
<p>And this is where you can go wrong as conference organizers, and where I feel the earlier mentioned conference went wrong. They had a Call for Papers which at no point mentioned speakers were required to pay for the ticket themselves, yet <em>after</em> they accepted the speakers they mentioned that speakers should purchase a ticket as well. Now, I prefer the Joomla Days approach where you kindly ask the speaker to pay, but it is a valid choice for a conference organizer to simple require speakers to purchase a ticket. But if you do so, communicate this fact before speakers send in their proposals. Make it very clear in the CfP process what you ask of your speakers. Then, speakers can make the choice of submitting, knowing perfectly well that they will have to pay for their own ticket.</p>

<h2>Be explicit</h2>
<p>So I guess the most important lesson to learn from this is: If you organize an event of some sort, be explicit about what you ask of speakers and what they get in return. Well, not even just speakers. Also for delegates, staff, volunteers, everyone. And sure, assumptions are probably the root of all evil so if certain information is not available, people should not assume but ask about it, but if you're an experienced speaker, you start expecting certain things unless mentioned otherwise.</p>
