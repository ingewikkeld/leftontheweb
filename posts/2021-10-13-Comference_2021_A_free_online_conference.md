---
layout: post
title: "Comference 2021: A free online conference"
date: 2021-10-13 9:30:00
comments: true
categories: 
  - php
  - conferences
  - comference
social:
    summary: Comference 2021 is this week, and I'm really looking forward to it!
    highlight_image: /_posts/images/comference.png
    
---

Last year, as the global pandemic caused us to have to cancel [WeCamp](https://weca.mp/), we felt compelled to do something else. That something else became [Comference](https://comference.online/), an online conference from the comfort of your own chair, couch, bed, or wherever you want to comfortable see a nice set of interesting talks. In terms of content we wanted to stay close to WeCamp in that we felt tech was important, but there's many topics related to the tech world that are equally important and maybe not getting as much attention at tech conferences.

Our go/no-go moment for WeCamp is at the end of December or early January. This is because we have to reserve the WeCamp island and make a down payment on the reservation, but also because we start our search for both sponsors and coaches in January. So early this year we had to make the sad decision that 2021 was simply too early to start planning a new WeCamp. The result of that was, obviously, that we wanted to organize a new Comference.

Tomorrow we kick off that second edition of Comference, and I'm really looking forward to it. We have some amazing talks that I want to highlight here.

### Documentation

As much as developers don't want to hear this, documentation is important. I'm really looking forward to hearing [Milana Cap](https://twitter.com/DjevaLoperka) speak about documentation based on the documentation of one of the most important open source projects in the world: WordPress.

### Get rid of management

With our company [Ingewikkeld](https://ingewikkeld.dev) we've been working a lot for and with [Enrise](https://enrise.com) over the years. It is fair to say that Ingewikkeld wouldn't be where we are today without Enrise. We've worked on some amazing projects with them. Some years ago they switched their whole business to completely self-managing teams and it's been interesting to follow that journey. Now we'll have some people from Enrise telling us about that switch and the new model.

### Change

Change can be hard. If you're convinced a change is necessary, but the rest of your team or organization isn't convinced, it'll take a lot of hard work. [Jeremy Cook](https://twitter.com/JCook21) will tell us more about what you can do to get adoption for change without having to become some kind of dictator that pushes change down people's throats.

### No More Scrum?

Most companies I come into as a developer or consultant have adopted some form of scrum. [Jeroen de Jong](https://twitter.com/jcdejong) is at Comference to tell people to stop doing scrum. What? Yes, he'll share the story of how he told a team to stop down scrum and how that helped them to improve collaboration.

### Do you see the light?

Due to the pandemic a lot of people have started working from home more. And as the pandemic (hopefully) leaves this world, remote working seems to have gotten a stronger grip on the workplace. This is a great development, but it also introduces some challenges: How to get a good home office set up. [Camilo Sperberg](https://twitter.com/unreal4u) talks about a topic often forgotten: Lighting.

### Diversity

Diversity is a diverse (see what I did there?) topic. Last year we have Lineke talking about diversity from a completely different point of view, and this year [Andreas Heigl](https://twitter.com/heiglandreas) takes a completely different view on diversity again: How do we scale diversity on a global level?

### Don't believe the hype

APIs are here to stay, but we can certainly improve on how we create our APIs. [Tim Lytle](https://twitter.com/tjlytle) will be sharing information on how to make an API easy to traverse and ensure less problems with implementing APIs by introducing hypermedia.

### The fellowship

If you've been following me for some time you know that I simply _love_ the PHP community, and the concept of community in general. I'm really happy that one of the greats of our community, [Wasseem Khayrattee](https://twitter.com/7php), has agreed to do a talk about the community and what community can do for you as well as what you can do for the community.

### Isolation on an island?

Last but certainly not least, we dive into the world of open source. [Tonya Mork](https://twitter.com/hellofromTonya) and [Juliette Reinders Folmer](https://twitter.com/jrf_nl) are going to have a conversation about open source, and about how open source projects are not isolated islands, but together they are a constantly changing ecosystem of related projects.

## See you there?

The whole of Comference is free to stream on YouTube (we'll also add the video's to our website). If you have any questions or want to discuss the subjects of the talks with fellow attendees, feel free to join the [Comference Discord](https://discord.gg/JKq3fm2). That same Discord is also used during the game night to discuss what games we're going to play. See you there?