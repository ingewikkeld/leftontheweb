---
layout: post
title: "Symfony-framework.nl - The Community Edition"
date: 2008-08-09 13:54
comments: true
categories: 
  - symfony 
  - php 
  - community 
---
<p>With the worldwide growth of symfony, The Netherlands has not stayed behind. Usage in the Netherlands has grown, and with bigger sites like Dutch Cowboys and Kliknieuws moving to a symfony platform, I think symfony is starting to prove it&#39;s worth in the dutch web application world.</p><p>But with bigger sites moving to the platform, those are also proving the point I was trying to make with symfony-framework.nl: symfony *is* a platform that you can use for your applications. And with a growing number of developers using symfony in The Netherlands, it&#39;s not just advocacy that is needed, but also a central place for the community to hang out.</p><p>Which is why I started working on the next version of the site. Instead of a static one-way communication of reasons to use symfony and news, the site now offers more options for the community to interact. Besides the fact that you can now comment on newsitems, a new forum has opened, where you can ask questions (or help others), but also post job openings.</p><p>So I hereby want to invite all the dutch developers using symfony to come by and have a look. And of course, offer feedback and give input on new features you&#39;d like to see. This is definitely not the last update of the site ;) </p>
