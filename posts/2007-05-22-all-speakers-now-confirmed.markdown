---
layout: post
title: "All speakers now confirmed"
date: 2007-05-22 15:05
comments: true
categories: 
  - technology 
---
It seems <a href="http://www.phpcamp.nl/">it</a> is all I write about these days, but it's one of the things that keeps me busy for a good time of my current days, aside of course from my regular work, but I just wanted to share this. We've now confirmed all three speakers for our PHP Bootcamp event! Earlier, I announced Francois Zaninotto of Symfony and Thomas Weidner of Zend Framework, two speakers that I am already very proud of to be presenting. Today, our third speaker has confirmed: Joomla! will be represented by it's Lead Developer, Johan Janssens! 

This confirmation is all I needed to be extremely proud of the line up we have here, with key speakers from each of the framework communities! Amazing! This is going to be a massively great day! 

If you're in the neighbourhood of Leusden on June 2nd, or you just feel like coming over, feel free to register through <a href="http://www.phpcamp.nl/">our site</a>. There's still a few spots available for interested PHP geeks!
