---
layout: post
title: "Picasa for Linux!"
date: 2006-05-27 3:51
comments: true
categories: 
  - technology 
---
There is one single tool that rules the world for photo management. This tool is called <a href="http://www.picasa.com/">Picasa</a>. Up until now, it was only available for Windows, which sucked bigtime and I definitely liked the app. Big Google rules (again!). They <a href="http://googleblog.blogspot.com/2006/05/picture-this-picasa-for-linux.html">created a linux version</a>!

OK, not really. They made an installer that includes a specially configured <a href="http://www.winehq.org/">Wine</a> to run Picasa on Linux. But it functions just like a native Linux app!

So <a href="http://picasa.google.com/linux/download.html">go and download</a> your Picasa for linux :)
