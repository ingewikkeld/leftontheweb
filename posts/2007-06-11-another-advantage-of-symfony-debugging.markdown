---
layout: post
title: "Another advantage of Symfony: Debugging"
date: 2007-06-11 12:48
comments: true
categories: 
  - technology 
---
I'm not even talking about the unit testing and functional testing setup that Symfony has built in, as I haven't even had the time to use it (I know, bad bad bad, but sometimes you just can't do it when the needs for speedy delivery are high). I'm simply talking about the clarity of structure.

The application I'm dedicating most of my worktime on these days used to be built in Mambo, with components doing all the hard work. Not an ideal setup, because the underlying technology (Mambo) hardly offers a good guideline for the development. It also hardly offers any real utilities for writing clean code, such as the Symfony helpers, a clear MVC structure, or even a clear OO approach.

Over the past few work days, I've been focussing on debugging our first stable release. Where, with our previous (Mambo-based) version, when you had to debug something, you were spending more time searching for the bug in the code than fixing it. A LOT more time. Now, with this new Symfony version we have, because Symfony offers clear guidelines on project structure and code structure, because the MVC setup is so clear, and because code has it's clearly identifiable place within your source tree, fixing bugs is a matter of opening the right file, and fixing the code. This way, it's not a boring and annoying job anymore to do bugfixing. It's a joy to do it, since it's so darn easy to identify the offending code!
