---
layout: post
title: "WebKoffer-1.0beta1"
date: 2005-02-16 9:10
comments: true
categories: 
  - leftontheweb 
---
For a while already I've been working hard together with some other people to develop a new open source CMS. Yes, yet another CMS. I've tried a lot of software already, but nothing suited my needs. Well, you can now see the results of that hard work. Because the first beta release it now available. Download it <a href="http://www.webkoffer.org/downloads">here</a>. A bit of documentation can be found <a href="http://www.webkoffer.org/docs">here</a>. And if you find any bugs, please report them <a href="http://www.webkoffer.org/bugs">here</a>.

The main focus for us for the coming time will be fixing reported bugs and documenting the system. So in the near future, everything will hopefully make some more sense. :)

Anyway, come by, download, and start testing it!

And for those who wonder: yes, we WILL be getting a Real Design<sup>TM</sup>, but we probably won't be making that public until the release of 1.0 stable.
