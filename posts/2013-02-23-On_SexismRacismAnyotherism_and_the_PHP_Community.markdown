---
layout: post
title: "On Sexism/Racism/Any-other-ism and the PHP Community"
date: 2013-02-23 13:30
comments: true
categories: 
  - php 
  - community 
  - sexism
---
<p>The PHP community also has very little problems with sexism, racism or other isms, especially compared to other developer communities. We've had a few controversies, but (in my humble opinion) they were of little importance, or at least the "issues" they were about were relatively small. To my knowledge, we've had little to no (sexual) harrassment issues, racism or similar problems at PHP conferences and usergroups. I think as a community, this is something to be proud of. We welcome people of any gender, sexual preference, religion or lifestyle without judging them and without excluding them. Well, nearly.</p>

<h2>The PHPUK13 controversy</h2>
<p>Last night, Twitter exploded because of a t-shirt the Web&PHP magazine was handing out at the PHPUK conference. The t-shirt had the text "Enhance your php-ness" on it, a smart and (in my opinion) innocent play of words on "penis" of course. This caused quite some controversy, as some people decided this was sexist. Now, obviously, everyone is entitled to his or her opinion and I strongly feel the PHP community should respond to true sexism, but if an innocent joke like this is reason to raise hell on the publisher that gave away these shirts, I personally feel we're overreacting, and I wonder if this does not hurt the community more than it helps it.</p>

<p>Now, the above statement may need some explanation. I think controversies as this one hurts the community in that it makes people afraid to express their feelings, to make jokes, to offer their opinion. Freedom of speech is a beautiful thing, and obviously people who think the t-shirt is a mistake have the same freedom of expressing their opinion on the shirt, but the way in which it is done should also be considered. Last night on Twitter, I got the feeling a part of the PHP community was on witch hunt for a simple joke. People were bringing out their burning torches and stakes, and going after Web&PHP in a way I would expect if they had brought in exotic dancers to the conference social, or had told people that the tech-world is only for men. They did no such thing. They made an innocent joke.</p>

<p>People think differently. That's the world, and that's a good thing. I love Cal Evans, he is a dear friend to me, but on a political level, we are probably on opposite sides. In the usergroup I'm involved in, I work closely together on the sponsoring with someone from the complete opposite side of the Dutch political spectrum. Obviously, I work with the lot of you, most of whom eat meat, something I've not done for over 15 years now. Many of you are very different from each other, but we have something in common, and that's the love for PHP. It is OK to disagree with other people, but treat them with respect. And that, obviously, goes both ways. So when I see Cal Evans respond to Web&PHP with a tweet that says:</p>
<blockquote>
@webandphp saaaaaaaad
</blockquote>
<p>I don't feel this is a very respectful reply. On the other hand, Web&PHP responds to that tweet with just same level of respect by telling Cal to "get a grip":</p>
<blockquote>
@CalEvans get a grip it's a play on words. If this is all you have to worry about..it's not offensive to women or men, what is your problem?
</blockquote>
</p>

<h2>Think!</h2>
<p>My main point here is: Think! It <em>is</em> OK to disagree, that's what freedom of speech is all about. But the way you bring across the message makes a huge difference. I love how Amy Stephen picked up on the same controversy, by having a serious and polite discussion on Twitter, and by contacting Web&PHP in a very nice way:</p>
<blockquote>
@webandphp Would you kindly contact me? I want to talk about the penis phrase. I promise to be polite.
</blockquote>
<p>See? A polite response, asking Web&PHP to contact her (taking the discussion out of the public and thereby lessening the public controversy) to offer her view on things. An honestly great response to something she feels could've been handled differently.</p>

<h2>Let's agree to disagree</h2>
<p>Discussion and debate is good. It allows us to understand eachothers viewpoints and perhaps handle things differently in the future. This is an important part of our freedom of speech. Let's, however, not respond to situations where you disagree by taking up the stakes and torches immediately, banishing someone with a different opinion just because they expressed it in their way. Because if your goal is to make a certain group of people feel welcome in the community, you shouldn't do that by expelling another group of people. Take the approach Amy Stephen took, and e-mail someone who offends you to tell them how you take offense in their expression, suggesting other ways to say the same thing without offending you. And then accept it if their way of expressing things is different from yours.</p>

<p>How would you feel if we have dinner together, and the minute you order a steak, I start attacking you for eating meat? You may think it seems odd, but it is exactly the same situation as yesterday. You choose to eat meat, I choose not to. That shouldn't make me hate you for eating meat. You just have a different lifestyle than I do, and as long as you don't force me to eat meat, I won't tell you that you shouldn't. Well, sometimes I do, but I that in a joking way. I'm never serious. I don't want other people to adapt to my lifestyle just because I tell them to. So let's agree to disagree, and enjoy dinner together. In this same way, let's agree to disagree on whether the phrase "Enhance your PHP-ness" is funny or not. If you don't think it's funny, don't wear the shirt. If you think it is, wear the shirt. It's that simple.</p>

<h2>Odd timing</h2>
<p>Having said all that, I think the timing of this Twitter-riot is odd. Last year at ZendCon, Anna (the same Anna that was now at the Web&PHP booth at PHPUK) and other Web&PHP crew had a booth, and they were handing out this shirt:<br />
<br />
<img src="https://skoop.dev/images/webphp.jpg" width="450" /><br />
I have not seen a single comment back then on Twitter about this shirt, I don't understand where the controversy is coming from now. Let's all just get along, OK?</p>

<p><strong>small update</strong>: based on the comments I got on twitter and here, I feel the need to clarify one thing: as much as I don't mind the joke, I understand other people do. This blogpost is not defending either the joke or the initial response by Web&PHP. The way I worded things seem to have caused some confusion. My main point in this blogpost was the way things are handled and the way we communicate with eachother, as well as the fact that mutual understanding comes a long way to opening up the debate and hopefully solving the related (and in my opinion much bigger) issues at hand. I hope this clarifies some stuff).</p>
