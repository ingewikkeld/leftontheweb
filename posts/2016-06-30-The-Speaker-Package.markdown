---
layout: post
title: "The Speaker Package"
date: 2016-06-30 13:15:00
comments: true
categories: 
  - php 
  - speaking
  - conferences
  - speakerpackage
  
---

I've been meaning to write more about speaking recently, so after I [wrote about my personal CFP rule](https://skoop.dev/blog/2016/06/24/On-Submitting-To-CFPs/), let's write about a very related topic: The Speaker Package.

### What is it?

The speaker package is the term used for the package of reimbursements and other advantages you have as a speaker. This may (or may not) include a free ticket to the conference, travel and/or hotel reimbursements, a speakers dinner and some other things.

### Why is it important?

When submitting to a conference, it is important to realize what the speaker package consists of. For instance, if you can not pay for your own travel or hotel and don't have an employer that does so, you need to be sure that the conference can cover this. Also, for planning purposes it may be useful to know about speakers dinner, so that you block your calendar so you are able to attend it.

I made the mistake once of making assumptions about the speaker package (in this case: I assumed travel was being reimbursed) when submitting proposals to a conference. I got accepted, but then found out my flight was not covered by the conference. Because of that, I had to cancel that conference. Cancelling a conference is never fun, but especially not because of something like this.

### Speaker package unclear? Ask!

If something is unclear about the speaker package, do not hesitate to contact the conference about it. I've found that many conferences these days use [OpenCFP](https://github.com/opencfp/opencfp) for their call for papers. The OpenCFP standard template contains some information about the speaker package, including:

  > Complimentary airfare/travel (according to conference policy)
  
Unfortunately, conferences usually do not elaborate on what this conference policy actually is. Since some conferences only give partial reimbursements for airfare, you're never really sure how much of the flight you're going to pay for yourself. If a conference has this standard text and no additional information on their reimbursement policy, just get in touch with them! Get them to clarify this before submitting to their CFP.

### Another personal rule

In my previous article I explained about my personal rule to not submit to a conference unless I'm sure I can make it if I get accepted. I have another personal rule, this one concerning the speaker package:

  > I will not submit to conferences that do not make an effort to reimburse their speakers
  
Unfortunately, there are conferences out there that do not offer to pay for anything for a speaker. These conferences mostly seem motivated by the idea that "you're already coming to this conference anyway, so become a speaker while you're here". I can sort of understand this sentiment, but I personally feel like this implies a certain lack of respects towards the speakers. 

Speakers invest a lot of their time in a conference. They spend hours, sometimes days on preparing a talk, creating slides, rehearsing it, finetuning it, submitting it to conferences. They also spend a lot of time on the conference: car trips or flights, actually being at the conference, talking to people before and after their talk. They even spend money because they come to the conference: They need to eat and drink, park their car, etc. If the speaker is a freelancer or entrepreneur, they'll also miss income because they can not make billable hours while at the conference. All this is a huge investment, to be part of the conference. Speakers are usually passionate and willing to do this, but when conferences do not even make an effort to reimburse at least part of this, this is a reason for me to not submit to a conference.

### It is the effort that counts

I'm not saying that all costs should be reimbursed. As I mentioned before, speakers are usually passionate and willing to invest in conference speaking. I know _I_ am more than willing to do this. I've spoken at conferences where travel is not being reimbursed (the [best PHP conference in the world does not reimburse travel](http://conference.phpnw.org.uk/)) and I've spoken at conferences where I offered to pay for travel (because the conference offered sponsorship packages in return for covering travel). All of these conferences have one thing in common: They all offer to pay for (part of) the cost of speaking. And in case of [PHPDay](http://phpday.it/), they even offer a valid way out of their reimbursements by offering sponsorship in return for not having to reimburse. It is the effort of trying to cover expenses that counts to me.

### So now what?

Next time you open the CFP page for a conference, look for the speaker package information and make sure that it meets your requirements. If you have an employer, talk to them to figure out if they can perhaps cover part of the cost, so you can help make the conference an even bigger success. If anything is unclear in the speaker package information, get in touch with the conference to clarify before submitting. And if it doesn't meet your requirements, simply don't submit.