---
layout: post
title: "Wim Dani&amp;euml;ls - Werk-woorden"
date: 2006-03-13 13:21
comments: true
categories: 
  - books 
---
A small little booklet in between about dutch words that are only in use in a single company or even one or a few departments of companies. Full of funny stories on the origins of such words, I definitely enjoyed this one.

There is little more to say about this I guess. It was a fun read, but nothing more than an 'in-between'. I'm now on to read Salvatore's The Two Swords, which has been waiting in my bookcase for way too long due to all kinds of other book discoveries through <a href="http://www.bookcrossing.com/">BookCrossing</a> came in between me and this one.
