---
layout: post
title: "Application-level Logging with the Zend Framework"
date: 2006-09-07 3:33
comments: true
categories: 
  - meta 
---
My second article for A/R/T is online! <a href="http://hades.phparch.com/ceres/public/article/index.php/art::zend_framework::application_level_logging">Application-level Logging with the Zend Framework</a> is, as the title already notes, an article about application level logging. The Zend Framework comes in mainly at the end, where I use some examples to show how the actual logging works.
