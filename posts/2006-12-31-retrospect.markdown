---
layout: post
title: "Retrospect"
date: 2006-12-31 4:29
comments: true
categories: 
  - year

---
2006 is nearly over, so it's time to look back a bit. What happened here in 2006? I'll link to some articles that I feel give a nice overview of things that affected my past year.

* <a href="https://skoop.dev/article/177/creating-your-own-rss-aggregator-with-drupal">Creating your own RSS aggregator with Drupal</a>
* <a href="https://skoop.dev/article/182/logging-in-dutch-english">Logging in English</a>
* <a href="https://skoop.dev/article/184/java-training">Java Training</a>
* <a href="https://skoop.dev/article/186/music-to-code-to">Music to code to</a>
* <a href="https://skoop.dev/article/193/hermann-hesse-siddhartha">Hermann Hesse - Sidhartha</a>
* <a href="https://skoop.dev/article/197/kubuntu-linux-my-new-os">Kubuntu Linux, my new OS</a>
* <a href="https://skoop.dev/article/203/loose-change">Loose Change</a>
* <a href="https://skoop.dev/article/205/zend-framework">Zend Framework</a>
* <a href="https://skoop.dev/article/212/a-man-shot".A Man Shot</a>
* <a href="https://skoop.dev/article/217/paradise-here-i-come">Paradise Here I Come</a>
* <a href="https://skoop.dev/article/223/changes">Changes</a>
* <a href="https://skoop.dev/article/234/tomtom-home">TomTom HOME</a>
* <a href="https://skoop.dev/article/242/maria">Maria</a>
* <a href="https://skoop.dev/article/260/byebye-tomtom">ByeBye TomTom</a>
* <a href="https://skoop.dev/article/279/text-link-ads-earn-money-from-your-website">Text Link Ads: Earn money from your website</a>
* <a href="https://skoop.dev/article/282/unicron-broke">Unicron broke</a>
* <a href="https://skoop.dev/article/286/responsibility".Responsibility</a>
* <a href="https://skoop.dev/article/287/rebooted">Rebooted!</a>
* <a href="https://skoop.dev/article/288/netlabels-and-reviews">Netlabels and reviews</a>
* <a href="https://skoop.dev/article/289/protecting-forums-against-spammers">Protecting forums against spammers</a>
* <a href="https://skoop.dev/article/291/symfony-no-cacaphony-of-code">Symfony: No cacaphony of code</a>
* <a href="https://skoop.dev/article/305/a-say-day">A sad day</a>

To also look forward a bit to the next year: Things will change. The company I currently work for is in the process of splitting up. One of the owners is taking one of the biggest projects we have and will be operating that project from a seperate company. I have the choice to either go with him, and be able to develop solely on that project (the project that I've been working on nearly since I started) or to stay with Dutch Open Projects and go more and more into my role of Lead Developer there. It will be a tough choice.

Furthermore, some of the projects that I've been working on or thinking about will hopefully see the light of day in the coming year. This even includes a new music project! 

Lots of things for the next year... let's see what the year will bring.

