---
layout: post
title: "Sometimes"
date: 2004-11-27 10:15
comments: true
categories: 
  - leftontheweb 
---
Sometimes something happens, and all of a sudden you realize you're very lucky. Today I was in a small car crash. The car is gone, total loss. I don't even have a scratch.
