---
layout: post
title: "Installing the Geoip PECL package for Zend Server on OSX"
date: 2012-01-11 19:20
comments: true
categories: 
  - pecl 
  - geoip 
  - zendserver 
  - php 
  - i386 
  - re2c 
  - community 
---
<p><strong>Note:</strong> I am using Zend Server Enterprise on my local machine. While I don't think the steps will be different for Zend Server CE, you might want to check to make sure.</p>

<h2>Starting out</h2>
<p>When you try to just install the Geoip package using pecl install, the installation will fail. Initially, the errors might seem a bit cryptic, but if you look back a bit in the log of what the installation did, you'll notice a couple of things. First thing is a missing 're2c'.</p>

<h2>re2c</h2>
<p>I simply <a href="http://re2c.org/">downloaded the source package</a> and compiled re2c using the simple commands:</p>
<pre>
./configure
make
sudo make install
</pre>
<p>This worked for me. Next time I tried pecl install geoip, the log shows it found re2c. BAM!</p>

<h2>Geoip</h2>
<p>The next error was slightly more cryptic:</p>
<pre>checking for geoip files in default path... not found
configure: error: Please reinstall the geoip distribution</pre>
<p>After some searching a bit of help from my current colleagues, I found out this refers to the <a href="http://geolite.maxmind.com/download/geoip/api/c/">MaxMind GeoIP API</a> package. I downloaded the source for that, checked the README.OSX and found a nice shell script which will do most of the work for you. So I issued:</p>
<pre>
sh README.OSX
sudo make install
</pre>
<p>I got no errors at all during the installation, so I assumed the installation went fine. And indeed, running pecl install geoip now worked!</p>

<h2>Enabling the extension</h2>
<p>Last step left now is to enable the extension. Lazy as I am, I used the Zend Server control panel. I went to the Server Setup tab, to the extensions option. The listing now included the geoip extension. I clicked the "turn on" link, then "restart PHP" (whoever thought of the term "restart PHP" should seriously consider another carreer ;) ). Unfortunately, "The system could not load this extension".</p>

<h2>Building for the right architecture</h2>
<p>Trying to Google for this error, I didn't really get much further. The error is too generic to really find any useful information. Asking around on Twitter, I luckily got some useful information that I could use in the search. <a href="http://twitter.com/old_sound">Alvaro</a> mentioned the files might not be built for the right architecture. Being on OSX Lion, I would assume everything was built for 64bit. And indeed, I could confirm this with:</p>
<pre>
file /usr/local/zend/lib/php_extensions/geoip.so 
/usr/local/zend/lib/php_extensions/geoip.so: Mach-O 64-bit bundle x86_64
</pre>
<p>Next step was to check whether Zend Server perhaps was not 64bit, and indeed, this was the case!</p>
<pre>
file /usr/local/zend/bin/php
/usr/local/zend/bin/php: Mach-O executable i386
</pre>
<p>So apparently, I had to build the geoip extension for i386 instead! Again, I had to ask around a bit to do this, and <a href="http://twitter.com/neorey">Jeroen</a> hinted me to the CFLAGS. Of course, I had to use this both on the GeoIP API and on the PECL extension. So, I had to reissue the commands. <strong>Note:</strong> just rebuilding the Maxmind GeoIP API didn't do the trick for me. I had to remove the directory and unpack the tgz again for this to work.</p>

<p>Before building the MaxMind GeoIP API, I opened the README.OSX file that contained the installscript. I updated it to be as follows:</p>
<pre>
export GEOIP_ARCH='-arch i386'
export MACOSX_DEPLOYMENT_TARGET=10.7
export LDFLAGS=$GEOIP_ARCH
export CFLAGS="-g -mmacosx-version-min=10.7 -isysroot /Developer/SDKs/MacOSX10.7.sdk $GEOIP_ARCH"
./configure --disable-dependency-tracking
perl -i.bak -pe'/^archive_cmds=/ and !/\bGEOIP_ARCH\b/ and s/-dynamiclib\b/-dynamiclib \\\$(GEOIP_ARCH)/' ./libtool
make
</pre>
<p>Not that I didn't just change the GEOIP_ARCH, but I also updated the version numbers in the SDK line. Then I ran the script and ran make install.</p>

<h2>Reinstalling the PECL extension</h2>
<p>Then I uninstalled the PECL geoip package to remove the old version:</p>
<pre>
sudo pecl uninstall geoip
</pre>
<p>After doing that, I reinstalled the package again, but specifying i386:</p>
<pre>
sudo CFLAGS="-arch i386" bin/pecl install geoip
</pre>
<p>I then restarted Apache, refreshed the extensions page in the Zend Server control panel, and lo and behold: It said the extension was loaded and ready to use. Then I ran the script I needed to work on, and it didn't error out anymore on the geoip functions missing. It worked!</p>

<h2>One thing to note</h2>
<p>In the above, for all PECL commands I did, I used the PECL binary in /usr/local/zend/bin/pecl. It is important to use this version if you're using Zend Server, because it is the same build as the other stuff, with the right configuration. I don't know if it would work with another PECL installation, but better be safe, right?</p>
