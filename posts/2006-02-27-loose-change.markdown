---
layout: post
title: "Loose Change"
date: 2006-02-27 17:01
comments: true
categories: 
  - politics 
---
You may have seen it already in my 'The Links' section: <a href="http://ryanmeehan.newsvine.com/_news/2006/02/18/101791-unbelievable-911-documentary-available-for-free">Loose Change</a>. I am definitely impressed with the information that this documentary brings. Though most of it isn't new information, the fact that they bring it in quite a structured way, reminding us of all the gaps and plain untruths of the official US government statements regarding 9/11 gives me a feeling of awe. How the hell can they do this? How the hell can this happen without the US people not starting a revolt?

<a href="http://video.google.com/videoplay?docid=-5137581991288263801&q=loose+change">Download this documentary</a>. It's worth it. Yes, it's maybe taken into the extreme in terms of bringing the information, giving only the conspiracy theory side, but there are some very valid points that are being made here.
