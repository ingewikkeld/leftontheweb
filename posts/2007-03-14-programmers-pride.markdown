---
layout: post
title: "Programmer's Pride"
date: 2007-03-14 8:04
comments: true
categories: 
  - technology 
---
Here's some Programmer's Pride for you. As of today, I have been one of the main developers of a system that <a href="http://www.prnewswire.com/cgi-bin/stories.pl?ACCT=104&STORY=/www/story/03-14-2007/0004545831&EDATE=">is used by more than 1 million users worldwide</a>. Now, that is what I call cool.

<a href="http://www.tomtom.com/home">TomTom HOME</a> is a desktop application that talks with the TomTom servers to fetch updates and information (using XML), and allows you to purchase new content and directly install it. One of my main tasks at TomTom was building the XML server, from scratch. 

Not long after the launch, it already became clear that the system would be popular. The userbase grew enormously fast, at times too fast. There were some server problems, which has been solved quite some time ago already and now everything is running wonderfully fine. 1 million users is a milestone though that I can not just let pass. It is amazing to be able to say you have developed a system that is used by this many people, around the globe.

So yeah, I'm quite proud about this, still. Even though I'm not with TomTom anymore, it's still a wonderful thing to see the system hit this milestone.
