---
layout: post
title: "The Ultimate Goals"
date: 2006-11-07 14:34
comments: true
categories: 
  - personal 
---
Having just switched jobs (and especially the period before switching) has given me quite some moments of thinking about my future, and my career. What are my goals for my professional life?

In <a href="http://www.dop.nu/">DOP</a> I found one of those goals. A challenge both on the technical level as on the organizational level. The role I have there has many aspects, from the development of nice PHP applications to professionalizing the whole development process by implementing a coding standard and trying to get knowledge sharing on the road.

But as nice as the job at DOP is, it is not my ultimate goal. There are two companies where I would love to work at some point in my career. They both have a big name in terms of (open source) development and technological advancement.

<li><a href="http://www.google.com/">Google</a> is probably high on the list of many a developer as favorite potential employer. What a great company with great products. That would be a great company to work for.</li>

<li>The second company, which I would probably even prefer above Google, is <a href="http://www.zend.com/">Zend</a>. The company behind PHP is the ultimate in the PHP career world if you ask me. Unfortunately, all the development and R&D jobs are either in Israel or in the USA. Maybe some day, they will open an office in The Netherlands, or allow me to work for their Professional Services department from here? Nobody knows.</li>

I can dream, can't I? ;) 
