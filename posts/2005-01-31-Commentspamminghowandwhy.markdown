---
layout: post
title: "Comment spamming: how and why?"
date: 2005-01-31 9:16
comments: true
categories: 
  - leftontheweb 
---
The Register published <a href="http://www.theregister.co.uk/2005/01/31/link_spamer_interview/">an interview with a link spammer</a>. You know, one of those annoying people who spams your comments with links to all kinds of porn sites or casino's. It's an interesting read for everyone with a weblog.
