---
layout: post
title: "Ian Caldwell and Dustin Thomason - The Rule of Four"
date: 2006-07-25 14:00
comments: true
categories: 
  - books 
---
Compared by many to the Da Vinci Code, expectations for this book were quite high for me, as I really enjoyed reading the Da Vinci Code. But if you start out that way, you may be disappointed. It's not that this book is bad, not at all, it's just quite different from the Da Vinci Code.

I do understand the link between both books. Both handle with historical happenings, with mystery, with the main characters puzzling their way toward some ultimate solution. However, where the Da Vinci Code is much more about action, about fast-paced happenings, The Rule of Four takes a different direction. The Rule of Four is more of a brainteaser, with all kinds of puzzles and less action but more psychological impact. 

The fact that this book is mainly about the obsession of the main characters with an ancient book will definitely be a reason why this book more than the Da Vinci Code was positively welcomed by fanatic book readers. It definitely worked for me ;)

So, to wrap it up: I have mixed feelings about this book. Most feelings are positive, the book is well-written, the story is good, the characters are great. The negative feelings are mainly because of the expectations I had beforehand. So basically, it's my fault to be influenced by what other people say :)
