---
layout: post
title: "The Next Step"
date: 2019-06-27 12:00:00
comments: true
categories: 
  - php
  - ingewikkeld
  - changes
social:
    summary: It's almost 9 years since we started Ingewikkeld, and things will be changing a bit this year. This is the first visible change...
    
---
Nearly 9 years ago [Marjolein](https://twitter.com/TearSong) and I started [Ingewikkeld](https://ingewikkeld.net/) together. It was mostly a joint freelance business, we weren't planning on having people work for us. My main focus was PHP development, and I wanted to help customers with their PHP-related problems, whether that was architecture, development, training. Anything related to PHP, really.

When we started, I really only focussed on my freelance PHP work, but at some point there was so much work, and I had to say *no* to so many potential clients, that we hired people. It started out as an interesting idea, but as soon as [Jelrik](https://twitter.com/jelrikvh) was on the job market, things went quickly. Only 2 months later, [Mike](https://twitter.com/mvriel) joined as well. 

Jelrik has since moved on, but Mike (who I've been friends with even before Ingewikkeld was started) stuck around. He's still with Ingewikkeld. And over the years, Mike turned out to really complement my chaotic nature. And this has triggered a change at Ingewikkeld.

Some time ago already we started preparations for several changes to the Ingewikkeld company structure. I'm happy to announce today that we've set the first step, by adding Mike to  the Ingewikkeld leadership. The new Ingewikkeld leadership will be:

- Stefan Koopmanschap: Business director
- Marjolein van Elteren: Creative director
- Mike van Riel: Technical Director

I am really happy with this first step. And it's not the last. More things will change in the coming time, to make Ingewikkeld an even more solid business delivering even more quality services. 