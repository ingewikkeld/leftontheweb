---
layout: post
title: "Twig: Don't forget to close your tags"
date: 2011-12-06 13:37
comments: true
categories: 
  - twig 
  - php 
  - symfony 
  - Symfony2 
---
<p>The error was a relatively simple error:<br />
<img src="/images/twigexception-block.png" alt="" style="width:475px;" /><br />
I couldn't figure out why it was complaining about the endblock tag. It was there and it looked correct. After much trial and error, I spotted the mistake. It was a stupid and very simple mistake: instead of &#123;% endtrans %&#125;, I actually had a second &#123;% trans %&#125;-tag in my template. Stupid! So, if you run into the exception <i>Unknown tag name "endblock" in &#123;&#125;</i>, start scanning your template for tags that haven't been properly closed. That should help you solve your problem :)</p>
