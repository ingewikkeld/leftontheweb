---
layout: post
title: "PHP on Azure contest: The cool stuff"
date: 2011-02-23 13:16
comments: true
categories: 
  - php 
  - symfony 
  - zendframework 
  - contest 
  - azure 
  - winphp 
  - microsoft 
---
<h2>Step out of your comfort zone</h2>
<p>Last year you might've seen several blogposts here about my experience working with PHP on Windows. It was because I participated in the <a href="https://skoop.dev/tag/winphp">WinPHP Challenge</a>, another coding contest which was organized by Microsoft Netherlands. One of the reasons I participated in this contest was because I wanted to step out of my comfort zone. I had been working with linux and OSX for years and had not worked with Windows for a while. My last experiences with Windows and PHP (back in the days of PHP3, maybe early PHP4) had not been very good ones. Being forced to work with PHP on Windows was an eye-opener for me in how much PHP on Windows had improved.</p>

<p>Aside from working with Windows again, it was the first time I seriously did some work with any "cloud" technology. This was also outside of my comfort zone, because I had not yet worked with any of this stuff anymore. I found out that in the end, this isn't as hard as I had thought it would be, especially because there's already libraries out there that can help you with your work. And that's definitely something to remember: There's libraries out there so even with programming contests like this one, it would be stupid to reinvent the wheel and write everything yourself.</p>

<h2>Work with cool new technologies</h2>
<p>With the WinPHP challenge, the only requirement was that the PHP app should run fine on Windows. In the end, my app ran fine on any platform that can run PHP, because the app itself wasn't very special in terms of code. In terms of functionality, however, I specifically chose to work not just with Windows, but with Windows Azure technology. Why? Well, I wanted to play around with the cool new stuff. Windows itself can be considered an "old" technology (with a newer version every few years), where Azure is a completely new technology. And since I'd never really worked with "the cloud" before (other than CDN hosting), I thought it would be fun to play around with Azure a bit.</p>

<h2>Win cool prices</h2>
<p>The first price I won was slightly different from the one in the PHP on Azure contest, but the biggest and most important part is definitely the same: A ticket, flight and hotel for the MIX conference in Las Vegas! Fucking Las Vegas! I'm going this year, and I'll definitely report back on my experience on this blog, but I'm really excited to be able to visit the MIX conference. Second and third prices are not bad either, with a nice Samsung Omnia 7 phone for second and Kinect sensor for third price. And I am happy that after a suggestion by yours truely, this year there's also a price for the top blogger. Blogging is part of the contest and blogging can now also get you a price! The person that blogged the best (most, most extensive, best quality content, you name it) will win a Samsung Omnia 7 phone!</p>

<h2>Have fun</h2>
<p>The most important reason is and will always be to have fun. It's fun to participate in a contest. To learn stuff, to develop stuff, to solve the problems you encounter. Communicate with the other contestants, see what progress they make, check how they solve their problems. It's all about fun. So definitely have fun as well!</p>

<p>So <a href="http://www.phpazurecontest.com/wp-login.php?action=register">register now!</a> Join the fun! Let's make this the coolest contest ever, with the coolest projects and the coolest people around!</p>
