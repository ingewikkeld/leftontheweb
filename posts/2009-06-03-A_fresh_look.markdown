---
layout: post
title: "A fresh look"
date: 2009-06-03 21:09
comments: true
categories: 
  - leftontheweb 
---
Under the hood some things have changed as well. Most application code has been moved into plugins in anticipation of some other technical changes I&#39;m planning for the future. There&#39;s some slightly better caching here and there, and I&#39;ve got some administrative improvements done. All in all, I am quite happy with this new release of my code. If you have any feedback, feel free to post it in the comments :)
