---
layout: post
title: "Laurent Garnier cancels US tour"
date: 2005-02-03 2:45
comments: true
categories: 
  - leftontheweb 
---
French techno god Laurent Garnier has cancelled his upcoming US Tour. <a href="http://www.laurentgarnier.com/pgs/ustour.htm">His website tells us why</a>.

<blockquote>I am very sorry to have to cancel my forthcoming tour in the United States. I have decided to cancel further to what I consider to be completely unreasonable demands from the US Embassy in France in order to renew my working visa.

In order to obtain this new visa, the rules have once again changed since November 2004 and I would now have to not only fill in an exceedingly probing application form, but also be interviewed by a member of the Embassy staff, and provide proof of ownership of my house, details of my bank account, my mobile phone records, personal information on all my family members and more. I consider these demands to be a complete violation of my privacy and my civil liberties and I refuse to comply.

I am horrified by these new regulations and feel really sad that this is what some call freedom and democracy.

It has now become almost impossible for an artist to come and perform in the United States. And until this new legislation changes I will unfortunately refuse to comply with this nonsense.

Thank you for your understanding.

Laurent Garnier</blockquote>

Isn't it amazing that the US is so full of bullshit that a well-known techno artist can't even get into the US anymore without providing the most useless of private information to the US government?
