---
layout: post
title: "Symfony and DDD (and SymfonyCon)"
date: 2024-07-25 10:15:00
comments: true
categories: 
  - php
  - conferences
  - talks
  - DDD
  - Symfony
  - SymfonyCon
social:
    highlight_image: /_posts/images/symfonycon23-ddd.jpg
    summary: How do you combine Symfony and DDD? I will be explaining just that in a new full-day workshop, and I'll be doing that at SymfonyCon as well.
    
---

Last year at the SymfonyCon conference I [did a talk about Domain-Driven Design](https://skoop.dev/blog/2023/12/09/_SymfonyCon_follow-up_Doctrine_entities_vs_domain_entities/) in which I mostly focussed on the theory behind trying to understand what problem you're actually solving. I really enjoyed doing that talk, but I got some interesting questions afterwards about practical implementation of DDD when using Symfony. And I've also seen at several clients that people are always looking how they can actually implement a DDD approach when building software with Symfony.

That prompted me to consider what would be the best way to do this. There is so much to talk about and the answer to a lot of questions is "it depends". So I decided to develop a new full-day workshop about this. In this workshop, we take a Symfony project, and we start adding some functionality. 

I am still developing the contents, but things that will come by are amongst other things:

- How to split up domains
- Implementing a hexagonal architecture
- Doctrine entities vs domain entities and how to make that split
- How to implement communication between bounded contexts and domains

The workshop will contain a theoretical part per subject, but also exercises for people to actually get some experience with doing this, as I realize that some people learn more from listening to the theory and others learn more by doing.

If you are interested to get this training for your team, [shoot me an email](mailto:stefan@ingewikkeld.net). For those coming to Vienna for SymfonyCon, I do have some good news: [On December 4, I'll be doing this workshop at SymfonyCon](https://live.symfony.com/2024-vienna-con/workshop). You can already [book your tickets](https://live.symfony.com/2024-vienna-con/registration/). 

Will I see you in Vienna?