---
layout: post
title: "Issue tracking: Where business and technology clash"
date: 2007-06-14 12:12
comments: true
categories: 
  - technology 
---
Issue tracking: two words with a very wide definition. It's tracking issues found by development: Those things that you still need to improve or those bugs that you found yourself and still need to fix. Or even your TODO list, depending on your way of working. Then there's the issues that testing finds in your products. And last but not least: the issues that clients find in the software, either during the testing of one of your iteration results or even when the system is already in production. 

And especially in that last category, we find the first clash. Development (and testing also in some companies) need a system where they can register issues, track them, close them, keep track of which version/build is the affected version and in which version/build the issue was fixed, and if possible with integration with the version management system of choice (Subversion in our case). This is vital information for development.

Then there's the business. For the business, in particular account management and sales, it is very important to know what is happening for any given client. When a client calls, or when they go to visit a client, they need to have a clear view on what topics might come up or which topics really need to be discussed.

So, in a perfect world, there would be this central system that can do all these things. Unfortunately, especially in the open source world, there is no such things.

At my current employer, we use <a href="http://www.sugarcrm.com/crm/">SugarCRM</a> for customer relationship management. For most projects, the from a technical point of view completely inadequate bug report system of this CRM is used. For our biggest project, which is probably also the one we're currently handling in the most professional way, we use <a href="http://trac.edgewall.org/">trac</a> including the Subversion hooks. This works perfectly for me as a developer, but is giving account management and sales headaches.

So again, clashes may occur. In a perfect world, there would be one integrated system. Due to our Open Source philosophy, we prefer to stay away from the big commercial packages. Does anyone know of a good solution, aside from developing our own SugarCRM plugin or writing a full custom piece of software to handle everything?
