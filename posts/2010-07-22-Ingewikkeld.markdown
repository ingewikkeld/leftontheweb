---
layout: post
title: "Ingewikkeld"
date: 2010-07-22 9:04
comments: true
categories: 
  - php 
  - symfony 
  - ingewikkeld 
  - work 
  - projects 
  - freelance 
---
<p>It has been an interesting journey so far. After my Ibuildings adventure I found a new job at <a href="http://www.unet.nl/">UNET</a>, a relatively small but very professional broadband ISP mostly aimed at the business market. While the work there was awesome and the team was one of the best I've so far been a part of, I quickly felt the need for a bit more freedom. At the end of April of this year I handed in my resignation, and I was gone by June.</p>

<p>Luckily for me, I started my new adventure well. At the beginning of June I started my temporary contract at <a href="http://www.angrybytes.com/">Angry Bytes</a>, a web agency in Hilversum who are quite active in the dutch media market offering their great (but closed-source) Zend Framework-based ABC Manager CMS. Until last week I worked there full-time, and until the end of August I'll work there for three days a week. It's so far been a blast and we've already achieved quite some cool stuff in terms of both improvements of the existing functionality and implementation of new features. </p>

<p>As of last week, my first official freelance job started by me working for <a href="http://www.genj.nl/">G+J</a>, an international publisher whose Dutch offices are located in Diemen. The two days a week that I'm not working in Hilversum, I'm now working for them to help them with several tasks surrounding their new symfony-based CMS, including data migration from their old database to the new database, their caching strategy and possibly also some testing.</p>

<p>Overall, exciting times are ahead! I'm really looking forward to the coming period. And just to plug myself: I don't have any work (yet) starting early September, so if you're in need of a developer, consultant or trainer, then don't hesitate to contact me!</p>
