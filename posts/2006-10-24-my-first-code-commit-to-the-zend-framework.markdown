---
layout: post
title: "My first code commit to the Zend Framework"
date: 2006-10-24 12:31
comments: true
categories: 
  - technology 
---
Today, I have done my <a href="http://framework.zend.com/fisheye/changelog/Zend_Framework?cs=1336&csize=1">first code commit to the Zend Framework</a>. I recently joined Shahar in the development of the Zend_Http_Client component. The first commit is nothing spectacular, just a bugfix in the existing code. My first "real" commit, with code I've actually written myself, will follow shortly, when I finish Zend_Http_Client_Adapter_Curl, the Curl adapter for the Zend_Http_Client.
