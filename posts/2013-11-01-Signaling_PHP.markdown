---
layout: post
title: "Signaling PHP"
date: 2013-11-01 14:00
comments: true
categories: 
  - php 
  - cli 
  - book
---
Very recently, elePHPant herder [Cal Evans](http://blog.calevans.com/) released a new book: [Signaling PHP](http://www.signalingphp.com/). The book is about handling signals when writing PHP CLI scripts that are long-running scripts. One could think of a daemon-like script, a long-running importer script or similar applications. I considered reading it on my flight to [TrueNorth](http://www.truenorthphp.com/), but since I was waiting for my upgrade to Mavericks, I decided to read it before that already. This post gives a short review of this book.

Long-running commandline scripts
--------------------------------
The first thing I need to say is that I've written quite a few commandline scripts in PHP before, but never have I written long-running commandline scripts. One of the reasons for this, though, was that PHP has never been suitable for those kinds of things. Memory issues alone would stop me from doing this in PHP. 

However, as Cal writes about the release of PHP 5.3 (and above):

> Now though, we can write processes that span days or weeks. (...) Things that I once had to use a low-level language to do, I could now do with my old friend PHP.

Since I've not yet actually done this kind of scripting, and I don't have experience with it, this book was interesting enough to read. It has taught me stuff I can remember when I do get to the point where I want to write something like this.

Focus
-----
Before I go into the actual content of the book I must say that I really like the approach Cal took with having a very specific focus of the book. This book is just about a single topic. This is not "Commandline applications in PHP", but even more specific. It is only about catching signals sent to commandline applications in PHP. 

The reason why I like this is that I was able to finish the book in less than an hour. There's 34 pages, and that includes the title page, the dedication page, the Leanpub info page and some other non-content pages. Given the amount of "free" time I have, I like this approach because I'm much more inclined to pick up my iPad and read the book. 

Structure of the book
---------------------
The structure of the book is the only sensible structure. First setting up the project, then start with basic capturing of signals, and then moving on to the best practices. I also like the appendix about working with PIDs to make things even more clear. I personally don't see the use of Appendix B, which contains the full code sample of the command-class, since this code is also available online in a Github repository, but ah well, perhaps someone else has use of this.

The content
-----------
Obviously, I am very happy to see Cal using the Symfony2 Console component for his code samples. Not just because I love Symfony2, but also because it is an implicit reminder to people that they should not try to reinvent the wheel, but should reuse existing components. And an additional bonus is that this book is a great example of using just a single Symfony2 component instead of the full framework. 

The example is a very simple, basic example. This is obviously very useful since the purpose of the book is to explain a very specific topic. It is inspiring me to look beyond the basic example and start experimenting with other scripts that may want to catch signals. What I do think could've helped to give a better overview of what you could do with long-running scripts is to give some examples of what you could use it for. I like the examples in chapter 6 of what you can do with the signals, but I miss some examples of what you can do with long-running scripts.

I don't use long-running scripts, why would I get this book?
------------------------------------------------------------
The fact that you haven't written these scripts is exactly why you should get this book. This is not your average 1000-page reference guide that you'll have on your desk when you're developing complex applications. This is the book that introduces you into the world of signal handling in PHP. This is probably the fastest way to get the knowledge in your head so that once you really start needing the functionality, you already know it exists and you won't even consider trying to write your own way of doing things. And given the fact the book is available for a suggested price of under $5, this is the best-value you're going to get for getting this knowledge. Recommended reading for everyone.
