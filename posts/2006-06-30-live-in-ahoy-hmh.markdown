---
layout: post
title: "Live in Ahoy` &amp;#38; HMH"
date: 2006-06-30 14:48
comments: true
categories: 
  - music 
---
<img src="http://static.flickr.com/73/178634499_eeb1bb9fa7_m.jpg" align="left" alt="Patrick Dahlheimer on bass" /> Since quite a few years already my wife and I try to attend every concert (excluding festival appearances) by <a href="http://www.friendsoflive.com/">Live</a> in The Netherlands. So when we heard that Live would be doing first one show, then two, we quickly had our tickets. But we were definitely not prepared for what we were going to get...

Wednesday June 28. The minute I finish work I leave for Rotterdam, where Live is playing Ahoy`. Two support acts  open the evening. <a href="http://www.aballadeer.nl/">A Balladeer</a> is quite nice, and were able to get the (relatively small) crowd going. Proof of the fact that this concert was far from sold out: We came in while A Balladeer was already playing and we easily able to position ourselves nearly at the front, a few rows from the stage. Anyway, A Balladeer was very nice, and the crowd definitely seemed to enjoy their music.

Second support was the dutch singer <a href="http://www.charliedee.nl/">Charlie D
