---
layout: post
title: "You Need To Do What You Need To Do"
date: 2015-07-08 09:00
comments: true
categories: 
  - php 
  - theartofasking 
  - distractions 
---

One of the topics in [The Art of Asking](https://skoop.dev/blog/2014/12/24/the_art_of_asking/) that I think more developers should be aware of is that you need to do what you need to do to do your thing. This sounds vague, but it is true. In the book, Amanda Palmer uses several examples of how this works for artists, but I think it is equally true for developers (although not everyone understands this):

At some point, Amanda is having a coffee with Samantha Buckingham, another crowdfunding artist. She releases her music on [Patreon](http://patreon.com/), and Buckingham was afraid it would not sit well with her fans that she would post pictures of her sipping a mai tai. After all, it was her fans' money being spent on such a holiday. The response Amanda gives:

  > What does it matter where you are or whether you're drinking a coffee, a mai tai, or a bottle of water? Aren't they paying for your songs so you can... live? Doesn't living include wandering and collecting emotions and drinking a mai tai - not just sitting in a room and writing songs without ever leaving the house?
  
Then she goes on to describe how Kim Boekbinder, another crowdfunding artist, thinks about this:

  > Kim had told me a few days before that she doesn't mind charging her backers during what she calls her "staring-at-the-wall time," which she thinks is essential before she can write a new batch of songs.
  
This attitude is important for us as well I think. Someone, many developers I encounter seem to think that you need to work furiously on writing new code, at least 8 hours a day, to do your work well. But this is far from true. I touched on this subject in [this blogpost](https://skoop.dev/blog/2015/01/06/We_Are_All_Artists/), but programming is a creative job. This means that sometimes, you need to take a step back. Go out for a walk, play a game on your Wii, Playstation or XBox, read a bit in a book, find the activity you need to get that inspiration. And don't feel guilty about doing it.

In my previous project, the office cafeteria actually had a pool table. At least once a day, I'd go down to the pool table with a co-worker and play some pool. I surely wasn't banging on my laptop for 8 full hours every day, but the games of pool surely helped me sometimes in finding the solutions or cooling down after a frustrating bug arose. Sometimes, we just need this change of scenery. Remember, you get paid simply to get the job done, regardless of how you do it. If your employer or client gives you a hard time about it, just tell them that you need this in order to do what they pay you to do.

As Amanda Palmer puts it quite nicely:

  > The relative values are messy, but if we accept the messiness, we're all okay. If Beck needs to moisturize his cuticles with truffle oil in order to play guitar tracks on his crowdfunded record, I don't care that the money I've fronted him isn't going towards two turntables or a microphone. Just as long as the art gets made, I get the album, and Beck doesn't die in the process.
