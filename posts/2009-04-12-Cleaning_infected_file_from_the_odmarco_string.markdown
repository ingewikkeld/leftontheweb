---
layout: post
title: "Cleaning &quot;infected&quot; file from the odmarco string"
date: 2009-04-12 20:58
comments: true
categories: 
  - odmarco 
  - script 
  - virus 
  - malware 
  - clear 
  - dreamhost 
  - php 
---
<p>First of all, let&#39;s look at the hack. At this point I am unsure what exactly caused so many of my files to contain the odmarco string. It looks like <a href="http://discussion.dreamhost.com/showthreaded.pl?Cat=&amp;Board=forum_troubleshooting&amp;Number=117866&amp;page=&amp;view=&amp;sb=&amp;o=&amp;vc=1#Post117866" target="_blank">quite some people on DreamHost got hit</a>  by this problem, so I am guessing a vulnerable script on one of their servers caused this. Now, I should blame myself as well, because apparently I didn&#39;t take notice to a lot of files in my websites being writable by the server. It&#39;s no excuse, but a lot of the sites that I have are very old sites, where I definitely didn&#39;t pay as much attention to such details as I do now.</p><p>Anyway, from a <a href="http://www.siteadvisor.com/sites/odmarco.com/postid/?p=1512410" target="_blank">comment on siteadvisor</a>  I learned that the script called in the iframe is trying to abuse an exploit in Adobe Acrobat. Though I hate all stuff like this, I hate exploits more than mere referrer spam injection, so I felt I needed to take care of this quickly, even if a lot of the sites are hardly maintained anymore. So I sat down to hack up a little PHP script that would remove the offending string. Why PHP? Simple, it&#39;s the only language I know good enough to hack something like this up in a short time. </p><p>After some hacking around I came up with <a href="/clear_odmarco.phps" target="_blank">this script</a>. It&#39;s not perfect, but it does the job and it does it well, so I&#39;m happy. Anyone interested, feel free to use this script to clear up the mess in your site. It&#39;s meant to be run from the command line, inside the directory that you want to (recursively) clean. What I did was put it in the homedir on DreamHost, and then go into a directory that needed cleaning and issue the command:</p><p>/usr/local/php5/bin/php ../clear_odmarco.php</p><p>As you notice, at DreamHost you need to explicitly specify the php5 path because for some reason, the &quot;php&quot; command still defaults to php4. Then I put in a relative path to the clear_odmarco.php file (you could put the full path if you want).&nbsp;</p>
