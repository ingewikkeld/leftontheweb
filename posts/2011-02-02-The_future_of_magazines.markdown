---
layout: post
title: "The future of magazines?"
date: 2011-02-02 15:48
comments: true
categories: 
  - php 
  - magazines 
  - ipad 
---
<p>Fastforward to today. I still can not read long texts from screen without getting tired eyes. OK, let me rephrase that: I still can not read long texts from my laptop screen without getting tired eyes. At some point at the end of last year, during the holidays, our family got a gift from this friendly guy with the long white beard. No, not Santa Claus, but the Dutch version, Sinterklaas. The gift? You can guess it: an iPad.</p>

<p>So with the iPad there and having seen tweets of people reading the magazine on the device, I decided to try that for a while. Now, having read some of the backlog of the magazine and also some of the newer editions, I can only say: This is the future of magazines. This is the way we'll read them in the future. iPads (and similar devices) make for much easier reading.</p>

<p>My setup right now is simple: I download the PDFs of the magazine on my laptop. I put them in a specific iPad folder within my <a href="http://www.dropbox.com/">Dropbox</a>. I have created a seperate Dropbox account for the iPad, with which I have shared this iPad folder. I put all my PDFs in this folder, which then allows me to open them on my iPad.</p>

<p>So. Had the magazine's management lost all touch with reality? Or were they really looking into the future, and were they simply thinking of ingenius plans to rule the world. I can do nothing but conclude that it is the latter. They saw an opportunity and jumped on it, as one of the first I've ever seen make such a move. I am actually quite happy they did so, even while they did alienate a big part of their readerbase. I am quite sure they'll get back most of those, one at a time, as soon as they buy their own iPad or similar device. They've just been an early adopter, and you need those in this world.</p>
