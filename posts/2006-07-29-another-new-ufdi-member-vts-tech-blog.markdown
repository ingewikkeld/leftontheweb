---
layout: post
title: "Another new Ufdi member: VT's Tech Blog!"
date: 2006-07-29 13:29
comments: true
categories: 
  - technology 
---
For a while already I've been following <a href="http://blogs.vinuthomas.com/">VT's Tech Blog</a>. Vinu Thomas writes in an excellent way about technology there, talking about all kinds of topics, ranging from PHP, security to setting up a linux wireless access point.

Welcome Vinu to the network!
