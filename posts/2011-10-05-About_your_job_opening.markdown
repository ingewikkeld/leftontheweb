---
layout: post
title: "About your job opening... "
date: 2011-10-05 7:07
comments: true
categories: 
  - php 
  - jobopenings 
  - symfony 
  - wordpress 
  - zendframework 
  - drupal 
  - joomla 
  - HR 
---
<p>PHP is an awesome language. You can do many things with it. And people do many things with it. In the open source world alone, lots of different products have been built on top of PHP. Drupal, Typo3, Joomla!, Zend Framework, symfony 1 and Symfony2. Some are better than others, and sometimes that statement depends on who's telling you.</p>

<p>A lot of the above-mentioned products (and a lot of products I didn't mention) need some form of specialization to be good in it. I am personally mostly specialized in frameworks and custom applications built on top of frameworks, but even then am I mostly proficient with Zend Framework and both symfony versions. However, if you give me a Drupal or Typo3 project it probably takes me ages to finish my tasks, because I have to figure out how those products work. I can do it, it just takes longer.</p>

<p>With that knowledge in mind, it keeps surprising me that companies still write texts for their job openings that list all of the popular PHP software as "nice to have skills". It may seem like you attract a lot of response to your job opening that way (because most developers will have at least one of their favorite products in such a list), but in the end, you are wasting your own time, and the developer's time. Why? Because if I come to interview for your job, and it turns out your listing symfony in the text was only to attract people but you actually work with Joomla!, I will go home after the interview with a disappointed meeting, and will reject any offer you might make (if you even make one). And why would you make one? You were looking for a Joomla! developer and now you just interviewed this symfony addict.</p>

<h2>Do it right</h2>
<p>The next time you're writing the text for your open position, start thinking about what software you work with a lot. You can of course still list multiple PHP projects, but make sure your company actually uses those projects. So if your company has standardized on Zend Framework, list only that. If your company has standardized on using symfony in combination with Zend Framework, list those two. Are you mostly building on top of Wordpress, make sure to list that one, and only that one.</p>

<p>In the past days, I've seen many bad ones, but I've also seen a couple of "Wordpress Developer" and "Drupal Developer" positions come by. These companies are doing it right. Even if the position also includes using Zend Framework components in their projects, the description "Wordpress Developer" clearly indicates what the majority of the work is, and I can decide in a split second whether or not to read the rest. I hope more companies will start doing this, as it will make the life of not just "us developers" easier, but also of their HR departments.</p>
