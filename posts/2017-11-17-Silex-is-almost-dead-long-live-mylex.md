---
layout: post
title: "Silex is (almost) dead, long live my-lex"
date: 2017-11-17 10:30:00
comments: true
categories: 
  - php 
  - silex
  - symfony
  - components
  - composer
social:
    highlight_image: /_posts/images/silex.jpg
    highlight_image_credits:
        name: Wikimedia Commons
        url: https://commons.wikimedia.org/wiki/File:Biface_Silex_Venerque_MHNT_PRE_.2009.0.194.1_Fond.jpg
    summary: Silex has been announced as reaching EOL in 2018. What is Silex, and what does this announcement mean?
    
---

SymfonyCon is happening in Cluj and on Thursday the keynote by Fabien Potencier announced some important changes. One of the most important announcements was the EOL of Silex in 2018.

> EOL next year for Silex! #SymfonyCon
> ( -[@gbtekkie](https://twitter.com/gbtekkie/status/931071081556955136))

## Silex

Silex has been and is still an important player in the PHP ecosystem. It has played an extremely important role in the Symfony ecosystem as it showed many Symfony developers that there was more than just the full Symfony stack. It was also one of the first microframeworks that showed the PHP community the power of working with individual components, and how you can glue those together to make an extremely powerful foundation to build upon which includes most of the best practices.

## Why EOL?

Now I wasn't at the keynote so I can only guess to the reasons, but it does make sense to me. When Silex was released the whole concept of taking individual components to build a microframework was pretty new to PHP developers. The PHP component ecosystem was a lot more limited as well. A huge group of PHP developers was used to working with full stack frameworks, so building your own framework (even with components) was by many deemed to be reinventing the wheel.

Fastforward to 2017 and a lot of PHP developers are by now used to individual components. Silex has little to prove on that topic. And with [Composer](https://getcomposer.org/) being a stable, proven tool, the [PHP component ecosystem](http://packagist.org/) growing every day and now the introduction of [Symfony Flex](https://symfony.com/doc/current/setup/flex.html) to easily setup and manage projects maintaining a seperate microframework based on Symfony components is just an overhead. Using either Composer or Symfony Flex, you can set up a project similar to an empty Silex project in a matter of minutes.

## Constructicons

I have been a happy user of Composer with individual components for a while now. One of my first projects with individual components even turned into a [conference talk](https://speakerdeck.com/skoop/build-your-framework-like-constructicons-phpkonf-2017). I'll update the talk soon, as I have since found a slightly better structure, and if I can make the time for it, I'll also write something about this new and improved structure. I've used it for a couple of projects now and I'm quite happy with this structure. I also still have to play with Symfony Flex. It looks really promising and I can't wait to give it a try.

So the "my-lex" in the title, what is that about? It is about the choice you now have. You can basically build your own Silex using either Composer and components or Symfony Flex. I would've laughed hard a couple of years ago if you'd said to me that I would say this but: Build your own framework!

## Is Silex being EOL'ed a bad thing?

No. While it is sad to see such an important project go I think by now the Symfony and PHP ecosystems have already gone past the point of needing Silex. Does this mean we don't need microframeworks anymore? I won't say that, but with [Slim](https://www.slimframework.com/) still going strong the loss of Silex isn't all that bad. And with Composer, Flex and the huge amount of PHP components, you can always build a microframework that suits your specific needs. 

The only situation where Silex stopping is an issue is for open source projects such as [Bolt](https://bolt.cm) (who [already anticipated this](https://twitter.com/BoltCM/status/931094453707792386) that are based on Silex, as well as of course your personal or business projects based on Silex. While this software will keep on working, you won't get new updates of the core of those projects, so eventually you'll have to put in effort to rewrite it to something else. 
