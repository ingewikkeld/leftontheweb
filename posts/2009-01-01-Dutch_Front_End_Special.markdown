---
layout: post
title: "Dutch Front End Special"
date: 2009-01-01 22:35
comments: true
categories: 
  - frontend 
  - php 
  - html 
  - css 
  - event 
  - phpgg 
---
<p>I am quite excited about us organizing this event. This is our first step towards a full-blown conference, and I am proud of the speaker schedule we&#39;re putting together. We have speakers from Microsoft and Adobe who will go into their respective frontend and RIA technologies, Boy Baukema - the javascript expert at Ibuildings - will be talking about Javascript, and Robert Jan Verkade <strike>and Stephen Hay</strike> (Stephen Hay has unfortunately cancelled, so Robert Jan will be on his own) - who I saw with a very interesting talk at last year&#39;s <a href="/event/show/id/1" target="_blank">pfCongrez</a>  - will give a take on HTML and CSS technologies. More on the content of the different presentations will follow as well as the actual schedule.</p><p>I think we also have an excellent deal for people interested in visiting the event: entry is free for paying members of the phpGG, and non-members are able to come for the small fee of 15 euro. For that price, they&#39;ll get a free year of membership of the phpGG thrown into the package. This deal is only available in the pre-sale. You&#39;re still able to come without a pre-sale ticket, and you will still get the same package with free membership, however the price will then be 25 euro.</p><p>I am very much looking forward to this event and know for sure that it will be an amazing event. If you&#39;re living in the Netherlands and are interested in frontend technologies, this event will be a must-attend! </p>
