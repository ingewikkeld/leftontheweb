---
layout: post
title: "SimpleXML, simple, but limited"
date: 2005-10-26 15:02
comments: true
categories: 
  - technology 
---
After playing with it for a while, I actually had some work this week which would be a good project for using PHP5's simpleXML functionality. I had to work with an existing standard which I had no control over, and which proved to become a bane of my existence though.

The main problem of this standard was that the root element of the XML strings contained the type of the XML call that was being done. When you load an XML string into SimpleXML, though, you can not easily access the element name of the root element. Any attributes are easily accessed, but the root element itself is harder.

So I decided to write a little SimpleXML extension. Or really, a simple class that would instantiate and return a SimpleXML document, but with a new dummy root element.

<pre><?php abstract class mySimpleXML &#123;
    public static function loadString($strXml) &#123;
        $strXml = str_replace('<?xml version="1.0" encoding="UTF-8" ?>', '', $strXml);
        $strXml = '<?xml version="1.0" encoding="UTF-8" ?><myDummyRoot>' . $strXml . '</myDummyRoot>';
        return simplexml_load_string($strXml);
    &#125;
&#125;
?>
</pre>

This is still version 0.1, there are definitely things that can be improved (such as introducing regular expressions in the xml tag to allow for other versions of xml and other encoding types) but for now this does the job.
