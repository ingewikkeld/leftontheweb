---
layout: post
title: "On Conferences, Women and Sponsors"
date: 2012-03-02 17:13
comments: true
categories: 
  - php 
  - tech 
  - women 
  - conferences 
  - inclusivity 
---
<p>Let me start off with quoting a tweet I just posted:</p>

<blockquote>I find it interesting that people that claim to care about "inclusivity" say one should exclude people to attain that goal</blockquote>

<p>Let me get one thing straight: I don't care *at all* about porn. I am not a consumer of adult entertainment. I also care *a lot* about getting more women into technology as I feel women are highly underrepresented in "our" field of work. So yes, I am all for promoting technology to women. However, I have a really hard time with the bitching (pardon my french) about what is wrong with tech conferences and such.</p>

<p>What you're saying is that to include one group of people, conference organizers need to exclude another group of people. Isn't that discrimination against the other group of people. The fact that they work in a business that you feel is wrong, does not mean they are evil people. I can highly understand the choice of Eric Pickup as a keynote speaker. Not because he works in the adult entertainment business, but because from a technological point of view his challenges are really interesting, and the solutions they implemented may benefit any attendees, male or female. It may well improve their skills for their next project. Now, I did not attend his keynote, so this is pure assumption, but I assume Eric did not actually include graphical content in his slides or talk. I assume that he kept to the technical challenges and solutions. If he did this, then there is nothing offensive to anyone. It is an interesting technical presentation.</p>

<p>Similarly, for having those companies as sponsors: If I look at the ConFoo website, I see nothing offensive. The sponsors did not add offensive banners/logo's to the site, and if I click on the links I don't see any immediately offensive material on either website. The only reason you might be offended is the fact that you *know* they are in the adult entertainment business. So I wonder: If you know they are in that business, then why not just ignore them. Why not just stay away from their booth (if they have one), not click on the links on the ConFoo website? </p>

<p>I've attended ConFoo once, and the ConFoo crew did not come across to me as stupid people that would not care about anything. They came across as really smart people that do not want to offend people. I am pretty convinced that if any sponsor would try to add anything offensive into the goodie bag, they would object to that and block it. </p>

<p>Again: I *want* women to do well in technology and do anything I can to support this, including supporting aspiring female speakers in getting spots at conferences. However, I do this mostly because I am confident that they have something interesting to say and *not* because they are female. I understand you would not feel welcome when you would get adult imagery thrown into your face as soon as you hit the conference website or open your goodie bag. I would object to that as well. But I see nothing wrong with including those companies in the sponsorlist or accepting a speaker from that business, as long as they stays within certain limits of decency in their outings at the conference. </p>
