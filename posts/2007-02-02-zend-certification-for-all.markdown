---
layout: post
title: "Zend Certification for all!"
date: 2007-02-02 8:34
comments: true
categories: 
  - technology 
---
One of the things I've been trying to accomplish while working for <a href="http://www.dop.nu/">Dutch Open Projects</a> is to get all our developers Zend Certified. Today, we put the first real step on the road to this goal for the first three developers by getting the <a href="http://www.zend.com/store/zend_php_certification/lp">nicely priced bundle</a> for three of our developers: Two juniors and one senior will be our test bunnies ;)

I feel that the certification is a win-win situation: As a company you gain by being able to show your clients that your developers have the skills they need to perform their tasks. Also, the developers will undoubtedly learn from their exam preparations.

As a developer, you also gain. You gain knowledge and experience, you gain the 'Zend Certified' title, which is of course also fun, and you have something nice to put on your resume for the future.

I am happy that the movement has been started. The eventual goal is to have all developers certified. With a bit of luck, by the end of the year, we'll have all that done. For now, we want to await the experiences of our first three developers.
