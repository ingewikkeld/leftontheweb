---
layout: post
title: "Real-time release notes"
date: 2006-03-01 15:12
comments: true
categories: 
  - meta 
---
As you all may know, every once in a while I publish an article somewhere else than on this site ;)

Today my article on using the mobile phone as a tool for <a href="http://www.bookcrossing.com/">BookCrossing</a> was <a href="http://www.bookcrossing.com/articles/2193/Real-Time-Release-Notes">published on the BookCrossing website</a>.

For those interested in BookCrossing, feel free to check it out :)
