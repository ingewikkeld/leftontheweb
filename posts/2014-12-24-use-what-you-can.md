---
layout: post
title: "UWYC: Use What You Can"
date: 2015-01-02 9:22
comments: true
categories: 
  - php 
  - theartofasking 
  - uwyc 
  - diy
---

In [her book](http://www.amazon.com/The-Art-Asking-Learned-Worrying/dp/1455581089), Amanda Palmer talks about DIY, and how when you start asking for help, Do It Yourself is a strange term. Instead, she suggests UWYC, which stands for Use What You Can. I think software developers can learn a lot from this mindset. As Palmer says:

  > I have no interest in Doing It Myself.
  
This is exactly how we should approach software development. 

Many of the problems we try to solve on a daily basis, whether we work on projects for clients or we work on our own products, have been solved before in one way or another. A lot of the solutions to these problems have been posted as open source packages on, for instance, [Github](http://www.github.com/). Especially with [Composer](http://getcomposer.org/) and [Packagist](http://packagist.org/) it is now incredibly easy to find these solutions. So why would you not use these solutions? Even if they are not a 100% fit, there is a good chance that a big part of the solution fits your problem.

Palmer says:

  > There's really no honor in proving that you can carry the entire load on your own shoulders. And... it's lonely
  
This is an important lesson to us developers. Trying to solve every problem yourself is a humongous burden for you as a developer. No matter how small the problem is, do not underestimate the effect this may have on you and your codebase. And... it's lonely. If you have the [NIH-syndrome](http://en.wikipedia.org/wiki/Not_invented_here) and you're looking for new developers, there's a very good chance there's not a lot of developers interested in working with you. Developers usually want a challenge, but they want a good challenge. They want to solve the problems unique to your project, not the problems they've already solved many times before.

## Filter the solutions

Obviously, not all open source software and libraries are good. Everyone can publish code into the open source world, so you need to filter the solutions you found into usable and unusable solutions. Don't make this a final thing: The solution you find inadequate today may prove to be really useful in a year's time, when either the code author has been working long and hard on the code or your problem may be slightly different from the one you're solving today. Always filter for the problem at hand, with today's code.

Some things to watch for when filtering:

  * Use of best practices (does it support Composer, use external libraries, adhere to PSR-standards)
  * Has the code been documented well?
  * Is the code under active development?
  * When it supports Composer, how many installs does it have according to Packagist?
  
This list can be huge, and not all of the above need to be answered positively for a solution to be a good solution. Have a serious look at the code itself to see if it solves the problem. Eventually, the most important thing to filter for is ***does this code solve my problem?***

## This goes beyond libraries

The most obvious subject of the above is code: The libraries and components you use in your project(s). However, this goes beyond that. Any tool you use, ranging from your IDE to your version control system, and from your Continuous Integration system to ... yes, even the programming language you use. All of it can be evaluated in the same way as I've discussed above.
