---
layout: post
title: "Removing stylesheets in symfony 1"
date: 2011-04-27 8:34
comments: true
categories: 
  - symfony 
  - stylesheets 
  - php 
  - howto 
---
<p>So I started searching around, and quickly found the removeStylesheet() method in the sfWebResponse class. This didn't work however, when I called this in my controller. Googling around a bit more I found <a href="http://groups.google.com/group/symfony-users/browse_thread/thread/9160a7b9d063fadb">this mailinglist discussion</a> where Alexander Deruwe describes exactly what I need to do! Instead of removing it programmatically in the controller, I can just do it in my module view.yml file. So my view.yml of the module now looks like this:</p>

<p><pre>stylesheets:    [-main, formdefault.css]</pre></p>

<p>By negating a stylesheet that was added on a higher level of configuration, it is removed again from the set of stylesheets, and not added to the actual response. Thanks Alexander!</p>
