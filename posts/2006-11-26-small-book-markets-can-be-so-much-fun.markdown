---
layout: post
title: "Small book markets can be so much fun"
date: 2006-11-26 13:19
comments: true
categories: 
  - books 
---
Every few months, there is a small local book market here in Woudenberg. Last saturday, another market was held. As every other time, I was there to have a good look at the books available.

The great thing about this market is that it's not organized by booksellers. It's organized by a small musicians club (for lack of a better translation of the dutch word "fanfare"). They get books from members and other nice people to sell as a fundraiser. Book prices range from 0.10 euro up until a few euros, or sometimes 10 or 20 euros for special books. You never find "true" antiquities there, but for people who like both novels and literature, both in dutch and english, there's always something nice to find.

Aside from a few novels that I found and which I may or may not read, I was very, very happy to find a hardcover version of a dutch translation of <a href="http://en.wikipedia.org/wiki/Tolstoy">Tolstoy</a>'s <a href="http://en.wikipedia.org/wiki/War_and_Peace">War and Peace</a>. For a whopping 2 euros! Now that was a good find. I've never read it before, and it's a big big book, but I really want to read it sometime. This is my chance to do it :)
