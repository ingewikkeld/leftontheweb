---
layout: post
title: "Westerbork"
date: 2005-08-18 4:39
comments: true
categories: 
---
Until yesterday, I was slightly ashamed to not have been to Camp Westerbork before. A pivotal point in dutch history when it comes to <acronym title="The Second World War">WWII</acronym>, this was the camp where over 100,000 jews, sinti, roma and caught resistance were sent, waiting for deportation to the german concentration camps.

Until yesterday.What a disappointment it was. Truely. The only thing that was really impressive about the place, was what you know has happened there. 

There are now two parts in their feeble attempt to keep the memory alive. A museum, way too small, in which the only really impressive thing was a 15-minute documentary that I suspect isn't limited to this museum, but that they just play because it's related to the camp.

The second part is the old camp itself. Which was probably worst. Aside from two well-executed remembrance monuments (the app