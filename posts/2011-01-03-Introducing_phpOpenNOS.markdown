---
layout: post
title: "Introducing phpOpenNOS"
date: 2011-01-03 8:57
comments: true
categories: 
  - php 
  - php5.3 
  - library 
  - nos 
  - api 
  - webservices 
  - opensource 
---
<h2>So what is it?</h2>
<p>Basically, the phpOpenNOS library is nothing more than a layer between the API and your application. It is there so you don't have to develop the code to make the requests to the server. At this point, it implements <a href="http://open.nos.nl/documentatie/">all documented functionality</a> in methods of the OpenNOS class. What you get returned are model classes with getters for all information the server returns. </p>

<h2>So what doesn't it do?</h2>
<p>It doesn't do any more than giving access to the API. Anything you want to do with the data, you'll have to implement yourself.</p>

<h2>So how does it work</h2>
<p>As mentioned before, all functionality is implemented as methods of the OpenNOS class. So using this class, you have access to all the API methods. In the <a href="https://github.com/Ingewikkeld/phpOpenNOS">repository</a> there is an example.php file that gives you some examples, and more in-depth documentation will follow soon. The code is quite easy to follow though, so probably looking at the method signatures and the docblocks should give you enough information on the available options. A simple example for fetching articles:<br />
<pre>&lt;?php
require_once 'src/phpOpenNOS/OpenNOS.php';

$nos = new phpOpenNOS\OpenNOS('YOURAPIKEYHERE');
$articles = $nos->getLatestArticles();
</pre><br />
It truly is this simple. The $articles above will now contain an array of Article model classes with get* methods for all properties.</p>

<h2>So where to get it?</h2>
<p>I already mentioned the repository above, but the code is available from Github. It is the first public repository on the new Ingewikkeld account, which is the account of the company I run with my wife. Because I'm using Github, I would recommend everyone to fork the repository and if you have any improvements or bugfixes, send a pull request so I can get in your fixes and improvements into the main repository. <a href="https://github.com/Ingewikkeld/phpOpenNOS">Get phpOpenNOS here at Github</a>.</p>

<h2>The Future</h2>
<p>As mentioned, documentation is something I need to add. It will come, and hopefully it will come soon. In the README, you will also note that error handling is still in the TODO list: At this point there is no good error handling in place so I will have to add this at some point in the (near) future. Of course, I will keep following the development of the API and adapt the library to any changes in the API as well. And if you have any feedback on phpOpenNOS, I would gladly hear from you!</p>
