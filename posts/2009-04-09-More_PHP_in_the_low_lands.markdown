---
layout: post
title: "More PHP in the low lands"
date: 2009-04-09 23:20
comments: true
categories: 
  - dpc09 
  - ibuildings 
  - php 
---
<p>First of all, next week is the <a href="http://www.pfcongrez.nl/" target="_blank">pfCongrez</a> , where amongst others I am looking forward to seeing Anne van Kesteren and Derick Rethans speak about their respective topics.</p><p>But most importantly, this june Amsterdam will again for a short while be the center of the PHP universe with the Dutch PHP Conference. This year will be special for me, as it will be the first year I&#39;m not actually speaking at the conference. But since my employers <a href="http://www.ibuildings.com/" target="_blank">Ibuildings</a>  are organizing, I might well be active in one way or another at the conference to keep myself occupied.</p><p>Between those tasks, however, I am looking forward to attending the sessions. Choosing will be very very difficult this year, with most slots filled with two or even more sessions I&#39;d like to attend. I am specifically looking forward to attending Michelangelo van Dam&#39;s <a href="http://www.phpconference.nl/schedule/talks#DPC0913" target="_blank">talk on SPL</a>, Matthew Weier O&#39;Phinney&#39;s talk on <a href="http://www.phpconference.nl/schedule/talks#DPC0921" target="_blank">contributing and community</a>, Eli White&#39;s <a href="http://www.phpconference.nl/schedule/talks#DPC0919" target="_blank">talk on Scalability</a>, and Ben Ramsey&#39;s <a href="http://www.phpconference.nl/schedule/talks#DPC0903" target="_blank">talk on HTTP</a>. At this point, I am undecided on which tutorial I would like to attend. </p><p>Of all conferences worldwide, the DPC is the conference I&#39;ve been to most so far, and it&#39;s also the one I&#39;ve enjoyed most so far. The people there are always nice and laidback, the schedule is well-balanced and the location is very nice.&nbsp;</p><p>For those interested, I just want to mention that the early bird registration is still open the whole month of april, so if you want to go, now is the time to register to save some money for beer. I surely hope to see you there! </p>
