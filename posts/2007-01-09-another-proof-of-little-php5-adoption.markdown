---
layout: post
title: "Another proof of little PHP5 adoption"
date: 2007-01-09 13:01
comments: true
categories: 
  - technology 
---
Yet another proof today of the poor adoption of PHP5 in the developer community: <a href="http://www.php-mag.net/magphpde/magphpde_news/psecom,id,26752,nodeid,5.html">Results from a PHPMagazine poll</a> give a whopping 78.5% votes for CakePHP as best PHP Framework, with the runner up being Symfony with 10.9% of the votes. A scary result, when you think that CakePHP is still a PHP4 framework, and Symfony being the PHP5 counterpart.
