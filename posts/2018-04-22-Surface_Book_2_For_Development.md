---
layout: post
title: "Surface Book 2 For Development"
date: 2018-04-22 15:50:00
comments: true
categories: 
  - php
  - development
  - windows
  - surface book
social:
    highlight_image: /_posts/images/surfacebook/header.png
    summary: For quite some time I've been considering moving away from the MacBook Pro. For quite some time I had no idea what my next machine was going to be, until I first saw the Surface Book 2. This is my review based on nearly 2 months of full-time use of the Surface Book 2 for PHP development (and other things).
    
---

Over the past month and a half I've been trying to fully switch to a new work machine. Instead of my trusty MacBook Pro, I've mostly been working with a Microsoft Surface Book 2. Here's my lessons of this period.

## Context

Let's start with a bit of context. Over the past 10+ years I've been using Apple laptops exclusively for work. It started when I started my job at [Ibuildings](https://ibuildings.nl) and I got the opportunity to choose between a PC laptop and a Mac. I'd heard good things about Mac so I decided to give it a try. When I came home from work after that first day I told my wife "If I ever leave Ibuildings, I'm going to have to buy myself a Mac". I was impressed. The ease of use, the intuitiveness and the user experience were all so nice. So much better than Linux, which I'd been using in the years before. Or even Windows, which I'd been using until Windows ME came out and forced me into the stable hands of Linux.

I've been a full-on Apple fanboi ever since. Until a couple of years ago, there was nothing Apple did that would stop me from using their stuff. The platforms they built were stable and because they control both hardware and software, everything was tuned to each other. 

But in the past couple of years I've been a bit more unhappy with Apple's decisions. Their platforms are becoming less stable, less reliable, and more than once I've felt their decisions were based mostly on economics, on money, and not on usability which had been their focus until then (or at least that's how it felt). 

When Microsoft first announced the Surface (and later the Surface Book), I was intrigued. A tablet that is also a laptop. Everything in a single device. Powerful enough to work on, yet also easy to bring to a meeting and not have a laptop screen in front of your face. When the first rumours started that Apple was going to introduce an iPad Pro, I sincerely hoped it would be similar: An iPad device running macOS. That would be great!

The announcement of the iPad Pro was a disappointment to me. With it running iOS there was no chance I could do serious development on it. The specs were also disappointing. This was not an option anymore.

My MacBook has been slowly becoming a device that was frustrating me instead of a device that I felt happy to spend 8-12 hours a day on. So I've been looking around. In February I was visiting a Dutch Mediamarkt store to get some stuff, I noticed a Surface Book, and I started playing with it. A Mediamarkt employee came by to give me a demo of some of its features and I was pretty much convinced.

## Borrowing a Surface Book

Switching platforms is a big decision however. I've got so much time, effort and money invested in the Apple platform that I'd (at least partially) lose when I switch to Windows, that I really wanted to test-drive the Surface Book before actually making a decision. So I started looking for companies that have Surface Book rental devices. I found several companies but they were all aimed at renting the device for a couple of days, for events and such. If I were to rent it for 2-3 months (which I'd need to _really_ test-drive the device) I could just buy the device. The prices were much higher than anticipated, and long-term rental did not really seem to be a thing. So I reached out to Gerard, one of my contacts inside Microsoft Netherlands, and asked him if he knew of companies that do this. Gerard introduced me to Paul from Microsoft Netherlands, and Paul offered to lend me a device. For free. The only catch was that I'd share my experiences. Given that I was planning on sharing my experiences anyway if I'd find a rental device, I quickly agreed. This was a great opportunity!

## Waiting is hard

After agreeing to lend the device, the waiting is probably the hardest. It felt just like that moment when you ordered your new MacBook Pro on the Apple website and then have to wait for it to be delivered. A shiny new device is coming your way. Luckily, the wait wasn't all that long, because a week later a parcel was delivered. Ooooohhh.

## The first time

I unpacked the Surface Book 2 and booted it up. It definitely felt like I was unpacking and booting up a new MacBook in terms of experience. A nice wizard helped me set up the basics of the computer like the user, the wifi etc. The whole setup could also be done using speech with the Cortana software, but as fancy as it may seem, I somehow dislike microphones constantly listening to what I'm doing and saying, I quickly turned that off. All in all the initial setup was done in a couple of steps and a couple of minutes.

Now, to set this up as a development workstation I need some software. The initial list of things I thought I needed was:

- Firefox
- Docker
- Git
- PHPStorm

That should at least give me a basic setup for doing my development projects. Just like any new computer this is pretty straightforward. Download, run the installer, run the software. Nothing special about that. But I quickly found out I was missing some other things:

- An SSH key for Github
- 1Password for my passwords
- A MySQL client

The first two were done, pretty quickly, the last took me a bit more time.

## Sequel Pro

Replacing Sequel Pro took some time. Nothing works like Sequel Pro in terms of user experience. My first thought was to try MySQL Workbench, but I quickly concluded that it is not my thing. It just misses any form of user experience. After searching around the Internet for a bit I found [HeidiSQL](https://www.heidisql.com/), a free software package for managing MySQL, PostgreSQL and MSSQL databases. It's not as good as Sequel Pro, but it comes really close. The interface is very clear and intuitive. I'd found my Sequel Pro replacement. 

## Connecting my headphones

The first real issue I ran into was when I wanted to connect my bluetooth headphones (JBL E65BTNC) to the Surface Book. At first, the Surface Book didn't even see my headphones, then when it recognized my headphones it wouldn’t connect to them. When I turned bluetooth and my headphones off and on again, they connected. I guess one would call this "the Windows way" jokingly, but it seems that after all these years, it actually still _is_ the Windows way. As I used the Surface Book more and more bluetooth turned out to be the main weak spot: I tried to connect or reconnect several different bluetooth devices during my trial period. Eventually, most devices did connect, but it usually took several tries and turning off and on of devices and the Windows bluetooth functionality before it worked.

![](/_posts/images/surfacebook/desk.png)

## Docker

Another issue I had was with Docker networking. My initial playing around with Docker for Windows worked fine, but as soon as I wanted to start working on my client project I had major issues with networking in Docker. We have a pretty complex Docker setup which somehow did not want to work. Luckily, a co-worker who is also using Windows was able to help me out. In the Hyper-V Manager under Virtual Switch Manager, I needed to create a new external network. I used my wireless controller as the external network for this. Important was to tick the box 'Allow management operating system to share this network adapter'. Once I had done this and restarted Docker, it worked like a charm.

## ConEmu

Another useful tip I got was to use [ConEmu](https://www.fosshub.com/ConEmu.html). ConEmu is an easy little application allowing you to have multiple Powershell tabs in a single window. I use shells a lot, and with Powershell it is impossible to have several tabs with different shells, but using ConEmu, you can do this. ConEmu is actually quite powerful, because you can configure several different shell configurations. This means you can easily open a new tab with a different configuration, if you have shells for several purposes. Quite useful!

## Bash on Windows

At some point I was also pointed towards the option to [Run an actual Bash on Windows](https://www.howtogeek.com/249966/how-to-install-and-use-the-linux-bash-shell-on-windows-10/). I tried it out and indeed it works quite nice. Since there is no (or I did not find any) less-like program in Powershell, the Bash shell makes it a lot easier to quickly check the contents of files or for instance tail -f a log file. I had quite a few issues though integrating the Bash with my Docker, because Bash actually runs inside a Linux subsystem on your Windows, so it is not fully integrated with Windows. I was pointed to a solution: You need to set EXPORT DOCKER_HOST=0.0.0.0 in your .bashrc in the WSL. Then in the settings for Docker for Windows, you need to tick the checkbox Expose daemon on tcp://localhost:2375 without TLS. Now you can use the docker commands in your WSL linux (after you installed the docker.io package and the docker-compose package using apt-get). Unfortunately, this did not fully solve the problem, so I've decided (given I only have a limited trial period at the moment) to let this rest and just use Docker from Powershell and use Bash for things like tailing, quick file access etc.

## iMessage

Over the previous years I have invested quite heavily in Mac. Not just in terms of money, but also in terms of tooling. One of the major issues I've encountered is the fact that I am a heavy user of the Messages app on my MacBook. It allows to me to quickly type iMessages to other people with Apple devices. So a big question for my decision to switch to Windows is: Do I want to get rid of iMessage? Messages.app is only available for Apple platforms, so there is no way I can have the same integration with iMessage if I switch to Windows.

In the previous months I've solved this by using WhatsApp Web in [Rambox](http://rambox.pro/). I was already using Rambox for access to Slack, Gmail/Inbox, Discord and Google Calendar, so it was easy to also add WhatsApp to that. This allowed me the same ease of sending messages, and actually it made it even easier, because I was now also able to communicate that same way with people that do not have Apple devices. Only downside is: It's WhatsApp.

## Games

I don't use my MacBook purely for work. At home I also use Steam to install and manage the games that I play on my MacBook. The most important games for me at the moment are [Orwell](http://store.steampowered.com/app/491950/Orwell_Keeping_an_Eye_On_You/), [Eternal](http://store.steampowered.com/app/531640/Eternal_Card_Game/), [Prison Architect](http://store.steampowered.com/app/233450/Prison_Architect/) and [Football Manager 2018](http://store.steampowered.com/app/624090/Football_Manager_2018/). All of these ran extremely well, with much better graphics, on the Surface Book Pro. The touch screen was an added value for some games (like Eternal), because you could literally play by dragging cards onto the playing field. No mouse required anymore! Yay!

And there was more. I could now try out [Fortnite](https://www.epicgames.com/fortnite/en-US/buy-now/battle-royale), which doesn't really run on OSX. A great game, which I could easily play with all sliders cranked up to the maximum.

## Switching was surprisingly easy

Now, this is not really a testament to Windows, Mac or anything like that, but more to the fact that over the previous years I'd switched to so many cloud-based services, but switching to Windows was extremely easy thanks to things like Dropbox, Todoist, Google Calendar, Google Drive, 1Password and OneNote. Thanks to applications like these switching to another platform is literally just installing the apps on the new platform, logging in and it works. I immediately had access to the majority of the important files that I had on my Mac, all my notes, my calendar and my todo-list (most of my life is dictated by my todo-list these days, I could not afford to lose this data).  

![](/_posts/images/surfacebook/tablet.png)

## The touch screen and the detachable screen

The touch screen on the Surface Book is quite nice. It works really well. Unfortunately many apps are not yet adapted to work with touch screen, which made it a bit more annoying. Luckily, I also got a Surface Pen with the Surface Book, which allows for more precise aiming.

Where the power of the touch screen really stood out was when I would detach the screen to use it as a tablet, for instance when I went into meetings. When using OneNote, I could use the Pen to write down notes, then select those notes and use the OneNote _Ink to text_ feature to convert my written notes into actual text. The handwriting recognition is impressive! It makes some mistakes here and there but they're small and easy to correct. 

As a developer, detaching the screen had some downsides as well though. When the screen is detached, the battery life is (understandably) a lot lower. If you're running several Docker containers, it's hard to even sit through an hour-long meeting without your battery running out. So using the screen as a tablet does add some effort: You'll have to shut down your Docker containers and IDE before going into the meeting. When you turn off such battery-slurping applications though, your battery problems disappear immediately and you can sit through meeting after meeting without ever fearing of running out of battery life.

## Where Apple still wins

There are still some things Apple definitely wins in. Mostly this is the default toolset that is installed. When you get a Mac, you can easily open any file you receive. Whether those are images, PDF files, Word documents, spreadsheats. It all just opens. One of the main reasons for this is that every Mac is equipped with a great standard toolset: Preview, Pages, Keynote, Numbers, it's all there and can open just about any file you get. With Microsoft, you get the Office tools installed but you don't get the license by default, which means you get a read-only view with a very annoying popup asking you to get a license. PDF files are opened in Edge (for some reason), which seems to work but Edge is not as lightweight as Preview. 

Another thing I've noticed is that it is a lot easier to find good apps with a good UX for MacOS. There's a lot of software you can find for Windows, but a lot of it simply doesn't seem to have been made with any sense of UX. Even things like the aforementioned Todoist is a lot less usable on Windows than it is on MacOS. [In a blog they promised improvements](https://blog.todoist.com/2017/05/31/todoist-windows-10/), but if they released those improvements already, I'm not sure how bad it was before. Don't get me wrong, Todoist works well on Windows, but the interface is far from as smooth as it is in MacOS. Todoist is just an example, but I have many similar experiences with other apps. 

Having said all that, all those experiences were with apps that already work well under Windows. They could just be better. It's definitely not a reason to stop my move to Windows.

## Concluding

After just under 2 months of using the Surface Book 2 I'm very sad to let it go. This machine is amazing. I'm very positively surprised by Windows these days. When I "left" Windows (during the Windows ME time) it was a horrible operating system for power users, and the times since then that I had to work with Windows were not very good experiences. But since then, a lot has changed. Windows is a serious option again for development work, and with the Surface Book 2 Microsoft has a fantastic and very powerful machine that does well both as development machine and for your occasional gaming pleasure. I know what my next development laptop will be, and it's not a MacBook Pro. It's going to be a Surface Book 2.