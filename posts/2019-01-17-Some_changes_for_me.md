---
layout: post
title: "Some changes for me"
date: 2019-01-17 16:00:00
comments: true
categories: 
  - kink
  - radio
  - personal
social:
    highlight_image: /_posts/images/kinkstudio.jpg
    summary: I'm going to be doing less conferencing and visit less usergroups because I'm going to be doing a weekly radioshow and a podcast for my all-time favorite radiostation.
    
---

For the past years a lot of my focus has been on the (PHP) community. I've spoken at numerous conferences and usergroups. And although I've been cutting down on the amount of conferences, I've done more usergroups in the past year than in the years before that.

In December 2018, I've made a decision to cut down on this a bit more. This has nothing to do with not wanting to speak anymore, but more with an opportunity that has arisen that I want to take. I want to put 110% of my effort into this, which means I have to cut down on other activities that I'm doing. Speaking at usergroups and conferences is one of those things.

PHP has been my biggest hobby for the past 20+ years. It is great that I have been able to make it my job as well. Since quite a few years, I've picked up on something I've been interested in for years. I've started doing live radio. My first radio show was on the now discontinued Internet radiostation [On Air Radio](http://onairradio.nl/), after which I've moved on to another Internet radiostation [IndieXL](https://indiexl.nl). Both times I did everything in my own little radio studio that I had built at home. It was a lot of fun. 

My interest in radio already began when I was a teen. A Dutch morning show was also broadcasting on TV, so I was "watching radio" every morning. In the 90's, the Dutch radiostation KinkFM introduced me to an incredible amount of alternative music. KinkFM was the best radiostation I could imagine in terms of music, but also in terms of DJ's. People with an incredible passion for and knowledge of music. When the station was stopped by its owner in 2011, I was incredibly sad.

2 years ago one of the original founders of KinkFM saved the brand name from the company that at that time owned the name. While he wasn't planning to restart the station, the response he got was overwhelming, so he started researching his options. I got in touch and over a year ago I started doing a Spotify playlist for them called [KLUB KINK](https://open.spotify.com/user/kink_fm/playlist/6o10Kx8AiriyMgfSh09jUR?si=7QaBFRLZSa-xd6MLrQ1RdQ).

Late last year, the announcement came: A new radiostation focussing on alternative music _will_ be launched. Since FM is something nearly of the past, the name will now be [KINK](https://kink.nl/).

I have been asked to evolve my Spotify playlist into a podcast, and next to that, present a radioshow. After giving it some thought and looking at my schedule, I have decided to take this opportunity. I love doing radio, and to be able to do it for my all-time favorite radiostation is amazing. Starting on Thursday February 7, I will be doing a radioshow every Thursday from 7PM to 9PM.

Will I be completely gone from conferences and usergroups? Of course not! But as I mentioed earlier, I really want this to succeed, I want to give it 110% of my effort, and that means making tough choices.