---
layout: post
title: "Building a popular web community"
date: 2005-07-29 4:16
comments: true
categories: 
  - community 
---
Being involved in <a href="http://www.phpbb.com/">some forum software</a> myself, I&#39;m always interested in reading what people have to say about related topics. <a href="http://venturus.com/index.php?/weblog/permalink/how_to_build_a_popular_and_profitable_forum_community_07191208/">Venturus published an article</a> on how to build a popular forum community, which contains some useful tips. I don&#39;t agree with everything said, but there are definately some valid points in this article. Basically, I agree with points 1, 2, 4 and 5, and just disagree with point 3. I don&#39;t like lying.  Anyway, worth checking out.
