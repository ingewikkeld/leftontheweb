---
layout: post
title: "Javascript graceful degradation in symfony 1.0"
date: 2008-05-18 21:53
comments: true
categories: 
  - php 
  - symfony 
  - javascript 
  - gracefuldegradation 
  - usability 
  - accessibility 
---
<p>Some people who may not be able to access (all of) Javascript:</p><ul><li>Blind people with non-js screenreaders</li><li>People who actively turn off javascript</li><li>People behind corporate firewalls</li><li>Search engines<br /></li></ul><p>I am sure there are more groups that can be thought of, but these four are probably the most common who have trouble with an interface using Javascript/AJAX. And you may not care about an individual group of the list above, but there&#39;s a big chance that one of these groups at least will fall inside the target audience of your web application. And it&#39;s not that hard to support those people as long as you keep it in mind from the start.</p><p>The easiest approach by far is to first write your application completely without Javascript. That means your application works in most common browsers, assuming of course you have correct (X)HTML and CSS, but that is outside the scope of this post. If you have such an application running, you can easily now start expanding your application by using the <a href="http://www.symfony-project.org/book/1_0/11-Ajax-Integration#Graceful%20Degradation" target="_blank">graceful degradation features of symfony</a>. These features are basically nothing more than two helper functions:&nbsp;</p><blockquote><p>if_javascript();<br />//Your javascript-based code goes here<br />end_if_javascript();</p></blockquote><p>&nbsp;Anything that goes between these two functions, will only be outputted if the browser supports Javascript. Easy, but what about our old code? We don&#39;t want to have that displayed if there is Javascript support available. Well, for that, there is the good old &lt;noscript&gt;-tag in HTML. So, here is what your code may look like before the extending of your functionality with Javascript:</p><blockquote><p>&lt;!-- Nice piece of regular HTML functionality --&gt;</p></blockquote><p>And now, here it is as extended with Javascript:</p><blockquote><p>&lt;?php<br />if_javascript();<br /> //Your javascript-based code goes here<br /> end_if_javascript();<br />?&gt;<br />&lt;noscript&gt;<br />&lt;!-- Nice piece of regular HTML functionality --&gt;<br />&lt;/noscript&gt;</p></blockquote><p>Symfony will ensure that the Javascript-based code will only be shown if Javascript is supported, and the browser will ensure that anything between &lt;noscript&gt;-tags will be ignored if Javascript is enabled. Powerful and easy. </p>
