---
layout: post
title: Dutch PHP Conference '24
date: 2024-03-25 13:00:00
comments: true
categories: 
  - php
  - conferences
  - dpc
  - dpc24
social:
    highlight_image: /_posts/images/dpc24.png
    highlight_image_credits:
        name: Marjolein van Elteren
    summary: It was time for another Dutch PHP Conference. This is a write-up of that awesome conference.
    
---

After several COVID-years of online conferencing and an earlier announcement that "Dutch PHP Conference" would remain online, some developments favorable to organizer Ibuildings meant doubling back on that decision. A lot of Dutch PHP developers, including myself, were really happy with that. And it was indeed glorious to meet again in a physical space with a big part of the Dutch PHP community.

The fact that Dutch PHP Conference was now organized together with AppDevCon and WebDevCon also resulted in some great cross-pollination. It's great talking to people from outside your "bubble". 

Being invited to speak was another bonus for me. I was invited to speak at EndPointCon last year but had to cancel that last-minute because of a nasty stomach flu or something like that. So being to come back and speak was fantastic.

## The talks

I watched quite a few talks from the DPC schedule, although I did make a little trip to one of the other tracks. 

### Using Open Source for Fun and Profit

We started with the opening keynote by Gary Hockin, a speaker that I know as very entertaining but also putting across a serious message. In this keynote, we walked through Gary's life as a developer, but also learned about the role of open source in all that. Very relatable at times, and in my talk on Open Source I could even reference Gary. Great talk!

### Crafting a greener future with PHP

Then it was onwards to Michelle Sanver. I try to make a change through [my involvement in XR](https://skoop.dev/blog/2024/03/18/regenerative_software_development/), but as a developer I also have a responsibility to make this world less worse, and as software developers we can also make a change. That is the important message I got from this talk. Michelle gave some great background on the how and why we should make a change. My biggest take-away was something I had never considered: We are used to running cronjobs at night. In some countries (such as Sweden) this is great from the perspective of green development, because there is less energy usage at night and the hydro-plants keep producing energy there. In The Netherlands, however, at night there is not a lot of renewable energy. Instead, at night the majority of electricity is generated using gas plants. So running heavy tasks at night is actually not good. Thinking about the location of your servers and how energy is generated there is therefore a good way of putting less stress on nature.

### We need to talk about sitting

The next talk I saw, or should I say participated in, was the talk by Laura Broekstra on sitting. 2 minutes into the talk we heard we sit too much (the Dutch are apparently world champion in the amount of time they spend sitting) so we got told to stand up. We should for the remainder of the talk.

Laura confronted us with some facts about sitting and how her team has made huge changes in the amount of time they spend doing things other than sitting, including all kinds of exercises. The talk ended in an actual exercise where everyone in the room had to join in on some squatting exercises for a couple of minutes. 

I really enjoyed this talk and need to really evaluate how I spend my working time. I also think more conferences should give this topic some attention.

### Offline first!

After lunch I went to the talk by Rowdy Rabouw on offline first applications. I am far from a front-end developer but I feel it is my responsibility as a backend developer to at least try and stay up-to-date with developments in that area, and the way Rowdy showed you can use service workers in the browser to do a lot of local caching was really interesting. And to be fair, I also went to this talk because I really enjoy the style of presenting that Rowdy has. 

### Evil Tech: How Devs Became Villains

After my talk I stuck around in the room to see Sergès Goma explain how devs can become villains. This was a very entertaining talk (including some developers actually coming to the front to do evil villain laughs) on a very important and underrated topic: ethics. This talk gave a lot of food for thought. Sergès delivered this really well. 

### Community, PHP and us: Growing up

I ended up with the second time Michelle Sanver (I support the idea of a MichelleCon), this time with Michelle doing the closing keynote with her story. Her story of becoming a software developer is very different from the story we heard from Gary in the morning. Well, there were some similarities, but Michelle also shared some very negative experiences she encountered along the way. We should learn from stories like these, and make the community an even better, more welcoming place. For everyone.

## Next year!

After the keynote it was announced that Dutch PHP Conference will be back again next year, and I'm already looking forward to it. Will I see you there?