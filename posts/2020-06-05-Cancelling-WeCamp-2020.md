---
layout: post
title: "Cancelling WeCamp 2020"
date: 2020-06-05 16:30:00
comments: true
categories: 
  - php
  - wecamp
  - wecamp20
  - comference
social:
    highlight_image: /_posts/images/wecamp-personal-development.jpg
    summary: We've had to cancel WeCamp this year due to COVID-19, but we're replacing it with something else
    
---

A couple of weeks ago we had an energy-draining meeting with the WeCamp 2020 team. While it was technically possible to organize WeCamp 2020, given the current crisis we had an interesting and lengthy discussion about whether it was the right thing to do. We ended the meeting with a very tough decision: To cancel WeCamp 2020. It is hard to justify the risk of attendees travelling to and from our island in a period where an infection can have such dire consequences.

All attendees should have had an email by now with more information. If you have not, please get in touch with me.

## We can't sit still and do nothing

One of the many amazing things of the people of [Ingewikkeld](https://ingewikkeld.dev/) is the fact that their passion for sharing knowledge and helping people level up is so strong that we can't just sit still and do nothing. We decided that we'd be doing an online event in the same week as WeCamp. Now, the WeCamp experience as it is can not be translated to an online event, so we decided we should do something different. We're still working on all the details, but expect an event where tech talks are scheduled together with talks on personal development, where relaxation techniques are discussed and you have the opportunity to do a talk yourself as well. We'll most certainly share more details as we confirm them, but for now, we have a name: [Comference](https://comference.online/), the online conference from the comfort of your own home. If you want to stay up-to-date on our announcements, you can subscribe to our mailinglist from our website, and you can follow us on [Twitter](https://twitter.com/comference20), [Facebook](https://facebook.com/comference.online) and [Instagram](https://instagram.com/comference20). 

Oh, and the best thing: Comference will be live streaming for free! So while we can't welcome you to our island in August, we'll be happy to welcome you to our online stream!