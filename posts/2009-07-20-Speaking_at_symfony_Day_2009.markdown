---
layout: post
title: "Speaking at symfony Day 2009"
date: 2009-07-20 21:17
comments: true
categories: 
  - php 
  - symfony 
  - conference 
---
This new single-day conference taking place in Cologne is an excellent way for both beginners and more advanced users of symfony to learn new things, expand their network in the symfony community, and simply have fun.<br /><br /><strong>Workshop</strong><br />For beginners, I will be hosting a full-day workshop. The workshop will be a combination of theoretical information on the structure of symfony and symfony projects and how certain things work and actual work. During the course of the day, all attendees will work on developing their own symfony application (so bring your laptop!). <br /><br /><strong>Expert track</strong><br />For those already proficient with symfony looking for more advanced topics, there is a presentation track. Topics such as case studies, doctrine 1.2 and symfony 1.3, testing with lime, best practices and Sympal CMF will be presented by Jonathan Wage, Bernhard Schussek, Nicolas Perriault, Xavier Lacot and Rob Bors.<br /><br /><strong>Party!</strong><br />After satisfying your thirst for information you will be able to satisfy your thirst for fun and drinks at the party. Organizer <a href="http://interlutions.de/" target="_blank">Interlutions</a>  is celebrating their 10th birthday and all participants of symfony Day Cologne are invited to join them in their celebrations. Come join speakers and attendees and have fun at the party!<br /><br /><strong>Wow, that must be expensive</strong><br />Not really! Participation is only 80 euro, a very affordable price for a conference like this! <a href="http://www.symfonyday.com/register.html" target="_blank">Registration is already open</a> , so register now! I hope to see you there!
