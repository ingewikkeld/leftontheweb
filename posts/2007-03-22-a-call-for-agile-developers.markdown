---
layout: post
title: "A call for Agile Developers"
date: 2007-03-22 12:46
comments: true
categories: 
  - technology 
---
Ever since first being introduced to the Agile methodologies while working at <a href="http://www.tomtom.com/">TomTom</a>, I've been intrigued. We worked in an agile way at TomTom, and now that I'm Lead Developer at <a href="http://www.dop.nu/">Dutch Open Projects</a>, I'm starting the introduction of Agile development there.

My experiences so far as a PHP developer is that a lot of companies and (open source) projects do not work according to Agile methodologies, or often not really according to any methodology (a.k.a. <a href="http://en.wikipedia.org/wiki/Cowboy_coding">Cowboy Coding</a>). While there may be nothing wrong with that, I still want to try to push the Agile methodologies more into the realm of PHP.

For that, I've registered a new domain name, AgilePHP.eu. As I am far from a true expert on Agile Development, I don't want to do this on my own. So for that, I'm looking for some (european?) PHP developers who want to start a nice project with me. These PHP developers should either have experience or a strong interest in the Agile methodologies. 

These developers will be working with me on thinking out the best form for the website, and also of course to get it up and running. It's all on a non-profit basis, the reward will be that you've worked on getting a wonderful site in this world and teaching the PHP community about Agile Development ;)

If you're interested, please send me an e-mail over at stefan@agilephp.eu, and we'll go from there.
