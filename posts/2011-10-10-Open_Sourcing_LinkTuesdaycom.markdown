---
layout: post
title: "Open Sourcing LinkTuesday.com"
date: 2011-10-10 20:30
comments: true
categories: 
  - php 
  - symfony 
  - Symfony2 
  - linktuesday 
---
<p>It did run into some issues, some of which I couldn't immediately fix because I was occupied by other things. Really annoying, and it made me feel guilty, not being able to fix things. I considered open sourcing the source code, but decided against it because I thought nobody was waiting for it. Until people started suggesting I open source the code so they could help fix it. So there <em>were</em> people waiting for it!</p>

<p>So last weekend at the PHPNW11 conference in Manchester, I published the code to <a href="https://github.com/LinkTuesday/Linktuesday.com">it's own Github repository</a>. I have to warn you though: this is code written on preview releases on Symfony2, and the project has not yet been converted to the latest release of Symfony2, so there may be strange things, inconsistent things or plain out wrong things. Something could be done better? Make sure to send a pull request and your code may make it into the next release of LinkTuesday.com :)</p>
