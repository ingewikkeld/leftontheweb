---
layout: post
title: "Three Months Of Patreon"
date: 2016-02-12 11:00:00
comments: true
categories: 
  - php 
  - patreon
  - content
  - opensource
  - money
  - appreciation
  - crowdfunding
---

Tomorrow it is three months ago that I [launched my Patreon page](https://skoop.dev/blog/2015/11/13/launching-my-patreon-page/). As I mentioned in my blogpost about launching my Patreon page, it was mostly an experiment to see how that would work out. I've seen a bunch of artists use Patreon very successfully, but the world of PHP, open source and software development, while it has some parallels, is different from the world of art, music and books. I was curious about how Patreon would be received in this world.

Since it was an experiment, any outcome would be a good outcome. I went without real expectations. Of course I was hoping it to be a major success, but with me being the only Patron of [Rafael](https://www.patreon.com/rdohms), I wasn't expecting much.

Now, three months later, I have two patrons. I am very grateful to Malte and Rafael for their support. I've promised to only charge them a maximum of once a month, and so far I've charged them twice. This does not mean I get a lot of money. With those two charges, I can now buy me a beer.

### We are not used to paying for free content

I found out about Patreon through [Amanda Palmer](https://www.patreon.com/amandapalmer), who successfully uses Patreon to fund music she gives away for free or sells for a very low price. Most of her music is available for "name your price" (basically, $0 or more) on her [bandcamp page](https://amandapalmer.bandcamp.com/). Her most recent release, the [David Bowie tribute EP Strung Out In Heaven](https://amandapalmer.bandcamp.com/album/strung-out-in-heaven-a-bowie-string-quartet-tribute) has a minimum of $1, but that's mostly to pay for the royalties she owes the Bowie estate. Yet for every Thing she releases, even though it is available for (nearly) free, she gets paid nearly $34000. I think that's awesome! And it makes sense, because this is her life. This is how she makes her money. From her music.

Through [Lorna](http://twitter.com/lornajane) I found out about [an article on A List Apart by Rachel Andrews](http://alistapart.com/article/the-high-price-of-free) on the high price of free. In that article, Rachel says some really important things. For instance:

  > As an industry we have become accustomed to getting hundreds of hours of work, and the benefit of years of hard-won knowledge _for free_.
  
This is what open source has done for us, and it is fantastic! If you run into a problem, you're a quick Google search away from finding the solution. If you come by a new piece of tech, most of the times there's already tons of documentation and blogposts on how to work with this tech. This is extremely valuable and one of the main reasons why open source is awesome: There is virtually no barrier of entry to get into programming, aside from the need for a computer and an Internet connection.

While this is awesome, it also means that we've grown accustomed to getting all this information for free. We gladly consume all this labour of love by passionate developers and content creators and expect things to be free. And of course we should, that is why the creators made it. What I do think, however, is that we should consider to show our appreciation a bit more. 

### We are used to paying for paid content

We consume free content without any problem, and we also consume paid content without any problem. Sure, there is a small problem of piracy for paid content, but mostly people will gladly pay for their [php[architect]](http://phparch.com) subscription, their copy of [Chris Hartjes' books](https://grumpy-learning.com/) or one of the [many awesome publication of Lorna Mitchell](http://www.lornajane.net/publications). We seem to be fine with paying for content that has a price on it. This is, obviously, a good thing. The people who create this content spend a lot of time on it and are, partially or fully, depending on the sales of their content for their income. I can recommend you a shitload of content that is very much worth the money (seriously, [ask me if you need anything](http://twitter.com/skoop)). 

### So about that free content

We're used to paying for content, that much is clear. So why not show your appreciation of the free content out there by paying for that as well? Patreon is only one way of doing so. Last December, I went into my Steam account and through my friends list, and randomly donated some games from the wantlist of those friends to them. Many content creators also have Amazon wishlists. Sometimes, people also have some paid product aside from their free work. For instance, [Jordi](https://seld.be/about) has [Toran Proxy](https://toranproxy.com/) and an [Amazon wishlist](https://www.amazon.co.uk/gp/registry/wishlist/2E8F7J1AHGANG/). And without wanting to do too much self-promotion, I have [a Patreon page and a Paypal page for donations](https://skoop.dev/about/). Derick Rethans also has an [Amazon wishlist](http://www.amazon.co.uk/gp/registry/registry.html?ie=UTF8&type=wishlist&id=SLCB276UZU8B). And these are just a few examples. If you consume a lot of free content (whether it is code, documentation/blogposts or anything else), please consider showing your appreciation.

### Enabling people to show their appreciation

One does not work without the other: If you are a content creator and you supply your work free of charge, please consider adding a way for people to show their appreciation. Whether this is an Amazon wishlist, a Paypal page or a Patreon page, your Steam wishlist or something else does not really matter. What is important is that you enable people to show their appreciation. If more content creators do this, perhaps more people will get used to somehow paying for free content. And those who are less fortunate and simply can't pay will still be able to access all the free content out there. 

Let's make one thing clear: I don't expect any content creator in the open source world to start earning $34000 for each piece of content they create, but it would be nice if people could just get that extra bit of money in that may allow them to sometimes just buy an extra ticket to a movie or concert, or that one bottle of whisky they really want. Let's try to change the mindset a bit and allow people to pay for free content.
