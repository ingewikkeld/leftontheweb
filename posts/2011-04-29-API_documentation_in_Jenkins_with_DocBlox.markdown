---
layout: post
title: "API documentation in Jenkins with DocBlox"
date: 2011-04-29 13:42
comments: true
categories: 
  - php 
  - docblox 
  - phpdoc 
  - phpdocumentor 
  - jenkins 
  - jenkins-php 
---
<p>First of all, of course, I needed to install Jenkins. This article is not about installing Jenkins, so I won't go into the details of that, but you can safely assume that I simply installed Jenkins on my Ubuntu VPS using apt-get. So this means that I now have a Jenkins user on my system, whose home directory is in /var/lib/jenkins (which is also the root of the Jenkins working directory). Next, I used the template of <a href="http://jenkins-php.org">Jenkins-php.org</a> to have a base template for my Jenkins projects. This saved me a lot of time in figuring out how to set up the build configuration for my project.</p>

<p>The only downside is that Jenkins-PHP assumes you want to use phpDocumentor for API documentation generation. I wanted to replace that with <a href="http://docblox-project.org">DocBlox</a>, the API documentation generator built by my good friend <a href="http://naenius.com">Mike van Riel</a>. One of the main advantages for me personally is the support for namespaces in DocBlox, that makes me want to use DocBlox over phpDocumentor. The projects that I am currently starting to use DocBlox for are not big projects, otherwise another reason would have been performance. DocBlox has been known to use far less memory and generate the documentation much faster than phpDocumentor. Anyway, I'm not here to sell DocBlox to you, I'm here to tell you what I did to set up DocBlox in the build process for Jenkins :) </p>

<p>So, to set up DocBlox is actually very easy. You just have to alter the build.xml template file that Jenkins-PHP has to replace phpDocumentor with DocBlox. Jenkins-PHP uses the following configuration for API documentation:</p>

<p><pre>
&lt;target name="phpdoc"
        description="Generate API documentation using PHPDocumentor"&gt;
 &ltexec executable="phpdoc"&gt;
  &lt;arg line="-d $&#123;source&#125; -t $&#123;basedir&#125;/build/api" /&gt;
 &lt;/exec&gt;
&lt;/target&gt;
</pre></p>

<p>Instead of that configuration, I now use:</p>

<p><pre>
&lt;target name="docblox"&gt;
  &lt;exec executable="docblox"&gt;
    &lt;arg line="project:run
               -d $&#123;source&#125;
               -t $&#123;basedir&#125;/build/api" /&gt;
  &lt;/exec&gt;
&lt;/target&gt;
</pre></p>

<p>You will have to make sure to remove the phpDocumentor antcall in the parallelTasks target, and call the docblox antcall instead:</p>

<p><pre>
&lt;antcall target="docblox"/&gt;
</pre></p>

<p>If you've done all this, then Jenkins should make sure that DocBlox documentation is generated. If you want to see the full build.xml that I'm currently using for my phpOpenNOS project, then check <a href="https://github.com/skoop/phpOpenNOS/blob/master/build.xml">here</a>.</p>
