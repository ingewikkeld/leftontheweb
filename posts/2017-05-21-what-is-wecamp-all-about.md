---
layout: post
title: "What is WeCamp all about?"
date: 2017-05-21 10:45:00
comments: true
categories: 
  - php 
  - wecamp
 
---

Recently I got an email from someone who was interested in coming to [WeCamp](https://weca.mp/), but needed some more information to help convince their manager. I wrote a big email trying to describe WeCamp and our ideas of what WeCamp is (or should be). 

Since this may actually be useful to more people that want to come to WeCamp but need to convince their manager, I'm posting that email here as well. 

> The idea behind WeCamp is that attendees will go through the whole process of a software development project. When the attendees come to the island they will be assigned in teams with 4 other people that they most probably not know (we try to split up people who work for the same company or live in the same region). Every team of 5 people will be assigned one of the coaches, and this coach will support the team in finding their own way in the project. These coaches are specifically NOT teachers, but instead people who will support the team, because we want the team to find their own way most of the time.

> Once a team is formed and assigned a coach, they find a working place on the island, and they'll have to think of a project to work on. The team will have to think of a project themselves and decide on what is realistic to build given the limited amount of time (usually about 3 days of development). Once they've decided on their MVP and created their initial planning, they start working on their project.

> Each team is free to choose a methodology for development. While we've so far seen no waterfall-style development, we've had several different methodologies from full scrum to kanban and variations on both because the team decided that was the best approach. One thing all teams have though: A central stand-up right before lunch where each team can share their progress, the problems they've run into and their lessons learned. In that way we try to get everyone to learn from everyone else. During the fifth day, each team will present their project as well as their lessons learned while doing the project.

> To balance out all the hard work, we have some social events during the week. We have a game night (with board- and card games, no electronic games!), we have a BBQ and we have a "pirate game", an activity where a pirate comes to the island with some friends to have everyone do some assignments. When there's no special events at night, there's always the option for a drink with fellow attendees around the camp fire. Everyone sleeps in tents with beds in 'em, on the island. The price for the ticket is all-in: It includes all drinks and meals so attendees don't have to worry about anything during the 5 days on the island.

> When we first started WeCamp 4 years ago, we meant to start a technical event. We had a lot of experience with conferences as an inspiring place for new tech, but felt we never had time to actually play with that new tech. That's what we initially aimed for with WeCamp: A place to actually play around with new tech. It quickly turned out though that all the tech is cool, but WeCamp was about more than just tech. It was about personal development. About learning a lot of soft skills next to all the tech: communication, teamwork, planning, making decisions, presenting.

> The coaches will have private conversations with all team members, and together they will create a personal development plan with attendees. Some time after WeCamp coaches follow up with their team members to see if the goals set in the personal development plan were reached, or at least are being pursued.

