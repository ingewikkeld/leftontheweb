---
layout: post
title: "Variation"
date: 2004-12-11 13:23
comments: true
categories: 
  - leftontheweb 
---
A saturday night out. It's been a while, and now that I'm gonna do it, it's gonna be good. And varied. I'm starting with a performance by <a href="http://www.adamandtheweight.com/">Adam Kowalczyk</a>, followed by a <a href="http://www.radiantslab.com/interzone/">nice party</a> with performances by Owen, Apparat and <a href="http://www.lackluster.org/">Lackluster</a>.

So, starting out with a pop/rock gig, and then moving to danceable electronic bleeps. Yes, this is going to be a good night.
