---
layout: post
title: "mod_rewrite saved my old RSS and Atom feeds"
date: 2005-09-19 4:20
comments: true
categories: 
---
After moving to Textpattern, I had one tiny little problem: Everyone that subscribed to my RSS and Atom feeds wouldn't know about the change. Luckily, <a href="http://www.zerokspot.com/">zeroK</a> came to the rescue. He suggested to use mod_rewrite and even helped me compose the write mod_rewrite code for it:

??RewriteRule ^rss.xml$ /rss [R,L]??
 ??RewriteRule ^atom.xml$ /atom [R,L]??

I tried accessing rss.xml and it indeed gave me my new RSS, so if all is well, all you old Left on the Web subscribers should now see this new post! Come by and see what I've done with the site.
