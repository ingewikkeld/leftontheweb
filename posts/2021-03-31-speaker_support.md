---
layout: post
title: "Speaker support"
date: 2021-03-31 10:00:00
comments: true
categories: 
  - php
  - conferences
  - speakers
social:
    highlight_image: /_posts/images/comference-blog.png
    summary: Over all these years it has been very normal for conference speakers to not get paid for their effort, at least in the PHP community. But is that fair? With Comference, we've decided that we'll reimburse speakers for their time.
    
---

Over all the years that I've been a speaker at PHP conferences, I have been very happy as a speaker. Most conferences I spoke at would reimburse my travel and get me a hotel room for the duration of the conference. Most of the time, a speaker dinner was included as well. It got me to travel the world. I've seen many amazing cities such as San Francisco, Montreal, Verona, Barcelona, Paris, Berlin, Cologne and many more. I would not have seen all those cities without conferences paying for my airfare and hotels. There were even conferences where I'd pay for some of the costs. Either because I just wanted to be there anyway, or because the conference would offer my company a sponsor slot if I covered my own airfare.

Over the past few years, I've been reading more about how accessible speaking is. Or rather, the lack of said accessibility. I had not yet considered this since I have been privileged to either have employers paying for part of the costs, or having a company with a high enough income to be able to cover some of the costs of speaking. There are actually developers that do not have the luxury of being able to cover those costs, or who are unable to just take the time off work. There's also other situations, such as having to pay for someone to babysit during a talk.

With [Comference](https://comference.online) and also before that with [WeCamp](https://weca.mp) we've always made an explicit effort to aim for a diverse line-up. And this year, we've decided to make an extra effort. This year's edition of Comference we will compensate speakers for their effort. Especially with online conferences that can be done directly from your home or office, we hope that this will allow new people to be a speaker. Eventually, we hope to expose as many different viewpoints on technology-related subjects as possible as we believe every viewpoint can be learned from.

We're not stopping at the speaker fee either. We facilitate different talk lengths as well. It is possible for speakers to submit talks for 15-, 30- and 45-minute talks. We hope that this enables people to submit a talk of the length they prefer for their subject. Because not every talk should be 45 minutes or an hour.

The [CfP for Comference is now open](https://cfp.comference.online/) and I'd like to extend a warm invitation to anyone who has something to share. Our speaker line-up will be created with a combination of invited speakers and selections from our CfP. 

