---
layout: post
title: "The Holy Root"
date: 2006-04-12 15:12
comments: true
categories:
   - technology 
---
There are many *nix purists that feel the root is holy. Only the basic, default directories should be located at the root. An ideal system probably looks something like this:

/
/boot
/dev
/etc
/home
/lib
/opt
/proc
/usr

I recently encountered such a purist. He felt that adding something to the root, be it a directory or a mount, was wrong. I don't really understand why.

Sure, mounts can be located in /mnt (or /media in some distributions). But some things are very important. Especially with web servers, a location in the root would be much better. For instance, if you have a webserver offering downloads, then you could place these binaries, which you want to keep outside of the document root of your website, in /binaries or something similar. We have a similar setup at work. So what is wrong with this? All important stuff goes in the root, so why not something as important as this. It's being used system wide.

On my home fileserver, I have a shitload of my cd's ripped to mp3 or ogg format. I have these files located at /mp3. I see no need to put this somewhere else. The mp3's have their own harddisk, and the music is shared so that I can access it from my laptop, and my wife can access it from her PC.

I understand that you don't want to clutter your root with too much directories and mounts, but adding very important information at the root should not be a deadly sin as some people seem to think.
