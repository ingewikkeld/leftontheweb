---
layout: post
title: "The PHP Developer During Advent"
date: 2015-12-01 17:30:00
comments: true
categories: 
  - php 
  - advent
  - 24days  
---

It is December 1st, which is the traditional start of the advent period, the period leading up to Christmas. It has also in the past years sparked initiatives in the PHP, web development and Open Source communities. Some of those initiatives I want to mention here for everyone's enjoyment and education.

## 24 Pull Requests
To give back to the open source projects we all use on a regular basis, the suggestion of [24 Pull Requests](http://24pullrequests.com/) is to send a pull request per day to any open source project. They give you suggestions on the projects and you can of course suggest other projects. It's an interesting challenge to send a PR every day up until Christmas, but if you're up for it, many projects will be grateful.

## 24 Days In December
During the days until Christmas, [this website](http://www.24daysindecember.net/) will post a daily thought on PHP and the community. Add it to your feed reader. Spoiler: I have contributed.

## 24 Ways
For those that do more than just PHP, [24 ways](https://24ways.org/) will give you an article per day on web design and development. Even if you only do PHP, it might be a good idea to broaden your horizon in December using this site.

## Advent of Code
If you're up for a series of coding puzzles, you might enjoy [Advent of Code](http://adventofcode.com/). Advertised as interesting for any level developer, you can do a new puzzle every day.

## 24 Days of Blackfire
To help guide you through "the challenges of managing the performance of your applications", Blackfire is publishing 24 tutorials under the [24 days of Blackfire](https://blackfire.io/docs/24-days/index) name.