---
layout: post
title: "We just launched Techademy!"
date: 2011-11-29 11:09
comments: true
categories: 
  - techademy 
  - ingewikkeld 
  - php 
  - training 
  - git 
  - spl 
---
<p>The idea of Techademy is simple: Every month we organize a training day during which attendees attend two workshops. There is a different set of workshops during each trainingday. And for a relatively low fee (currently set at 150 euro), you get a full day of training by experts! Obviously, it won't be as in-depth as the expert training sessions I mentioned earlier (there's only so much time in half-day workshops), but that's not the purpose of the these days: The idea is to inspire and get attendees started on the subjects of the workshops, allowing them to continue learning about it during their regular job or in their spare time.</p>

<p>I am quite excited about this idea and it fits well within my plan to start delivering more training sessions. It also gives me a perfect excuse to work together again with my friend Joshua on a topic that we are both passionate about, sharing our knowledge and experience with others.</p>

<p>To celebrate the launch of this new venture, Joshua and I decided to donate the full proceeds of the first training day to charity. The charity we picked is the "Serious Request" campaign of Dutch national radio station 3FM, who will raise money in the period approaching Christmas for the Red Cross. I'm hoping for a full house during this training session!</p>
