---
layout: post
title: "CSS tricks"
date: 2005-04-19 10:34
comments: true
categories: 
  - leftontheweb 
---
<a href="http://www.evolt.org/article/Ten_CSS_tricks_you_may_not_know/17/60369/">Here's 10 <acronym title="Cascading Style Sheets">CSS</acronym> tricks</a> you may not know about. I didn't know about some of them, and some might actually be useful.

However, I am having a slight problem with trick #4. In my humble opinion, that's more of a bug in <acronym title="Internet Explorer">IE</acronym>. Though it might be useful in some cases, using it might break your layout might the IE developers decide to fix this in either the next minor upgrade of IE or in the next major upgrade of IE. So calling that a trick is maybe not good.
