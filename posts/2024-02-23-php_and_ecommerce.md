---
layout: post
title: PHP and e-commerce
date: 2024-02-23 10:40:00
comments: true
categories: 
  - php
  - ecommerce
  - symfony
  - sylius
  - shopware
  - spryker
  - magento
  - cologne
social:
    highlight_image: /_posts/images/sfugcgn.jpg
    summary: Last night was a new Symfony User Group Cologne meetup and the panel discussion they had on PHP and e-commerce was really interesting. I feel we have several very mature options for e-commerce in PHP these days.
    
---

It had been years since I was in Cologne for anything development-related, so when the new Symfony User Group Cologne meetup was announced and it turned out it was a panel discussion in English about PHP and e-commerce, I decided to pay Cologne another visit. That was a good decision.

## PHP and e-commerce: A match made in heaven?

It was interesting to hear from the representatives of [Magento](https://business.adobe.com/products/magento/magento-commerce.html), [Spryker](https://spryker.com), [Sylius](https://sylius.com) and [Shopware](https://www.shopware.com/en/) about the differences and things in common between the different projects. I learned that Spryker and Magento are pretty strict in what they allow, Sylius is pretty loose and Shopware is somewhere in between. All have their own way of dealing with security as well as working with external plugins. Although Dennis from Spryker made a very good point:

> You need to automate important things such as scanning for security issues. Those mistakes you feel no one will make because they are so easy are the mistakes that you will make yourself at some point

A very interesting discussion was about why these platforms had chosen PHP. While basically the answer for most projects was "because that was the language we were working with at that moment" the panelists were united in that they were still regularly deciding "PHP is still the right choice". One of the main reasons is because it is very easy to quickly implement new features using PHP. The projects do on a regular basis evaluate their choices, and still come to the conclusion that PHP is the right choice. That is good to hear. Then again, a good point was also made that a decision to rewrite the whole platform to another language in a week is not something you make. 

The conclusion for now though is that PHP and e-commerce are still a match made in heaven. Being able to quickly make changes and deploy without having to compile things, the fact that PHP is good at Rapid Application Development and that the language has greatly evolved and has a clear release schedule all make for good reasons to use PHP for e-commerce solutions.

## So what about the next e-commerce project?

It's interesting to have seen the options of the represented projects. I've worked with Magento before (that was not a good experience for me personally, but that was Magento 1 and I hear the newer versions are better) and Sylius (multiple times, and that has been an amazing experience so far). I've not yet worked with Spryker or Shopware so far. I surely hope to do an e-commerce project in the future where I can either use Sylius again or, depending on the requirements, try a different platform. Surely, there are a lot of mature options in the PHP world when it comes to e-commerce, that much is clear.

## Big thanks

Big thanks to [Qossmic](https://www.qossmic.com) for organizing this panel discussion. I had a great time speaking to several attendees, both people I knew before and some people I met during the meetup. And the panel discussion itself was really interesting.