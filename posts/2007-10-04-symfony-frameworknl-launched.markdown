---
layout: post
title: "Symfony-framework.nl launched!"
date: 2007-10-04 13:23
comments: true
categories: 
  - symfony 
---
Ever since I came in touch with the <a href="http://www.symfony-project.com/">symfony framework</a> I've been wildly enthousiastic about it. I've been preaching symfony since I got to know it. I've started translating the documentation to dutch, and really want to promote it in the Netherlands.

To support that effort, I've just launched the first beta of <a href="http://www.symfony-framework.nl/">Symfony-framework.nl</a>, a website to promote symfony towards developers and management alike. It's only the first version, but I want this to grow. Eventually, I might even want to make this more into a local community than just an informational site.

On this site I've implemented the first (very rough) version of the sfFeedAggregator plugin that I'm working on. I am aware that sfFeed2 does do some kind of aggregation, but it doesn't save the aggregated data anywhere. sfFeedAggregatorPlugin is using a database (using Propel at the moment) to save the aggregated data, so that an archive of data is built. I really need to refine the plugin before even releasing a public version, there's still even some design decisions to make before actually doing anything. For now, you can at least view it on symfony-framework.nl
