---
layout: post
title: "Stupid SPAM bots"
date: 2005-03-29 12:06
comments: true
categories: 
  - leftontheweb 
---
Wonderful. These new intelligent SPAM bots that only SPAM sites that can be found when searching for certain keywords. For instance. Take this Mercedes-related site. I just got this e-mail:

<blockquote>Hi,

I took a look at your site a couple of hours ago...
and I want to tell you that I'd really love to trade links with you. I think
your site has some really good stuff related to my site's topic of Mercedes Benz
and would be a great resource for my visitors as it deals with some great
aspects of Mercedes Bbenz that I'd like to give my visitors more information about.

In fact, I went ahead and added your site to my Mercedes Benz HQ Resource Directory at 
[SPAMmed URL]

Is that OK with you?

Can I ask a favor? Will you give me a link back on your site? I'd really
appreciate you returning the favor.

Thanks and feel free to drop me an email if you'd like to chat more about
this.

Best wishes,

Mark</blockquote>

I got this on the e-mailaccount for my website <a href="http://www.fantasylibrary.net/" rel="tag">FantasyLibrary.net</a>. Why, would you think? Well, probably because there is an entry for an author called <a href="http://www.fantasylibrary.net/author.php/84" rel="tag">Mercedes Lackey</a>.

Oh the joys of supposed targetted SPAM.
