---
layout: post
title: "I want to see all of you at DPC!"
date: 2009-01-16 21:41
comments: true
categories: 
  - dpc 
  - dpc09 
  - php 
  - symfony 
  - conference 
  - talk 
  - cfp 
---
<p>The past two editions of the Dutch PHP Conference were a big success, and now it&#39;s time to be part of that success! So start writing that proposal on the topic you know everything of, and submit it for consideration for the Dutch PHP Conference.</p><p>If you&#39;re a bit unsure of how to write a proposal, my colleague Lorna wrote a great article on &quot;<a href="http://www.lornajane.net/posts/2008/How-to-Submit-a-Conference-Talk" target="_blank">How to Submit a Conference Talk</a>&quot;, which may help you along. In case you&#39;re still not sure, feel free to <a href="/who">contact me</a>, I&#39;d be happy to help you out! </p><p>So, no excuses now, start writing those proposals and send them in!&nbsp; </p>
