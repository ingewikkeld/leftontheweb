---
layout: post
title: "Invitation"
date: 2007-05-12 4:13
comments: true
categories: 
  - technology 
---
Everyone, I just want to pass on this invitation to everyone interested:

<blockquote>Dutch Open Projects is organizing it's first PHP Bootcamp for all genuine PHP devotees. Herewith you are kindly invited to register and join the PHP Bootcamp. The PHP Bootcamp will be held on Saturday, June 2nd 2007.

Theme of this PHP Bootcamp is: PHP Frameworks. For the program we have invited a variety of guest speakers relating to the subject of the following PHP Frameworks: Symfony, Joomla! 1.5 and Zend Framework. After the presentations we offer the possibility to having a discussion with the guest speakers regarding the pro's and con's of frameworks.

Following the official program we offer a BBQ. Besides the foods and beverages you also have the opportunity to speaking with all guest speakers and attendees of the Bootcamp.

After all activities you can either go home or sleep over in our backyard. Tents will be provided but do bring your own sleeping bag, towel and toiletries. Before going home the next morning you can have a shower and a coffee and breakfast.

Subscribe now at <a href="http://www.phpcamp.nl/">http://www.phpcamp.nl</a>. Attending is free of charge. Registrations will be closed on Monday, May 28th.

During the oncoming weeks you can find regular updated information regarding the PHP Bootcamp on our website, so please visit us regularly.

In case you have any queries please contact Guido Spee (++ 31 33 4 50 50 53 or guido@dop.nu)

We are looking forward to seeing you on Saturday June 2nd!

Best regards,

Dutch Open Projects</blockquote>
