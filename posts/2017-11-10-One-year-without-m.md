---
layout: post
title: "One year without -m"
date: 2017-11-10 13:10:00
comments: true
categories: 
  - php 
  - git
social:
    highlight_image: /_posts/images/story.png
    highlight_image_credits:
        name: Saku Takakusaki
        url: https://www.flickr.com/photos/sk4e/11663332034/
    summary: One year ago I blogged about stopping my usage of the -m operator when using Git. I'm now reviewing how this has changed my usage of Git.
    
---

One year ago I [blogged](https://skoop.dev/blog/2016/11/08/Get-rid-of-m/) about starting a new practice: Not using `-m` when committing something to Git. `-m` allows you to directly insert the commit message, which makes the whole process of committing faster, but not necessarily better.

## Committing to Git

When you commit your work to Git, you not only make sure the code is in your version control, but you also have an opportunity to document that exact moment in the history of your software. When using the `-m` option, you're very likely to write a very short message. You're not really encouraged to actually document the current state of your code, because writing longer or even multi-line messages is harder in a console.

## Not using `-m` anymore

So, about a year ago I stopped using the `-m` parameter when committing changes to Git. Has it really changed anything?

Yes and no.

Yes, it has changed something in that I now take more time to write the commit message and sometimes take the time to document what is in the change and why the change was made.

No, because all too often I'm still tempted to write a pretty short commit message.

It is still something that I need to focus on more, to take the time to write useful commit messages. Things that allow you to create a timeline of your development out of the list of commits. But most certainly, commit messages have improved since making this little change in my process.