---
layout: post
title: "Patricia Highsmith - The Talented Mr Ripley"
date: 2006-05-17 12:36
comments: true
categories: 
  - books 
---
Everyone of course knows the movie. Not everyone knows the book. This happens a lot of course. I was in the same situation.

I've now read the book. It's nice. It took me quite a while to get into the book. It seems to start very very slow. But once things start happening, the book is very nice. I ended up really enjoying the book. And eventually, you remember your last impression, not your first impression ;)
