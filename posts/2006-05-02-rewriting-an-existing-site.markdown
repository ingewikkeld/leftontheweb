---
layout: post
title: "Rewriting an existing site"
date: 2006-05-02 8:12
comments: true
categories: 
  - technology 
---
I'm currently in the process of rewriting an old site from scratch. The only thing that will stay the same (well, the same, it will be extended probably, but the current structure will stay) is the database. I've chosen to keep the database the same to at least keep away from a painless database migration process. And the current database is pretty good anyway :)

Rewriting can be a drag. The constant feeling of "been there, done that" can be quite annoying. Rewriting can also be an adventure though.

In this case, the current site has a very unstructured, basic ("my code 5 years ago is always horrible") approach. I've chosen to use the <a href="http://framework.zend.com/">Zend Framework</a> as the base for this new site. And working with an existing code base makes your own thinking much more structured. Using a very structured code base written by someone else will make your own code base much more structured. That is the conclusion I've come to. 

I've worked with quite a few frameworks and other systems written by others, but seldom have I encountered such a structured approach to coding as with the Zend Framework. I've immediately adapted my code to the Zend Framework coding structure, and so next to the Zend directory is my own FantasyLibrary directory containing my classes. I'm not using any include_once() or require_once() anymore, but only Zend::loadClass(). The only include_once() call I do is to load the Zend base class.

I could go on and on, and maybe in the future I'll follow up on this, but the important thing in this message is: <strong>Using a very structured code base written by someone else will make your own code base much more structured.</strong>
