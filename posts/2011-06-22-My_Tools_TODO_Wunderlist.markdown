---
layout: post
title: "My Tools: TODO: Wunderlist"
date: 2011-06-22 5:53
comments: true
categories: 
  - tools 
  - todo 
  - wunderlist 
---
<p><a href="http://www.6wunderkinder.com/wunderlist/">Wunderlist</a> is a free TODO system built by 6Wunderkinder. In essence it is very simple: You can have multiple lists, and each list can have multiple TODO items. There are some additional features, such as "starring" an item, which then gets a higher priority (is put on the top of the specified list), the option to put notes with your item, some filtering options (show me all starred items) etc. So far, it may sound a bit standard, and I guess it is. That is what makes it so good, because it doesn't bloat itself with too many features, but just keeps to the necessary basics.</p>

<p>There are two specific features though that I want to highlight because they go beyond the basics but are so enormously cool that they make Wunderlist the tool for me over all the other TODO tools.</p>

<h2>Synchronization</h2>
<p>Wunderlist supports synchronization. Now this in itself is already useful, but it is especially useful when you consider that Wunderlist is available for just about any platform. At the moment, I have the OSX desktop app, the iPhone and iPad apps but also the Windows app running. OSX is my laptop, the iPhone is, well, my iPhone, and the Windows is my desktop at my current client. And they all have the same list of items I need to finish. So if I get another task, all I need to do is put that task in the Wunderlist instance I have open at that time, and it will be available on all the other ones as well. This is really useful if you think of something while at work that you need to research when at home, or when you're in a bar discussing something with a friend and want to follow up on it.</p>

<h2>Sharing</h2>
<p>Another very useful feature of Wunderlist is the sharing option. You can share one (or more) of your lists with other people, providing of course they have Wunderlist. This makes it easy to manage TODO lists also for teams of developers, or between you and your partner, whether it's about things to do, groceries to get or even concerts that you hear about and want to go to. Where the synchronization is useful for individual users, the sharing makes collaboration a lot easier.</p>

<h2>Free</h2>
<p>The only thing I don't like about Wunderlist is that they offer this tool for free, without having even a donate button on the website. I'm not complaining here (of course!) but I believe in paying for the software that help you earn money or that are simply really useful. Wunderlist definitely earns me money (or saves me from losing it) because it will help me remember the things I would otherwise forget. And even if not, it's really useful just for keeping track of my tasks. So, 6Wunderkinder, well done! Thank you!</p>
