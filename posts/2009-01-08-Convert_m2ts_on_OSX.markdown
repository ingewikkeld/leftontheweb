---
layout: post
title: "Convert m2ts on OSX"
date: 2009-01-08 22:10
comments: true
categories: 
  - movie 
  - imovie 
  - m2ts 
  - osx 
---
<p>Searching for something like &#39;<a href="http://www.google.nl/search?q=m2ts+convert+OSX&amp;ie=utf-8&amp;oe=utf-8&amp;aq=t&amp;rls=org.mozilla:en-US:official&amp;client=firefox-a" target="_blank">m2ts convert OSX</a>&#39; gives you a lot of results. However, a lot are quite useless, for instance torrent sites and such. However, there are some discussion forums in the results where solutions like ffmpegx are being mentioned. I am afraid I couldn&#39;t get the <a href="http://ffmpegx.com/">ffmpegx</a>  conversion solution working. It did convert the movies, but the resulting movies were mostly noise and not much of the original image.</p><p>The discussion forums also mentioned some other tools, some of which - as was commented, used libraries that were from iMovie. People on these forums wondered why iMovie didn&#39;t support the m2ts format.&nbsp;</p><p>I decided to start iMovie anyway. When iMovie was running, all of a sudden it recognized the DVD I had in my DVD player as a &quot;camera&quot; and listed all the available movies on the camera. It was my lucky day. As it turns out, iMovie is easily able to simply import the movies if it sees the DVD with the video&#39;s as a camera.</p><p>The resulting movies are of excellent quality and definitely usable for video. I&#39;ll use iMovie to export to a format Premiere will accept and edit in Premiere, as I for some reason can&#39;t get used to the iMovie&#39;08 editting interface. </p>
