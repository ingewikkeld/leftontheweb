---
layout: post
title: "Moving companies again"
date: 2007-01-15 12:48
comments: true
categories: 
  - personal 
---
I know, I've been with <a href="http://www.dop.nu/">Dutch Open Projects</a> for only a few months, and already there's new developments. I'll still have my ties with Dutch Open Projects, but my contract will move to another company.

One of the owners of Dutch Open Projects is taking the <a href="http://www.jongereninbeeld.nl/">most successful project</a> into a new company. As I've been mainly working on this project, he has asked me to "go with him" into this new company. After some thinking, I've decided to accept the invitation, to be able to work nearly full-time on the development of new features for this application.

My ties with Dutch Open Projects will not be severed however. I will still be involved with them at least for some time, to bring structure into the development of that company. Also, we'll still be working from the same office as before, just under a different name and focussing (for now) solely on Jongeren in Beeld.
