---
layout: post
title: "Image to CSS"
date: 2005-04-27 4:22
comments: true
categories: 
  - leftontheweb 
---
In the category 'useless but so friggin cool', <a href="http://elliottback.com/wp/archives/2005/04/25/convert-image-to-css/">Elliot Back brings the Image to CSS converter</a>. Amazing. Looking really really good. For instance, have a look at the <a href="http://elliottback.com/tools/image-to-css.php?url=https://skoop.dev/images/seperator.gif&pix=1&mode=0">seperator image I am using</a>. I say: wow!
