---
layout: post
title: "New Ufdi.net member: FreeBizWare"
date: 2006-10-25 4:18
comments: true
categories: 
  - technology 
---
Ken has a lot of ideas. He has <a href="http://www.59ideas.com/">59 ideas</a>, and seemingly just as many weblogs. Well, not really, but he has quite a list of weblogs on various topics. I am subscribed to quite a few of his weblogs by RSS, and enjoy reading just about everything. One weblog especially always has my interest, and that is <a href="http://www.freebizware.com/">FreeBizWare</a>, Ken's weblog about open source software. Ken is able to find a lot of unknown (to me at least) open source software that is very useful, in work, but sometimes also for private use. This excellent location to discover new technology is now part of the Ufdi.net network!

Welcome Ken. Good to have you aboard.
