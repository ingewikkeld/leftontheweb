---
layout: post
title: "The Witch Hunt"
date: 2005-07-10 1:59
comments: true
categories: 
  - leftontheweb 
---
It seems I missed out on another fun Internet thing lately: The <a href="http://japundit.com/archives/2005/06/30/808/">puppy poo girl</a>. A girl who had her dog with her on the subway, and he dog dumped some crap right there. She only had a a few words to spare for the elderly people asking her to clean it up: "fuck off".

What happened next was someone with a cameraphone taking a picture, posting it on a popular website, and the hunt was on. Shortly after, they found out all information about the girl, and a public shaming campaign started against the girl.

This whole situation started a discussion about the ethics of such a public shaming campaign. I'm not sure yet how I feel about this. On the one hand, if you know something like this can happen, you might be more careful in your response in a situation like this (maybe even do The Right Thing&trade; and actually clean up the mess). On the other hand, is it ethical do ruin the life of someone in this massive way? Maybe it is. Maybe it isn't. I'm not sure yet about that.

(<a href="http://japundit.com/archives/2005/06/30/808/">Here's the image</a>)
