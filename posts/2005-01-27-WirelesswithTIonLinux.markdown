---
layout: post
title: "Wireless with TI on Linux"
date: 2005-01-27 7:48
comments: true
categories: 
  - leftontheweb 
---
If you're running Linux and you have a wireless card based on the <a href="http://focus.ti.com/docs/apps/catalog/overview/overview.jhtml?templateId=1101&path=templatedata/cm/level1/data/wire_ovw&DCMP=TIHomeTracking&HQS=Other+OT+home_a_wireless">Texas Instruments</a> chipsets, then you don't need to search for any Linux drivers for those cards. That's what I found out <em>after</em> purchasing a license for my D-Link wireless card from <a href="http://www.linuxant.com/">Linuxant</a>.  It seems that Linuxant has landed a deal with Texas Instruments, resulting in the free use of the Linuxant Driverloader software for users of TI-based wireless cards. Now that's what I call good service from TI.  Also good service by Linuxant, by the way, who promptly refunded the license I bought.

If you're NOT using a TI-based card, however, the Linuxant Driverloader software is still a very good buy. I have it running on my laptop (Intel Pro Wireless 2200 card) and it works perfectly. And the installation was dead easy. Definately a well-spent $20.
