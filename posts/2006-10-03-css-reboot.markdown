---
layout: post
title: "CSS Reboot!"
date: 2006-10-03 15:01
comments: true
categories: 
---
Yes! Signups are open for the new <a href="http://www.cssreboot.com/">CSS Reboot</a>! I have just entered this site for the reboot. The design is nearly done, I only need to finish some small details. But I'll keep you guys in the dark for a while. I promise the colours will be completely different :)
