---
layout: post
title: "Zend PHP5 Certified"
date: 2007-12-14 8:11
comments: true
categories: 
  - personal 
---
<p>I was not really nervous before the exam, but for some reason during the exam the nerves played up anyway. This wasn&#39;t really triggered by the questions though, because even though they were sometimes quite tricky and required reading over them two or three times to ensure you&#39;ve understood things correctly, mostly it was common sense.</p><p>The road to certification was a long one. First, I wanted to get it myself. For some reason, that never happened. With my previous employer, after quite some talking, I got it arranged that they would start the process of getting developers certified, but I left before that actually got off the ground.</p><p>Ibuildings.nl, however, has the (in my humble opinion) healthy attitude that developers should be certified. So when the opportunity presented itself to get certified, I jumped right on it.&nbsp;</p><p>I did some preparation, mainly by reading the Zend Certification Study Guide by Davey Shafik and Ben Ramsey, and testing my knowledge using PHP|Architect&#39;s Mock Testing system. And even though most of it was common sense and basic knowledge, I was still happy I prepared. It just helps getting used to the exam structure and content.&nbsp;</p><p>Anyway, I passed the exam, so now I am a Zend Certified PHP5 developer. :)</p>
