---
layout: post
title: "Hattrick Organizer on Kubuntu"
date: 2006-02-19 4:15
comments: true
categories: 
  - technology 
---
As I may have mentioned before, I play an online game called <a href="http://www.hattrick.org/">Hattrick</a>, which is an online football management game. For this, I use a tool that helps me playing called <a href="http://www.hattrickorganizer.de/">Hattrick Organizer</a>.

After installed Kubuntu, I already tried to use this but ran into an error. Today, as I had to enter my line up for the upcoming match of this evening, I decided to see if I could fix this problem. Indeed, the problem is fixed, and it seems to be pretty easy.

The problem was described on <a href="http://www.wow-auctions.net/ho/phpBB2/viewtopic.php?t=3870&highlight=exception+during+runtime+initialization">The HO forums</a>, and the solution was also listed. Apparently, Kubuntu comes with an open source version of Java, not the Sun version. I had the choice of either installing the Sun version, or using the Sun version if it was already present on my system. It is already on my system, as I am a happy user of <a href="http://www.zend.com/products/zend_studio">Zend Studio</a>. So the problem was easily solved. I just edited my HO.sh, which launches Hattrick Organizer. Problem solved. I can again use Hattrick Organizer without a problem :) That's the power of a support community for you.
