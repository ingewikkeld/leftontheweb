---
layout: post
title: "Technorati"
date: 2005-02-01 6:16
comments: true
categories: 
  - leftontheweb 
---
Lately, I've been looking a bit more into Technorati and it's system. Quite impressive, and very interesting. I already added a technorati link to my <a href="http://www.stefankoopmanschap.nl/weblog/">dutch weblog</a> but hadn't done that here yet. So just now I've added the Technorati link here as well.

Now, I'm already planning to write a plugin for <a href="http://www.nucleuscms.org/">Nucleus</a> to include Technorati Tags in that system, but I guess it would be good to also have an extension for <a href="http://www.pivotlog.net/">Pivot</a>. I don't think the extension system is not really documented yet.
