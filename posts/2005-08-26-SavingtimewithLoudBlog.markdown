---
layout: post
title: "Saving time with LoudBlog"
date: 2005-08-26 8:29
comments: true
categories: 
  - loudblog 
  - software 
---
I have been planning to do some sort of radio show for my website <a rel="tag" href="http://www.electronicmusicworld.com/">Electronic Music World</a> for quite a while. One of my plans was to write a module for <a href="http://www.webkoffer.org/">WebKoffer</a> which handled this. But too little time and a bit of laziness always held me back.  Yesterday, <a href="http://www.christophersisk.com">Chris Sisk</a> pointed me towards <a rel="tag" href="http://www.loudblog.de">LoudBlog</a>, an open source PHP/MySQL weblog/CMS package that is meant especially for PodCasts (or <a href="http://www.audiofeeds.org">AudioFeeds</a>, if you don&#39;t want to use the Apple term too much). This sounded like a good solution, especially since you can also link to third party sites that hold mp3&#39;s, something that is quite useful in saving diskspace, especially with services like <a href="http://archive.org/">Archive.org</a> and <a href="http://scene.org/">Scene.org</a>.  And yes, I am quite impressed with LoudBlog. It&#39;s easy to install, and quite easy to manage your AudioFeed. Adding new files/posts is simple, and altering the layout to fit in with the rest of your site is also quite easy with the (XSLT-based?) template system. So yeah, I&#39;m quite happy with it. Check it out at the <a href="http://radio.electronicmusicworld.com/">Electronic Music World Radio</a>.
