---
layout: post
title: "PHPCon Italia coming up!"
date: 2009-03-16 22:36
comments: true
categories: 
  - conference 
  - php 
  - symfony 
  - refactoring 
---
<p>As I will be flying in on wednesday, I&#39;ll be missing the workshops. Quite a shame, as Sebastian&#39;s workshop on Quality Assurance in PHP looks quite interesting. I guess I&#39;ll have to catch that one elsewhere. Looking at the <a href="http://www.phpconference.nl/schedule/" target="_blank">DPC schedule</a>  there&#39;s still one slot open... oh Cal, look what is very interesting! ;)</p><p>On thursday I&#39;ll be speaking in the first timeslot after the opening. At that point, I will be doing my myphpbusters talk about the symfony framework. I&#39;m competing with two italian sessions, so I&#39;m hoping enough visitors will be comfortable enough with the english language to come by. After the coffee break I would like to attend <a href="javascript:NF(&#39;http://entwickler.com/konferenzen/planer/show_details.php?konferenzid=94&amp;sessionid=9483&#39;)">Agile development and domain driven design</a> by Jacopo Romei. After lunch and keynote it&#39;s my turn again with my refactoring talk. Now, the rewritten talk that I&#39;ve first given at the 4developers conference is one I&#39;m quite happy with, so I&#39;m really looking forward to giving it again. I&#39;ve got some improvements lined up before doing it on thursday, which will improve it even more. It will be awesome! I am, however, competing against the likes of Sebastian Bergmann and Lars Jankowfsky, so I hope enough people will find refactoring an interesting topic :)</p><p>After my session the day is closed by three sessions in Italian, so I&#39;ll just hang out and talk to people. If you&#39;re visiting the conference, please do come by and say hi, I&#39;m very interested in meeting people from the Italian PHP community! </p><p>On friday I&#39;ll be flying back but hopefully I&#39;ll be able to attend Lukas&#39; keynote and maybe his session on freetext search (but I may need to leave before that one ends). I may opt out of visiting the freetext search session to not disturb the session when I have to leave, and instead maybe talk to some people at the conference.</p><p>If you&#39;re in Italy and do PHP: Come by in Rome this week. It&#39;s gonna be lots of fun! </p>
