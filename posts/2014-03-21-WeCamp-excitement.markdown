---
layout: post
title: "WeCamp excitement"
date: 2014-03-09 11:00
comments: true
categories: 
  - php 
  - wecamp 
  - conferences
---
Last night I made one of the most exciting announcements of my professional life. At the [AmsterdamPHP](http://amsterdamphp.nl/) meetup I announced the first conference-like event that we're organizing with the [Ingewikkeld](http://php.ingewikkeld.net/)-crew. One of those things that I've done quite a while now for different usergroups (organizing events) is now also part of my work, and that feels pretty good.

Ever since I organized my first event (PHPCamp) for the company I worked for at that moment ([Dutch Open Projects](http://dop.nu/)) I've liked doing that. Arranging speakers, the venue, sponsors, thinking of cool stuff to do. All that stuff is awesome, and I love that. From the moment I started Ingewikkeld I wanted to do an event. And we're finally doing it!

To give you some background: I love conferences. They inspire me to dive into new technologies, research topics, hack around a bit, play with stuff. My main frustration around conferences is, though, that once I get back from the conference, I need to get some work done again. And usually that means my "technology to check out"-list only grows longer. I hardly have time to really dig into things after the conference is over.

Combine that with the fact that I learn much more when [actually doing stuff](https://skoop.dev/blog/2014/02/07/Prototyping-your-experience/) and I hear the same thing from many people I talk to. I thought it would be good to do an event where people don't just listen, but actually work on a project.

On a great day somewhere in the summer last year, [Mike](http://php.ingewikkeld.net/teammember/mvriel), [Jelrik](http://php.ingewikkeld.net/teammember/jelrikvh), [Marjolein](http://twitter.com/tearsong) and myself sat down for a brainstorm. We started just throwing out ideas to see what we could do. We let it sink in for a while, and then started working on filling in the blanks, finalizing some ideas, and arranging things we had decided on.

The location turned out to be the first major challenge. Our initial idea was to find some kind of boyscout-venue. This would ensure electricity, some basic facilities and hopefully an Internetconnection. It turned out this was actually pretty hard, and we didn't find such a location. We had a quick brainstorm to think of alternatives. Campsites were an option, but then we started considering more special locations: islands. Some searches led to island De Kluut, and after a meeting with someone from De Kluut, we knew we wanted to get this place.

There was only one big challenge: The island is very natural in that it does not have cables of any kind leading to the island. Electricity is not really an issue: They have solar energy collectors and backup generators. But the other pretty big challenge when you don't have cables is an Internet connection. We had two main options that both turned out not to work for our situation. Eventually, someone came up with a company that provides sattelite Internet for events, and that turned out to be exactly what we needed.

For ticket sales, [EventBrite](http://www.eventbrite.com/) was our best option and since I'd had enough positive experience from the buyer-side, and after researching it could do all we wanted, I signed up for an organizer-account and set up the event. This was very easy. The only frustrating thing was that I had to make my event public before being able to get an embed-code from their control panel. Since we were trying to create a hype by anonymously dropping some goodies at local usergroups, we didn't want any trackable lines leading back to us. With some last-minute working, we got all of this sorted. Apparently, there were people thinking WeCamp was initiated by [Ibuildings](http://www.ibuildings.nl/) or [AmsterdamPHP](http://www.amsterdamphp.nl/). Only a few people actually thought it was us.

And now it's there. We've announced. A good feeling. Especially with all the positive response we get from people. Everyone basically says the idea is awesome. And ticket sales are already going better than expected. All early bird-tickets are already gone. And since we're keeping this event small, that means there's only 30 regular tickets left. Exciting times. There's still much to do (organize fun activities, arrange some more sponsors to at least break even, arrange a couple more coaches, etc) but it's a lot of fun. I'm looking forward to the last week of August. It will be EPIC. It will be [WeCamp](http://weca.mp/)! 
