---
layout: post
title: "Hey, what's this?"
date: 2005-09-17 14:10
comments: true
categories: 
---
You might notice that something has changed here. Actually, quite a lot has changed. I've moved from <a href="http://www.pivotlog.net/">Pivot</a> to <a href="http://www.textpattern.com/">Textpattern</a>, and in the process I obviously also moved to a whole different design. I'm now using the default installation of <a href="http://textgarden.org/layouts/29/Green%20Marinee">Green Marinee</a>, though I am planning on changing some stuff in the near future to make it more to my liking. But even the basics are already to my liking :)

Why did I move away from Pivot? Good question. First of all, because I like to try different CMS'es and weblog packages. Textpattern also seems to me to be a more complete CMS, which makes it easier to expand this site in the future. And last but not least, I recently had a problem with Pivot which caused me to temporarily panic as I thought I had lost all but three posts. It ended up being solved easily (by rebuilding the index file) but I thought to be on a slightly more safe side by moving to a MySQL based software package.

We'll see how it works out. I definately have the idea that this move isn't a bad one.
