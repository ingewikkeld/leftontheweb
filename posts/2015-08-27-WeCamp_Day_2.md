---
layout: post
title: "WeCamp Day 2"
date: 2015-08-27 10:00
comments: true
categories: 
  - php 
  - wecamp15 
  - wecamp 
---

Yesterday was day 2 of WeCamp and it was an interesting day. While gathering requirements in day 1 we created two *spikes*, topics to research to find out if we could actually do what we assumed we could. So in the morning, two people started on the research, while two other team members started on setting up the vagrant box and the Laravel project (we decided to go with Laravel for our project on day 1).

The first bit of research was quickly finished with a successful result, however the second one caused some problems: It turned out we could not do what we had assumed we could do. This meant we had to go back to the drawing board for at least part of the application we're building. 

After a very constructive and successful session we decided that our change in scope wasn't all that big. We could still build our main scenario in a slightly different way. So we decided on a new list of tasks to focus on and started building those. At the start I noticed that my team members were all working very much as isolated units, but as the day progressed a lot of interaction started happening between team members. This was a nice development.

Another thing I focussed on as a coach was to set personal goals with each team members. So during the afternoon, I had private conversations with each team member to determine what goals they wanted to set. We talked about work, life and ambition and set one or two long-term goals (for "life after WeCamp") and one short-term goal ("what do I want to have learned/done during WeCamp?"). Some of the short-term goals immediately had an effect on the work we were doing in the team, so we made some changes to the team dynamic to give the people with those goals the opportunity to reach those goals.

We wrapped up the day in a really positive vibe with some excellent progress on our project and went to dinner. After dinner it was time for the Persgroep Gamenight. I pretty much lost track of my team members at that point, each went to play their own choice of games. In my case, this was: Dixit, Exploding Kittens, Masquerade and more Dixit. Being the responsible adults we are here at WeCamp (*grin*) we turned off the light at 1:30 and went to bed.

As I'm writing this, day 3 has started and the team is already working on the project again. I'll try to summarize today in another blogpost tomorrow morning.
