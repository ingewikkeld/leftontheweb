---
layout: post
title: "My schedule for PHP UK Conference 2009"
date: 2009-02-23 21:32
comments: true
categories: 
  - conference 
  - phpuk2009 
  - php 
---
<p>Obviously I&#39;ll try to visit the keynote, and I should, really, because the keynote by Aral Balkan sounds interesting enough. After the keynote, I&#39;ll be wanting to visit David Soria Parra&#39;s talk on Sharding Architectures. Having been seriously introduced to a sharding architecture last year in a project, but not seriously exposed to the details of that architecture, I really want to see what it is all about.</p><p>After that I want to check out David Axmark&#39;s Drizzle talk. I&#39;ve not given Drizzle a serious look yet, and I want to know more. A perfect opportunity to get it from the source.</p><p>The first timeslot after lunch will be the hardest to choose from, and I haven&#39;t made this decision yet. On the one hand, I&#39;ve not seen Stuart Herbert&#39;s talk Living with Frameworks yet and I really would like to see it, having a pretty serious framework background ;) but on the other hand, having talked to Hank Janssen before about PHP and Windows, I already am quite curious on what Hank has to say. I&#39;ll probably decide then and there during lunch which one it will be.</p><p>After that is my timeslot. I am luckily programmed against Mihai Corlan&#39;s Flex talk, which I&#39;ve seen already at the Frontend Special of the Dutch PHP usergroup. That means I won&#39;t miss something... I already have the information ;)</p><p>In the last slot, I am aiming for attending Chris Shiflett&#39;s Security Centered Design talk. It sounds like a nice talk and is definately a topic I&#39;m interested in.&nbsp;</p><p>Overall, PHP London have been able to select a top schedule with awesome speakers and a nice list of topics covered. I&#39;m very much looking forward to it! </p>
