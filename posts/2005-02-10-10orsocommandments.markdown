---
layout: post
title: "10 or so commandments"
date: 2005-02-10 4:17
comments: true
categories: 
  - leftontheweb 
---
The 10 (or so) Commandments of Web Design

SUNNYVALE, CA (REUTERS) -- A recently located makeshift 'instruction manual', has been located under the backseat of Tim Berners-Lee's car. The 'manual', a short treatise on web design principles, was scrawled on a series of old Denny's napkins and has revealed the lost 10 Commandments of Web Design - now made public, exclusively at CoolHomepages.com!Commandment 1: Thou shalt be clear about what the hell thou art presenting to the user. Unless your user is the Amazing Kreskin.

Commandment 2: Thou shalt design the site so that it's appropriate for your audience. Don't use a dark Tomb-Raider theme if you're an online shoe store or a financial service.

Commandment 3: Thou shalt not use java applets unless Moses himself appears in a burning pile of copier paper in your office and insists that you should.

Commandment 4: Thou shalt not force a user to wait for a megabyte or more of Flash file downloading as the first thing they experience at your site.

Commandment 5: Thou shalt make the more important parts of any given page layout more prominent than the less important parts of your page.

Look at the newspaper. The front page headline is the biggest. There's a good-sized page 1 photo keeps the page from becoming a visual wasteland of black type. There are headlines above each story. There are subheads within some stories. The body copy is legible, but not too big or too small. The author bylines are small. There is a menu somewhere on the page telling you how to find movies, sports, etc. The page is organized in easy to understand columns. There's a reason why every major newspaper in the world adheres to these basic design principles.

But don't make your web designs look like newspapers. That's not the point! If you don't get the point yet, wash, rinse, and repeat the preceding paragraph.

Commandment 6: Thou shalt not use too many colors. Thou shalt attempt to learn something about color harmony.

Commandment 7: Thou shalt kern thy bitmapped type unless thou wantest to be known simply as "Shmendrick, the Village Design Idiot"

Commandment 8: Thou shalt look at thy site on a variety of browsers. Just because you're on a PC using IE 6 doesn't mean your users are. You can't accommodate every browser config for sure, but it's horrifying how many site submissions we get that ONLY look good on a PC on IE 6.

Commandment 9: Thou shalt make sure your images are clear, and free of those dreaded Mad-Cow disease causing jaggies. On somebody else's site, jaggies are hilarious -- but not on your site.

Commandment 10: Thou shalt not be accused of Flash-love. It's habit forming and causes hairy palms and blindness. Everything in moderation grasshopper. Use Flash when it's appropriate.

Commandment 11: Thou shalt learn as much as possible about typography, layout, and color as soon as possible.

Commandment 12: Thou shalt achieve BALANCE in thine page layouts. Look at the elements on the page as if they all possess weight and gravity. The larger items possessing more weight than the smaller ones. Imagine a seesaw underneath all the page elements, supporting the entire page layout. The seesaw should seem as though it would balance itself due to the weight of the elements being evenly distributed. Not too much weight on either side so as to 'tip' the seesaw.

Commandment 13: Thou shalt proofread, proofread, proofread. Then do it again.
Contributed by: CJackson www.junkology.org

Commandment 14: Thou shalt have clear and easy to use navigation which does not change with each new page.
Contributed by: Ely Cannon

Commandment 15: Thou shalt not use the HTML tag &lt;BLINK&gt;
contributed by: Mike Hagan

Commandment 16: Thou shalt not loop a sound over and over without a 'Sound Off' button. No matter how cool you think the sound loop is.
Contributed by: Anthony Keenan www.custombike.org

Commandment 17: Thou shalt not apply for web-design jobs after a single community college class on using FrontPage.

Commandment 18: Thou shalt not using Comic Sans as a header font unless thou wants to induce vomiting for someone that has ingested poison. (Ed. Note: This is my favorite Commandment!)

Commandment 19: Thou shalt not use horizontal rules that are rainbow animations unless you are a Hawaiian.

Commandment 20: Thou shalt not sneak in links to their sponsors as ways to increase click-throughs. We will hunt you down and kill you for this!

Commandment 21: Thou shalt realize that 42 fonts is not better than two on a page.

Commandments 17-21 contributed by:
Josh Spivey www.jlswebsource.com

Commandment 22: Thou shalt look up the word 'consistency' in Webster's Dictionary before layout out all your site pages. The user should not be confused with a different page design every time they click to a new page.
Contributed by: Chris Irwin

Commandment 23: Thou shalt use thy Spell Checker.
Contributed by: Danna Griego www.pinkflamingodesign.com

Commandment 24: Thou shalt see a doctor and get rid of it if thou sufferest from "Pop-Up Window Madness". Pop-ups are not meant to be used as a way to get to the next page.
Contributed by: Rich Rodecker www.testa.com

Commandment 25: Thou shalt optimize thine images
contributed by: Ann Schwartz

Commandment 26: Thou shalt not use the same Photodisc photos that everyone else uses.
Contributed by: Mark Priestap

Commandment 27: Thou shalt attempt to design pages that look great on low-end or high-end systems. To achieve this is truly a sign of divinity.
Contributed by: Mark Kathmann www.kathmann.com

Commandment 28: Thou shalt not use frames unless there is a need.
