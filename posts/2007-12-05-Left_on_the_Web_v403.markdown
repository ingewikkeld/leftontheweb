---
layout: post
title: "Left on the Web v4.0.3"
date: 2007-12-05 14:00
comments: true
categories: 
  - leftontheweb 
---
<p>The main changes in this update is a more readable weblog post screen. I&#39;ve added some whitespace that will hopefully make the page a lot more readable (thanks Davey). And another thing that was fixed was the comment order. There was a bug making the comments appear in a weird order.</p><p>Anyway, it&#39;s again a bugfix release. A release with more shocking changes will follow some time in the (near/far) future.&nbsp;</p>
