---
layout: post
title: "Scotty got beamed up"
date: 2005-07-20 13:05
comments: true
categories: 
  - scifi 
  - startrek 
---
It seems Scotty got beamed up himself. <a href="http://www.cnn.com/2005/SHOWBIZ/TV/07/20/obit.doohan.ap/index.html">James Doohan</a>, Scotty in Star Trek, passed away. Now the real beaming can begin...
