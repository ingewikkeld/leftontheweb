---
layout: post
title: "IDM continued"
date: 2005-11-03 6:00
comments: true
categories: 
  - music 
---
After publishing my <a href="https://skoop.dev/article/153/idm">Essay on IDM</a> earlier I got contacted by the editor of <a href="http://www.igloomag.com/">Igloo Mag</a>, a quite fine Internet publication. He wanted to publish the essay as a feature on their website. I was quite honored, so <a href="http://igloomag.com/doc.php?task=view&id=1155&category=features">I said yes</a>.

While you're there, also check out the rest of the site. Igloo rules.
