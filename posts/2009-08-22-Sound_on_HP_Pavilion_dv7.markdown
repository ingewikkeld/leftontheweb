---
layout: post
title: "Sound on HP Pavilion dv7"
date: 2009-08-22 11:57
comments: true
categories: 
  - linux 
  - opensource 
  - community 
  - kubuntu 
  - hp 
  - pavilion 
  - dv7 
  - sound 
  - google 
---
<p>This is another example why the community around open source projects is awesome. It is the perfect example of why you should share your solutions with the world at large. Whether you do it on your own weblog, on a forum, in a mailinglist. The solution to a lot of common problems is a single google away.</p><p>This new laptop I have, it is awesome. I mentioned that before. But sound was not working. And music is one of the most important things I need when I&#39;m working. So I just sat down and started googling. Since I run Kubuntu on my Pavilion, my search query was simple:</p><blockquote><p>kubuntu sound pavilion</p></blockquote><p>I click <a href="http://ubuntuforums.org/archive/index.php/t-993075.html" target="_blank">the first link</a>  in <a href="http://www.google.nl/search?q=kubuntu+sound+pavilion&amp;ie=utf-8&amp;oe=utf-8&amp;aq=t&amp;rls=com.ubuntu:en-US:unofficial&amp;client=firefox-a" target="_blank">the results</a>  and the original poster of the thread describes a problem that sounds (pun intended) just like mine. The first person that responds describes a very simple solution. The solution makes sense: adding a line in the configuration of the Alsa module that leads a specific option for the Intel HDA chipset, the chipset that powers the dv7. I add the line, reboot. It works!</p><p>Don&#39;t you love open source communities? </p>
