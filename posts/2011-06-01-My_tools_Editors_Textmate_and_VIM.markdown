---
layout: post
title: "My tools: Editors: Textmate and VIM"
date: 2011-06-01 5:33
comments: true
categories: 
  - tools 
  - editors 
  - vim 
  - textmate 
---
<h2>Textmate</h2>
<p>Using a Mac, Textmate was probably one of the first apps I've ever purchased. And a great purchase it was. With the Textmate bundles you can extend the editor to be an editor for just about any language you want. It can also do cool stuff such as colorcoding diffs or converting a Markdown document to HTML. And there's stuff like spelling and grammar checks. I use Textmate for quickly editting files, for comparing stuff, as a diff tool but also to write most of my blogposts or articles in. It's a very clean interface, which is very nice. No clutter, it just works.</p>

<h2>VIM</h2>
<p>Obviously, when working in a linux virtual machine or when connected to an external server through SSH, I won't have Textmate available to me. In those situations, I use VIM. Even though it is slightly harder to use (you have to remember all the key combinations) I've gotten used to it and most of the common combinations are in my head now. Just like Textmate, I like VIM because of the clean interface, there's nothing really cluttering my interface. And when configured well, it will do code highlighting for PHP and any other language you want.</p>

<p>I guess the most important thing for me in the editors is their speed. You can very quickly use both editors for the quick stuff. For projects, I use an IDE, but for quick editting, these tools are awesome.</p>
