---
layout: post
title: "A say day"
date: 2006-12-21 14:29
comments: true
categories: 
  - personal 
---
As I <a href="https://skoop.dev/article/296/being-lead-by-mixed-emotions">wrote earlier</a>, we've been living with quite some emotions lately. Today, Marjolein's grandpa died at the age of 84. Today is the birthday of Marjolein's grandma. A sad birthday.

Of course, given the situation, it may be better that he won't have to suffer anymore. Still, when it happens, you feel sad.
