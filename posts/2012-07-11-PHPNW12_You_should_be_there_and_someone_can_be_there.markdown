---
layout: post
title: "PHPNW12: You should be there, and someone can be there"
date: 2012-07-11 8:44
comments: true
categories: 
  - php 
  - phpnw 
  - phpnw12 
  - freeticket 
  - conferences 
  - manchester 
  - ingewikkeld 
---
<p>It all started last year: I decided I wanted to go to PHPNW even if I wasn't accepted as a speaker. Being Dutch, I made sure to purchase my ticket for the conference during the blind bird period to save some money. But then I got accepted as a speaker. Part of the speaker package is a ticket to the conference, so I had this spare ticket.</p>

<p>One option would be to get a refund.  The PHPNW crew had no problem with this. But then I figured I could actually also try and do some good for the PHP community with the ticket. I'd spent the money already, why not use it for someone's benefit.</p>

<H2>Giving back</H2>
<p>When I was just starting out with PHP, 15 years ago this year, I got so many help from the PHP community in my learning and understanding the language. Whether it was through blogposts, the documentation, IRC channels, even e-mails from people who reviewed the code of some stuff I open sourced and found security flaws, I got so much help. So I decided to try and help someone to learn as well.</p>

<p>From my involvement with PHPBenelux, I knew Tobias Gies, who had been a volunteer at the PHPBenelux Conference and had been a great asset there. I knew Tobias is usually not in a position to get around to a lot of conferences, so I suggested he take my ticket, so he only had to arrange for travel and lodging. Tobias accepted, and he had an awesome time in Manchester. He actually had such a good time that he decided to attend PHPNW again this year.</p>

<h2>2012</h2>
<p>This year, the same thing happened: I purchased my blindbird ticket, but got accepted as a speaker. And again, I want to support someone who otherwise would not be able to attend the conference. This can be someone local to Manchester (or the UK), but could also be someone internationally. I will only raffle a ticket, so whoever wins has to be able to cover travel and lodging expenses.</p>

<p>Where last year I contacted someone I already knew, this year, I'm open to anyone! However, I don't want people applying for the ticket themselves. So what I want to do is that you can nominate someone to win the ticket. I'm mostly looking at members of the PHPNW usergroup here, but also to students who want to nominate fellow students, people who want to nominate a colleague, or friends. International submissions are welcome, but you have to be certain that the person you submit for will be able to cover travel and lodging expenses!</p>

<h2>How to nominate?</h2>
<p>Nominating someone is easy. Send an e-mail to phpnw12@ingewikkeld.net, listing the following information:
<ul>
<li>Name of the person you're nominating</li>
<li>E-mailaddress of the person you're nominating</li>
<li>The reason why I should pick this person</li>
</ul></p>

<p>Out of all entries made before August 15th, I will pick someone based on the reasons submitted in the e-mail. The winner will be contacted before the end of August, and announced as soon as the winner has confirmed (s)he will be able to attend.</p>
