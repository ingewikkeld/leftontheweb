---
layout: post
title: "SymfonyCamp - All I can say is WOW"
date: 2007-09-10 12:01
comments: true
categories: 
  - symfony 
---
When we did the PHP Bootcamp in June of this year, it was an unexpected success. The PHP-wide one-day event on PHP Frameworks attracted about 35 visitors, and everybody was really happy about the event.

The idea of SymfonyCamp had come up even before that, but after the success of the PHP Bootcamp we decided to go through with it. Of course, we were really too busy with other things to actually spend time on it, but we went through with it anyway.

A lot of things got arranged at the last possible moment, even the speakers. Just before my holiday a few weeks ago, about 20 people had registered based on an incomplete program. I throught that was a great number. I finished most of the program just before my holiday, and went on my one-week holiday feeling good about the event that was upcoming.

There was a huge shock after coming back from my holiday. The number of registered people had more than doubled! Wow! I got a bit nervous and we needed to arrange some extra stuff, but it still looked wonderful.

And wonderful it was. I really felt great during the whole event. What a great bunch of people are those that came over for the event, and what great speakers we had. Due to the unexpected extra work, I couldn't spend enough time on my own presentation, and I feel bad for actually going through with it based on what I had. But I did it anyway, and though was short, incomplete and probably not *that* good, I think it went as good as it could go with the little amount of preparation that went into it.

I think I can live on the buzz that I got during the past week for about 6 months. That leaves a few months until the next SymfonyCamp, which will probably be before the summer holiday next year. One thing is certain: We're going to do this again!

Thanks everyone for coming over and making this a great event! I've thoroughly enjoyed myself. See you guys next year!
