---
layout: post
title: "The Digg System"
date: 2006-08-08 13:30
comments: true
categories: 
  - technology 
---
Everybody knows <a href="http://www.digg.com/">Digg</a>. And that is why I don't really check the site out anymore. Even though Digg still seems to be able to keep some kind of quality to it's content, a lot of useless junk is submitted. When the mass discovered Digg, it went downhill and wasn't as much fun anymore.

So I was glad to find the dutch incarnation of Digg a while ago: <a href="http://www.ekudos.nl/">eKudos</a>. A small but active group of people was submitting stuff to the site, writing nice comments with each link. But lately, it's gone downhill. Not really because "the mass" has discovered it, but because some developers have discovered a way to (automatically?) post to eKudos from Wordpress and the dutch low-quality weblogging tool <a href="http://www.punt.nl/">punt.nl</a>. And so lately a lot of crap is being submitted to the site, making it less fun to check out the latest submissions. This may become their downfall.

I hope that the developers behind eKudos will find a way to prevent this, and to maintain the high quality of the content that it previously had. I'd hate to see eKudos go downhill.
