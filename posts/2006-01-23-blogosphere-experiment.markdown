---
layout: post
title: "Blogosphere experiment"
date: 2006-01-23 13:01
comments: true
categories: 
  - weblogging 
---
How quick will something spread in the blogosphere. Let's find out, is what <a href="http://59ideas.wordpress.com/">59ideas</a> thought. So he's offering <a href="http://59ideas.wordpress.com/2006/01/23/free-domain-give-away-idea/">free domain names</a> to random commenters. Go ahead, comment there. Oh, and do check out the rest of this weblog, it's worth the reading.
