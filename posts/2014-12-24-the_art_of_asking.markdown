---
layout: post
title: "The Art Of Asking"
date: 2014-12-24 11:00
comments: true
categories: 
  - php 
  - book 
  - review 
  - psychology 
  - life 
  - theartofasking
---

With [Ingewikkeld](http://php.ingewikkeld.net/) we've sent out a gift to some customers and other contacts. A book that has *nothing* to do with PHP. While this may seem odd, it definitely isn't. What book? **The Art Of Asking** by Amanda Palmer.

## Amanda Palmer? Who is she?

[Amanda Palmer](http://en.wikipedia.org/wiki/Amanda_Palmer) is an artist. Performance artist, musician, cabaret punker, living statue, author, blogger and above all human being. Along the way she has [done](http://en.wikipedia.org/wiki/The_Dresden_Dolls) [some](http://en.wikipedia.org/wiki/Evelyn_Evelyn) [things](http://www.ted.com/talks/amanda_palmer_the_art_of_asking) and while doing those things she's learned a lot. And in her book The Art Of Asking, she has shared a lot of the lessons she learned. On life, music, community, crowdfunding and many other things.

## But... what does this have to do with PHP?

**Nothing**. Well, also **everything**. Believe it or not, we all have lifes. We all operate within a community. Some of us work a lot with crowdfunding. Most of us love music and a lot of us need music to code. And really, our work is very creative. Not in a musical or graphical way, but we need to find creative solutions to often complex problems. In a way, we could be considered artists.

## So what about this book?

[The Art Of Asking](http://www.amazon.com/The-Art-Asking-Learned-Worrying/dp/1455581089) is a book that is not just about asking. It is about life lessons. It is about life. It is about understanding. About human relationships. About experiences, good and bad. And while in a totally different community, it is about many of the problems and challenges we also face in the PHP community today. I think we as a community can learn a lot from this book.

## What can we learn?

There's too much to put into a single blogpost, so over the coming weeks I'll be posting a series of blogposts going into more detail on the lessons we can learn from the book and why I think it also applies to the PHP community. In the meanwhile, feel free to also [follow Amanda Palmer on Twitter](http://twitter.com/amandapalmer) or [read her blog](http://blog.amandapalmer.net/).

## The articles:

[UWYC: Use What You Can](https://skoop.dev/blog/2015/01/02/use-what-you-can/)
[We Are All Artists](https://skoop.dev/blog/2015/01/06/We_Are_All_Artists/)
[Coming Out Of The Box](https://skoop.dev/blog/2015/01/23/Coming_Out_Of_The_Box/)
[The Judge](https://skoop.dev/blog/2015/06/19/the_judge/)
