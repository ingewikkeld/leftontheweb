---
layout: post
title: "Unsupported operand types string * int"
date: 2024-07-30 21:12:00
comments: true
categories: 
  - php
  - error
  - tip
social:
    summary: I ran into this confusing error today: Unsupported operand types string * int. I'm documenting my findings here for either future me, or for you.
    
---

Today I had a weirdly failing unit test. PHPUnit complained about the data provider being invalid, with a very vague error:

```
Unsupported operand types: string * int
```

This was weird, because I wasn't doing any calculations in the dataprovider. 

That made debugging annoying as well. If I'm not doing any calculations, why this error?

It turned out a typo was the cause. The weird thing though: There was no `*` in there. The typo was:

```
$errorString -= 'Missing required data key: "%s".';
```

See the `-=` there? That was the issue. It's still unclear to me why I got the error I got, but if anyone runs into this error, be aware that the error may imply more than just `*`.