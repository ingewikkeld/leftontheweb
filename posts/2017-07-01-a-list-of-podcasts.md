---
layout: post
title: "A list of podcasts"
date: 2017-07-01 11:30:00
comments: true
categories: 
  - php 
  - podcast
  - podcasts
  - learning
  - continuouslearning
 
---

I had sitting in travel not being able to do anything. Listening to music can help, but can end up also being frustrating. While I was working at Schiphol last year I got pointed to podcasts. Since then I've been really getting into listening to podcasts on my daily commute and it's been making the trip a lot more fun... and useful.

Here is a random collection of podcasts that I've been listening to in the past year and a half, on many different subjects. My main problem right now is that it's so many that I'm way behind on the episodes of most of these. Ah well, it's still quite good content.

## Software

The following podcasts are related to development or design of software. 

### [/dev/hell](http://devhell.info)

Chris "Grumpy Programmer" Hartjes and Ed "OSMI" Finkler talk about software development and related topics in this funny and interesting podcast. They're already up to episode 92 by the time of this writing. 

### [Revision Path](http://revisionpath.com)

I found this one when Liz Naramore retweeted one of their tweets. The podcast "focuses on showcasing some of the best black graphic designers, web designers and web developers", and it is extremely interesting to listen to the stories that are being told. If you're a backend developer, you'll learn a lot about the design process as well. 

### [The Agile Path](http://www.agilepath.fm)

John Le Drew started this very ambitious podcast in which he creates "audio documentaries on the issues facing organisations as they move towards more agile ways of working." The first two episodes are out already and are extremely good. John has a very special style of storytelling that makes it very pleasant to listen to.

### [Sound of Symfony](http://www.soundofsymfony.com)

If you like Symfony or you want to learn more about Symfony, this is one of the good podcasts to listen to. 4 seasoned users of Symfony discuss new developments in the Symfony community and the Symfony framework. 

### [That Podcast](https://thatpodcast.io)

Beau Simensen and Dave Marshall discuss, well, their life. Since they're both software developers, this includes but is not limited to software development. I really like the combination of tech and non-tech subjects in this podcast, and the laidback way of presenting. 

### [Voices of the ElePHPant](http://voicesoftheelephpant.com)

Of course if you're listing podcasts on software development, you can not go without listing Cal's Voices of the elePHPant. Every episode Cal interviews a member of the PHP community, and every episode you learn something. I would like to highlight the recent episode with Sara Golemon which, as you would expect with Sara, is hilarious.

## Justice, true crime, wrongful convictions etc

### [Serial](https://serialpodcast.org)

I got recommended Serial after the first season, which talks about the wrongful conviction of Adnan Syed, which was a really good and insightful season with an unfortunately relatively disappointing conclusion, mostly because of the fact that it did not give any closure on the story. The second season was also quite good, although a completely different story. 

### [Undisclosed](http://undisclosed-podcast.com)

The first season of Undisclosed left off where Serial stopped after their first season, and dug way deeper into the loose ends of the case of Adnan Syed. It may sound slightly less professionally produced at the start, but they dig a whole lot deeper than Serial. And they dig similarly deep with their new seasons. Extremely interesting to hear, and very educating for those of us not in the USA.

### [Suspect Convictions](http://www.suspectconvictions.com)

Another interesting podcast on an actual murder case that has a lot of loose ends is Suspect Convictions. Again, another podcast that seems to dig deeper than the average journalists these days dig into stories, and which teaches a lot about the USA and the US policing  and justice system.

## Other non-tech podcasts

### [Dissect](https://dissectpodcast.com)

In this podcast a very detailed analysis is done of Kendrick Lamar and specifically his album _To Pimp A Butterfly_. Every episode a new track of this instant classic album is analysed. Aside from understanding more about the music and the lyrics, we learn a lot about Kendrick Lamar and life in Compton. 

### [Inner Pod](http://innerpod.co.uk)

This one is perhaps related to tech in that is contains stories by people from the tech community, but it is about mental health. The guests on this podcast tell about their personal stories of mental health issues and how they have handled it. 

### [Jerks Talk Games](https://www.jerkstalkgames.com)

Chris and Gary talk computer games. And Chris and Gary are PHP developers. But mostly, this is about computer games. And it is a lot of fun to listen to.

## Dutch podcasts

The following podcasts are in Dutch, but I find them extremely interesting.

### [De Appels En Peren Show](http://appelsenperenshow.nl)

Wietse and Reinier talk tech. Sometimes specific new technologies, but also about the impact of technology on our society, or ethical discussions around new technology. Entertaining and informative.

### [Glitch](http://www.glitch.show)

Glitch podcast is another podcast about new tech, but from a slightly different angle than De Appels En Peren Show (even if both podcasts share one host, Reinier). Unfortunately Glitch has not published an episode in months, and I'm not sure whether this podcast is dead or alive, it is a good listen. Just listen back to the previous episodes.

### [Michiel Veenstra@Made With Opinion](http://michielveenstra.madewithopinion.com)

OK, so this is a nerdshow for those who like tech and/or radio. In the 5 episodes that are out so far Michiel interviews Domien Verschuuren and Adam Curry, but also talks about Meneer Aart, digs into privacy on the Internet and looks at audio design.

### [Wilde Haren De Podcast](https://www.youtube.com/channel/UCFjT2Yj-3CsZzrDP66XnH6w)

Vincent Patty (Jiggy Djé) and Kees van den Assem (Spacekees) talk to people. I love this concept. No format, no specific subject, no real deadline. Just a recording of people talking to eachother about topics they like and the world in general. 


