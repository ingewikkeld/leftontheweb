---
layout: post
title: "Finding a job without a recruiter"
date: 2015-04-18 13:30
comments: true
categories: 
  - php 
  - jobhunting 
  - recruiters 
  - work
---

Earlier this week we had a discussion in the [PHPNL](http://phpnl.nl/) Slack chat about finding jobs and using recruiters for this purpose. Some people had used recruiters to find their job and were stating that they might not have found their current job without the recruiter. I disagreed, and eventually promised to blog about it to share my ideas about finding a job without a recruiter.

## About recruiters

First of all, funny enough, most developers seem to agree recruiters are (with a few exceptions) horrible people. Their only focus usually is money, not making the connection between a person and a company that seem to be a good fit.

I agree with the above. There are a few exceptions, people who really do care about the people that they represent as well as the companies they represent, but I've dealt with too many recruiters that:

* alter CVs
* send 10 CVs to a company at once
* send random e-mails to everyone in their database, regardless of any matches in skills vs requirements
* actually call people at work to try and get them to change jobs

And that list can go on and on and on.

## Reasons for using a recruiter

Talking to people who have used a recruiter before, the main reason for using them was because recruiters have knowledge about jobs that they don't have themselves. In the past you could just Google for jobs and find them, but these days using Google to find a job usually results in finding tons of recruiter websites, and not the sites of the actual companies looking for people. Does this means Google has no use when looking for jobs? It is perhaps less useful, but definitely not completely useless.

Another reason for using a recruiter is that they want to look for a job "below the radar", and recruiters usually anonymize their CVs and profiles and hide the current employer. While this may sound like a valid reason for using a recruiter, I've seen CVs of candidates being offered to their current employer, or seen CVs that, even anonymised, were pretty clear in who it was.

Perhaps the most honest reason I've heard was "I'm lazy". I believe this is also the only really valid reason for using a recruiter. I can't counter this argument at all: If you're lazy, using a recruiter is most probably the easiest way of finding a new job. 

## Reasons for not using a recruiter

First and foremost: The majority of recruiters do a horrible job focussed just on money. They don't (seem to) care about the people they represent and the companies they represent, their only focus is the bonus they get out of it. They create a really toxic environment for jobseekers as well as companies with job openings. The fact that these days you can't even use Google in a simple way to look for a job without finding dozens of recruiter websites is enough proof of that. Now if they were doing a good job finding matches, that would be OK, but in a lot of situations they lie, twist, hide the truth and misrepresent developers and companies. Using recruiters to find a job keeps this toxic environment alive. This alone should be enough reason to not use recruiters.

Keep the above in mind also for yourself and your image towards potential employers. I've heard of numerous situations where after a very nice job interview that was positive from both sides, either the developer or the employer got the message "yeah, the developer just isn't interested" or "the company is not interested in hiring you", while there was a clear interest. The reason they say there is no interest is because in the negotiations for the recruiter fee, the recruiter didn't feel like they got enough money, so they lied to both parties. After a good interview, this reflects really bad on you. Based on an interview, your potential employer can get certain expectations, and when they hear you're not interested (or worse yet, when they hear you think they offer way too little salary while you don't actually think that) this reflects poorly on you. 

Many recruiters are known for their bad business practices. I once went to an interview for a company that was really excited I was interested in working for them, because my CV was the perfect match for their job! We had a really good interview, up until the point where they asked me what kind of C++ projects I had done. I was confused. I had never done any C++, and surely this was not on my CV. "Well, your CV here lists you with a couple of years of C++ experience". The recruiter had altered my CV to include experience I didn't have to make me a better match, in the hopes of it not being noticed.

Another good example is the recruiter that I accepted on my LinkedIn. A couple of days later, I got several messages from people I know asking me why I recommended them for another job. I was confused. I did no such thing. It turned out that the recruiter was just going through my LinkedIn network, calling everyone he could find a phonenumber for telling them that I had "recommended them for this position" that he was trying to fill. I was pissed, and ever since then I do not accept LinkedIn requests from recruiters anymore.

And then of course there's the recruiters that misrepresent themselves. The best example must be the guy that started a new meetup group in Amsterdam for PHP developers, and signed all his e-mails with him being the "Amsterdam PHP Community Manager". Of course it was just a coincidence that there was actually an AmsterdamPHP (note the absence of a space here) meetup group as well, a quite popular group. Even after being told that this group existed, he kept on using this title for quite some time, purposefully trying to scam his way into the lives of PHP developers.

Long story short: The chance of you being misrepresented by a recruiter is huge and this may reflect badly on your image towards fellow developers as well as potential employers.

## Looking for a job

So if you decide not to use a recruiter, how are you going to find your next cool job? Well, there's enough ways to do so.

### Google

With all the people doing "incorrect SEO" it is getting harder to find the stuff you really want to find, but that mostly means you have to use some Google-fu. Find the keywords that recruiters use and use "[keyword exclusion](https://www.youtube.com/watch?v=wUwPWLjjEfE)" to remove those from the results. I'm thinking of terms like "rockstar" or "ninja", for instance. There are companies that also use those terms, but really, would you want to work for a company that considers you a rockstar or ninja? I wouldn't. Aside from a couple of companies it is mostly the recruiters that are looking for a rockstar or a ninja, so excluding those words from your search results will help you avoid recruiters.

You can also use the recruiter-powered websites to your advantage. Recruiters are usually smart enough to remove the company names from their job openings, but with a bit of common sense, it is often easy to find out which company they are recruiter for. The location, the required skills and what they are offering are often great indicators of what company is looking. Use your common sense and Google to find out what company is looking. Also, most recruiters are pretty advanced copy-pasters, so just using Google to search for one or two sentences from the job opening can give you the website of the actual company that is looking for people.

### In-person (aka: The community)

The best way to find a good job is to actually talk to people. Go to a local usergroup, attend conferences, join mailinglists or forums, go on IRC. There's several community-oriented Slack networks these days (such as the [PHPNL](http://phpnl.nl/) that I mentioned before). All of these are great places to meet people and hear about exciting new opportunities. Remember we're currently living very much in a developers' job market, so companies looking for people will broadcast their search in many places. Interacting with the community will expose you to a lot of their opportunities and hopefully get you in touch with the best job for your current search.                                                                                                                                                    

Meeting people in person and talking to them also allows you to get much more insight into the companies that have job openings. Often you don't talk to their HR people but you first talk to their developers, meaning you can get a good idea of what the jobs are about, how they work, whether they are a good fit for what you're looking for. Developers are usually (not always) quite honest about their work and employer, which is a good thing. 

In most communities there are also a few key people that have a good overview of available jobs. I'd like to call them the Community Recruiters. They are usually thought leaders, user group organizers, conference organizers etc. They have a lot of connections and contrary to "professional" recruiters they don't want money for their services and actually care about people finding the job they really want. Some people I consider to be community recruiters are [Cal Evans](http://twitter.com/calevans), [Michelangelo van Dam](http://twitter.com/dragonbe) and [Rafael Dohms](http://twitter.com/rdohms). There are many more. Identify the Community Recruiters in your local community and let them know what kind of job you're looking for. There's a very good chance they'll be able to help you either find the job or at least get in touch with the right people that may be able to connect you with a good job.


### Job boards

Generic job boards (like Monster.com) are unfortunately taken over mostly by recruiters these days, so finding a job there is not easy if you want to avoid recruiters. Fortunately, there's some specialized job boards available these days that can help you find a job. For instance, if you like working with Symfony framework, there is the [Symfony jobs section](http://symfony.com/jobs), for Laravel there is [Larajobs](https://larajobs.com/). There may be some recruiters there, but most of the ads on those job boards are placed by the actual companies looking for developers, so you will have direct contact with the companies instead of through recruiters. 

Stack Overflow (the solution to most programming problems these days) also has a [careers website](http://careers.stackoverflow.com/). It is a mixture of recruiters and companies from what I can gather, but there's a lot of companies that put their job openings there directly so you can get in touch with them, research them etc.

### Try the obvious choices directly

Think of your work. Think of what you do. Which technologies you use. Which products. Which frameworks. Which hosting. Which events do you go to? Then identify which companies are related to that. These are the obvious companies in terms of work you might like to do. Then check their websites for job openings. [Try](http://www.zend.com/en/company/careers#opening) [the](http://sensiolabs.com/en/join_us/why_join_us.html) [most](http://www.magmadigital.co.uk/contact/career-opportunities/) [obvious](https://enrise.com/jobs/) [choices](http://werkenbijmijndomein.nl/) [first](https://www.facebook.com/careers/). There's a good chance you'll find at least a couple of potentially interesting jobs when doing this. Perhaps you don't need to look beyond this.

### Twitter

Some years ago when I was looking for a job, I tweeted about it. Within a week, I had 10 (ten!) interviews lined up. Twitter is an amazing place to look for a job, because it is open and extremely interactive.

However, because Twitter is so open, there is no way to do a covert search for a new job without your employer finding out. However, if you follow the right people, the jobs will be delivered to you. The community recruiters I mentioned earlier often retweet cool jobs, and there are many more community leaders in the PHP world that (re)tweet jobs as well. Follow those people and you'll be sure to get the jobs delivered right to your (twitter) doorstep. 

## Decide what you are looking for first

Most importantly though, whether you are using a recruiter or not, is to decide what it is you are looking for. This will make your search a whole lot easier. Determine what makes you happy in terms of technology, projects, customers or product development, salary, etc. This makes it a lot easier to company possible jobs to your requirements and will help you find the right job. Sometimes, when making the requirements you can already conclude that because of those, you should or should not use a recruiter.
