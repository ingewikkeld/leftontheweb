---
layout: post
title: "Music recommendation: Brent Mottley"
date: 2005-05-16 6:40
comments: true
categories: 
  - leftontheweb 
---
Some of you may know that I'm a big fan of laidback electronica. Nice stuff to relax to. In that vein, I can recommend <a href="http://www.archive.org/audio/audio-details-db.php?collection=hippocamp&collectionid=hc126&from=landingReviews">Brent Mottley's 'treeline'</a>. Great laidback music to chill to. Unfortunately the people at <a href="http://www.hippocamp.net/">Hippocamp</a> seem to have some server trouble at the moment, but linking to archive.org directly works fine :)

<a href="http://www.archive.org/audio/audio-details-db.php?collection=hippocamp&collectionid=hc126&from=landingReviews">Listen here</a>
