---
layout: post
title: "Using custom namespaces with (C/S)ilex and Composer"
date: 2012-04-11 15:10
comments: true
categories: 
  - silex 
  - cilex 
  - autoloading 
  - PSR-0 
  - composer 
---
<p>Aside from installing dependencies and creating an autoloader for those dependencies, Composer can also be the autoloader for your own custom code. Obviously, it's a matter of <a href="http://getcomposer.org/doc/01-basic-usage.md#autoloading">reading the F***** documentation</a>, and as soon as Jordi told me Composer could actually do this, I quickly had stuff set up. All I needed to do was add an autoload entry to my composer.json file:</p>

<pre>
"autoload": &#123;
    "psr-0": &#123;"Foo": "lib"&#125;
&#125;
</pre>

<p>After that, you need to run composer.phar install to generate the new autoloader, and I'm all set. I can now load all libraries as long as they adhere to the PSR-0 naming standard (because I specified psr-0). If you don't use PSR-0, you could actually also use a classmap, as <a href="http://getcomposer.org/doc/04-schema.md#autoload">discussed in the documentation</a>.</p>
