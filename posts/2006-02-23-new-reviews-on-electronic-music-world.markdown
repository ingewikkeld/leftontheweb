---
layout: post
title: "New reviews on Electronic Music World"
date: 2006-02-23 16:28
comments: true
categories: 
  - meta 
---
Finally there are some new reviews on Electronic Music World! It's been a while, but I plan on getting new stuff on there more regularly again.

<strong>MonoCulture - The Animal Economy</strong>
A beautiful album by MonoCulture, this is one to keep listening to.
<a href="http://www.electronicmusicworld.com/review.php?id=157" />read >></a>

<strong>Courtezan - Untitled</strong>
A two-track release from Courtezan, pleasant to the ears.
<a href="http://www.electronicmusicworld.com/review.php?id=158" />read >></a>

<strong>Greg Scrase - Untitled</strong>
Two tracks from Greg Scrase bring a lot of variation.
<a href="http://www.electronicmusicworld.com/review.php?id=159" />read >></a>
