---
layout: post
title: "Ibuildings partners with PHP|Architect"
date: 2008-05-23 19:09
comments: true
categories: 
  - ibuildings 
  - zend 
  - php|architect 
  - php
---
<p>Ibuildings is a very professional company, with an enormous amount of high quality developers with the right mindset. I can not think of another company in The Netherlands (and probably also the UK) that has such a wonderful team of people. Great, skilled developers, consultants, project managers. Our partnership with Zend supports the claim that Ibuildings are <em>the</em> PHP Professionals.</p><p>For years, I&#39;ve been following PHP|architect. Their magazine, their books and lately also their trainings are quite impressive. And it&#39;s really a perfect match for Ibuildings, so it didn&#39;t really come as a surprise when I heard that Ibuildings was partnering with PHP|Architect. It really is supposed to be. It strengthens the portfolio of Ibuildings and confirms the status of being <em>the</em> PHP Professionals. And for PHP|Architect it gives a very good, strong presence in Europe.</p><p>Of course, looking at it from outside of Ibuildings, for instance in my role as secretary of the <a href="http://www.phpgg.nl/" target="_blank">Dutch PHP Usergroup</a>, I also look at this very positively. All PHP|Architect products are very high-quality, supporting the developer in gaining more knowledge of PHP and best practices. Having those products gaining a stronger presence in our region is a very positive thing. Excellent. </p>
