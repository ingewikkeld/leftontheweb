---
layout: post
title: "The first BugHuntDay is coming!"
date: 2008-10-15 19:53
comments: true
categories: 
  - bughuntday 
  - phpgg 
  - phpbelgium 
  - php 
  - zendframework 
---
<p>Where the PHP TestFest focussed on writing tests (in that case for PHP itself), the <a href="http://www.bughuntday.org/" target="_blank">BugHuntDay</a>  will focus on one specific project. More specifically, it is focussing on open bugs for the project, and fixing those bugs. By fixing those bugs, the project will be more stable and that will in turn help both the community and the users.</p><p>The first project that will enter the BugHuntDay fray is <a href="http://framework.zend.com/" target="_blank">Zend Framework</a>. This popular PHP5 framework is one that everybody will know. Going through the <a href="http://framework.zend.com/issues/" target="_blank">JIRA installation of Zend Framework</a>  you&#39;ll notice that there&#39;s quite a few bugs in there. We plan to reduce the amount in there!</p><p>So, if you&#39;re in The Netherlands or Belgium, then <a href="http://upcoming.yahoo.com/event/1230795/" target="_blank">come on over to Roosendaal</a>  to help us with the bughunting on november 8th!</p><p>Oh, and for those who fear I may have turned in to the darkside... the next project that will be part of the BugHuntDay is <a href="http://www.symfony-project.org/" target="_blank">symfony</a>  ;) </p>
