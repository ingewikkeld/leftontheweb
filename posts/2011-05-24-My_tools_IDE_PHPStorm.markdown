---
layout: post
title: "My tools: IDE: PHPStorm"
date: 2011-05-24 10:26
comments: true
categories: 
  - php 
  - tools 
  - phpstorm 
---
<p>The first piece of software I want to describe is my IDE. The IDE is without a doubt the heart of my workday (even though my <a href="http://social.wakoopa.com/skoop">Wakoopa profile</a> seems to disagree), as I spend a lot of my working time writing code. In the past, I've used many different IDEs. I started with <a href="http://eclipse.org/">Eclipse</a>, moved to <a href="http://zend.com/studio">Zend Studio</a> 5.0 and 5.5 and then on to 6.0 as well. After the move to the Eclipse platform with Zend Studio 6.0, my annoyances with Zend Studio started to build up and it started to block me in my work (Building workspaces anyone?) so I started looking around at other options. I moved to <a href="http://netbeans.org/">Netbeans</a> shortly after but then was pointed to <a href="http://www.jetbrains.com/phpstorm/">PHPStorm</a> by <a href="http://www.naenius.com/">Mike van Riel</a> and stuck with that. I do realize that many of my earlier annoyances with Zend Studio have been fixed in recent versions but at the moment, I'm really happy with PHPStorm so I have no need to move.</p>

<p>So what makes PHPStorm good? I guess this will differ per developer, but to me it's the great performance, very good code hinting and excellent code completion. PHPStorm also has excellent integration with other tools, but to be honest, most of the times I don't use such integration, opting for manual use of those tools.</p>

<p>The only downside I can find for anyone is the price. PHPStorm is a commercial package (84 euro for a personal license) and with some very good free IDEs available as well I can understand that not everyone will want to switch to PHPStorm. But to be honest, I've happily paid the license fee after using the trial and would pay it again if I didn't have it yet. So I'd want to recommend everyone who uses PHP on a regular basis to try the PHPStorm trial and see if it fits your needs.</a>
