---
layout: post
title: "Two new jobs in one week!"
date: 2009-08-06 18:53
comments: true
categories: 
  - php 
  - symfony 
  - community 
---
The position of Community Manager is a relatively new position inside the symfony project. It was launched last year at the SymfonyCamp at a discussion on how the community can be more active and how these contributions could be managed better. I posed the idea of a community manager, and Fabien liked the idea. Kris Wallsmith stepped forward and took the lead in filling the position. In the past (nearly a) year, he has done a very nice job, launched a few ideas and kept in touch with people.<br /><br />Kris is <a href="http://www.symfony-project.org/blog/2009/08/06/about-symfony-1-3-and-symfony-1-4" target="_blank">taking on the position of Release Manager</a>  for both symfony 1.3 and symfony 1.4 though, and as such was looking to pass his Community Manager position on to someone else. I am quite honored that I was approached for the position, and I think I&#39;ll have a blast doing this. As most people are aware of by now, I am quite a fan of symfony, and also someone who likes to be involved in communities. So now, I can combine those two things in this new position!<br /><br />As to what you can come to expect, this is all still a bit vague. I&#39;ve had some ideas for a while already which time constraints did not allow me to work out in detail. But one thing is certain already: If you have a symfony event or community effort that you want to launch, please do <a href="http://www.symfony-project.org/blog/2009/08/06/stefan-koopmanschap-is-your-new-symfony-community-manager" target="_blank">get in touch</a>  with me and I&#39;ll see if I can be of assistance in any way!
