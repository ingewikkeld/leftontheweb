---
layout: post
title: "New Ufdi Network member: Osxy[dot]nl"
date: 2006-07-22 14:35
comments: true
categories: 
  - technology 
---
I've been following this weblog for quite a while after <a href="http://www.filipdewaard.com/">another member of the network</a> recommended the weblog to me. Yes, I know, it's yet another mac user ;) but I feel Andre knows what he's talking about and I think his weblog will add something to the network.

So please join me in welcoming <a href="http://www.osxy.nl/">Osxy[dot]nl</a> to the <a href="http://www.ufdi.net/">Ufdi Network</a>. 
