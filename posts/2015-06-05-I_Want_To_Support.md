---
layout: post
title: "I Want To Support"
date: 2015-06-05 12:00
comments: true
categories: 
  - php
  - ask 
  - donate
  - opensource
---
I have been blogging in the past months about [asking](https://skoop.dev/blog/categories/theartofasking/), as I think it is quite important to realize asking is not wrong at all. When Joshua posted his [slightly controversial post about elePHPants](https://www.adayinthelifeof.nl/2015/06/03/the-php-elephant-stampede/) I felt it made a good point: We should be donating more to open source projects.

It inspired me to create a simple new site listing donation options in the Open Source world, but as I was trying to find content for it, I found out that many open source developers and projects don't list donation options on their website. I think that is a terrible shame. I know the idea of Open Source is that you give it to the community, and that people can thank you by contributing, but sometimes when you use Open Source you just want to buy the developer a beverage of their choice or help them unwind by letting them afford a new game.

## Introducing I Want To Support

As I mentioned earlier, Joshua's article inspired me to create a site that lists donation options. So I sat down earlier this week and started playing around with [Sculpin](https://sculpin.io/), one of the projects that I'd heard of but hadn't actually tried myself. It took me about an hour, or perhaps an hour and a half, and I had the basic idea of the site set up.

What the site basically does is list projects that accept donations. The homepage lists the projects, and you can click on a project to get more information on donation options.

So when you have some spare money that you want to donate, you go to the site, pick a project to your liking, and donate. It is that simple. That is the idea behind [I Want To Support](http://iwantto.support/).

The content comes from an [open Github repository](https://github.com/Ingewikkeld/iwantto.support). Adding a new project is as simple as creating a Markdown file that adheres to the [template](https://github.com/Ingewikkeld/iwantto.support/blob/master/README). Everyone can create pull requests to add projects they know. So if you know a project that accepts donations, please do add it to the list.

## Money?

Of course, donating to Open Source projects does not mean you can not contribute in other ways anymore. Please do keep sending pull requests, writing documentation or blogposts or helping your favorite project in the ways you're already doing.