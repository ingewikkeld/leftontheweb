---
layout: post
title: "The Beastles"
date: 2004-12-21 8:13
comments: true
categories: 
  - leftontheweb 
---
Oh! Fun music! Mash-ups are mostly crap, but sometimes there are really interesting mash-ups. <a href="http://www.djbc.net/mashes/beastles.html">The Beastles</a> is a good example of really fun mash-ups. A guy called DJ BC mashed up The Beatles with The Beastie Boys. And they're really good!
