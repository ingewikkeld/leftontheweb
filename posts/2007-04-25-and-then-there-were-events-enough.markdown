---
layout: post
title: "And then there were enough events"
date: 2007-04-25 12:25
comments: true
categories: 
  - technology 
---
Ever since the International PHP Conference moved away from The Netherlands, it's been quiet here for PHP-minded people. This year though, we're having a full schedule here in The Netherlands. I love it! Finally we're having lots of events on PHP. A short list.

On April 14, we had <a href="http://www.pfcongrez.nl/">pfCongrez</a>, a gathering of quite a few dutch PHP developers with nice sessions on MySQL.
Tomorrow, April 26, iBuildings is organizing a <a href="http://www.phpseminar.nl/">business seminar</a>. Though focussed on business, it's still a very nice things. This seminar might mean more penetration of PHP in the big business world
On June 2nd, we at Dutch Open Projects will be organizing a PHP Seminar around the topic of PHP Frameworks. More info on this will follow soon enough :)
On June 16th, iBuildings will be organizing a replacement for the International PHP Conference with their Dutch PHP Conference.
And finally somewhere this year Dutch Open Projects will also organize <a href="http://www.symfonycamp.eu/">SymfonyCamp</a>. More on this also later.

Awesome! Finally The Netherlands can attempt to have a more tight PHP community.
