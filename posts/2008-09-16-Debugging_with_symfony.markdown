---
layout: post
title: "Debugging with symfony"
date: 2008-09-16 8:25
comments: true
categories: 
  - presentation 
  - slides 
  - php 
  - symfony 
  - symfonycamp 
---
<div id="__ss_596302" style="width: 425px; text-align: left"><a style="margin: 12px 0pt 3px; font-family: Helvetica,Arial,Sans-serif; font-style: normal; font-variant: normal; font-weight: normal; font-size: 14px; line-height: normal; font-size-adjust: none; font-stretch: normal; display: block; text-decoration: underline" href="http://www.slideshare.net/skoop/debugging-with-symfony-presentation?type=powerpoint" title="Debugging With Symfony">Debugging With Symfony</a><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="425" height="355"><param name="movie" value="http://static.slideshare.net/swf/ssplayer2.swf?doc=debuggingwithsymfonynonotes-1221293691203170-8&amp;stripped_title=debugging-with-symfony-presentation" /><param name="quality" value="high" /><param name="menu" value="false" /><param name="wmode" value="" /><embed src="http://static.slideshare.net/swf/ssplayer2.swf?doc=debuggingwithsymfonynonotes-1221293691203170-8&amp;stripped_title=debugging-with-symfony-presentation" wmode="" quality="high" menu="false" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="425" height="355"></embed></object><div style="font-size: 11px; font-family: tahoma,arial; height: 26px; padding-top: 2px">View SlideShare <a style="text-decoration: underline" href="http://www.slideshare.net/skoop/debugging-with-symfony-presentation?type=powerpoint" title="View Debugging With Symfony on SlideShare">presentation</a> or <a style="text-decoration: underline" href="http://www.slideshare.net/upload?type=powerpoint">Upload</a> your own. (tags: <a style="text-decoration: underline" href="http://slideshare.net/tag/php">php</a> <a style="text-decoration: underline" href="http://slideshare.net/tag/symfonycamp2008">symfonycamp2008</a>)</div></div> <p>My presentation went OK, even though I had the idea I perhaps spoke a bit too fast. This idea was confirmed by the fact that I finished too early. Not that that was such a big problem, because we were running a bit late with all sessions ;)</p><p>SymfonyCamp itself was a great event again this year, kudos to DOP and especially Lambert and Peter for organizing it. Thanks to everyone who attended, presented and simply was there, I had a great time! </p>
