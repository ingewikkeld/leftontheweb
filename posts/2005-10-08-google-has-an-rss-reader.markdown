---
layout: post
title: "Google has an RSS reader"
date: 2005-10-08 13:38
comments: true
categories: 
  - weblogging 
---
I just heard about the new <a href="http://www.google.com/reader/lens/">Google RSS Reader</a>. Being a big fan of Google's software, I decided to check it out.

My first impression is unexpectedly a bit negative. Though everything looks mighty fancy with all the AJAX work, some things are not as userfriendly as they could be. The default post list is not categorized but just a huge list of weblogs. You can view them by category on the 'Your Subscriptions' page, but if you're working with categories in the first place, what's the use of not immediately showing them?

Another downside is the fact that when you're in your 'reading list' and you click on a weblog, it jumps to a seperate page with a list of posts for that weblog. Now that would be fine, except that when I 'return to my reading list', it starts at the top of the list again. With over 100 weblogs in my list, that is quite annoying, especially since you can only jump at intervals of 4 weblogs at a time.

The OPML import worked fine, it immediately showed me my nice list of subscriptions I had on <a href="http://www.bloglines.com/">BlogLines</a>. 

All in all: it's a nice effort but not fully developed if you ask me. Google, usually the master of great user interfaces, has lacked in it's attention to a useful interface if you ask me. There's definately some things missing that they should have a good look at. For now, I'll just continue to use Bloglines, as I'm still a happy Bloglines user.
