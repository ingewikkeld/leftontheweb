---
layout: post
title: "Backwards compatibility"
date: 2008-02-08 19:47
comments: true
categories: 
  - meta 
  - backwardscompatibility 
---
In the <a href="http://www.ibuildings.nl/blog/archives/541-Backward-compatibility,-bane-of-the-developer.html" target="_blank">article</a>, which I recommend to every developer and especially those involved in Open Source projects, he covers the ground of why backwards compatibility can be important, why it is caused, and how to deal with it. He handles several methods of how to deal with backwards compatibility, listing pro&#39;s and cons of that approach. An excellent read that makes you consider the code you&#39;ve written yourself, and makes you think a bit harder about code you&#39;re about to write.
