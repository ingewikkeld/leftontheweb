---
layout: post
title: "Zend Framework revisited"
date: 2006-04-05 15:48
comments: true
categories: 
  - technology 
---
I have to rectify my <a href="https://skoop.dev/article/206/first-zend-framework-observation">earlier words about the problems with the Zend Framework</a> loadClass() method. Since posting this message I for a while did not have time to get a better look at this. Yesterday I posted my question about the Zend::loadClass() method to the <a href="http://www.zend.com/lists/fw-general/200604/msg00044.html">Zend Framework mailinglist</a> and pretty quickly I had a very useful response. On the train home, I decided to dive into the solution that was posted. And indeed. I had misunderstood the intention.

I was calling the method like this:

Zend::loadClass('Filter');
or
Zend::loadClass('Filter', 'Zend');
or even
Zend::loadClass('Zend_Filter', 'Zend');
and also
Zend::loadClass('Filter', array('Zend'));

This last one after I finally found out the second parameter is meant to be an array. After I found out about that, I tried all combinations with the second parameter being an array but couldn't get it to work. But I was making it harder than I needed to, because it will work as simple as this:

Zend::loadClass('Zend_Filter');

This will work as long as the library directory (the directory where Zend.php is located) is in your include_path.

A classic case of RTFM, because I did read the documentation:

