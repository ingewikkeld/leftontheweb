---
layout: post
title: "Joomla! Days ahead"
date: 2006-12-06 11:52
comments: true
categories: 
  - technology 
---
Friday and Saturday the dutch <a href="http://www.joomladag.nl/">Joomla!Days</a> are happening in Den Bosch. As a company that does a lot with and owes a lot to Joomla! my <a href="http://www.dop.nu/">employer</a> is one of the main sponsors of this event, and most of the employees will be attending the conference. We will even have a stand there!

There are two days, friday is the "business day" and saturday is the "community day". I am looking forward to both days, with interesting sessions on both. What I am especially looking forward to:
<br />
<li>Leslie Hawthorn (Google) on how Google uses and stimulates Open Source</li>
<li>Lead developer Johan Janssens on the Joomla! (1.5) Framework</li>
</ul>

I might also drop in on the community day sessions on Web Services and the migration from Joomla! 1.0 to 1.5, since our company has a lot of Joomla! 1.0 websites currently running with custom components.

I'm not sure yet if there will be wifi, but if so, I might do some live logging on how the day is and what the sessions are like :)

If you're also going, do come and say hi to me. I'll probably be hanging out at or around the Dutch Open Projects stand during the breaks, or if not, then someone there will be able to point you to me :)

