---
layout: post
title: "Pivot to Textpattern converter, all but finished"
date: 2005-11-06 7:14
comments: true
categories: 
  - technology 
---
As long-time readers know, this weblog used to be powered by <a href="http://www.pivotlog.net/">Pivot</a>. Though Pivot worked fine, <a href="https://skoop.dev/article/134/hey-whats-this">I had my reasons to switch</a>. Aside from those reasons, I'm always curious to work with new software. Anyway, to not lose all the content, I wrote a simple converter to keep my old posts.

I got some requests to share this converter, and so I have worked a bit on documenting it. That work is now finished and you can <a href="https://skoop.dev/pivot_to_textpattern.phps">view the result here</a>. It's far from perfect, and far from finished, but it works. I only lost a single post in my conversion process, which was not a valuable post to me. I blame the extensive use of HTML in that post for the problem (so people who use a lot of HTML in their posts may want to fix that first).

Anyway, since my conversion already worked, I'll leave further development to the Textpattern community. But please do share your changes with me, I like to stay informed on what people do with my script :)
