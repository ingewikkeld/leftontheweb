---
layout: post
title: "Building websites with Plone"
date: 2005-03-03 3:03
comments: true
categories: 
  - leftontheweb 
---
Well, I finally did it. I ordered <a href="http://www.packtpub.com/book/plone">this book</a>. Plone really looks interesting, and I should finally widen my focus. <a href="http://www.php.net/" rel="tag">PHP</a> is great, of course, but having more choice than just PHP is even better.
