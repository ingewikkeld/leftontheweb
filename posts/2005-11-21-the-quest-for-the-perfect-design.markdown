---
layout: post
title: "The quest for the perfect design"
date: 2005-11-21 15:45
comments: true
categories: 
  - weblogging 
---
I'm still not completely happy with my current design. It's still too much like the original template. Though the original template is quite nice, I like something with a more personal touch. However, creating a whole new design... well, let's just say I'm not a designer. My wife is, but she's busy enough as is.

Does anyone have any good tips on template sites that contain good, high quality templates that are lighter than this current one (less images is good, I want something that is as much as possible css-oriented). Or is there anyone willing to donate some time to creating something like that for me? I'd be very grateful :)
