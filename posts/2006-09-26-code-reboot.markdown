---
layout: post
title: "Code Reboot?"
date: 2006-09-26 14:12
comments: true
categories: 
  - technology 
---
Soon, the new website for <a href="http://www.cssreboot.com/">CSS Reboot</a> will be launched, or so it looks from the current state of their website. There is a message about their site currently being rebooted. The main page works, and the rest fails at the moment.

So I was thinking: If you have the CSS Reboot to refresh the design of your website... would it not be fun if there would also be a code reboot day: A single day where all participants would add new functionality to their website. Call it: D-Day (Deployment Day) or some such. For Open Source projects, it would be the ultimate day to release a new version, and for websites, it would be the ultimate day to deploy their new functionality.

Would be fun. Anyone going to set it up? :)
