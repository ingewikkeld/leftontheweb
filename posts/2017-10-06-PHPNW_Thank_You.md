---
layout: post
title: "PHPNW: Thank You"
date: 2017-10-06 09:30:00
comments: true
categories: 
  - php 
  - phpnw17
  - conference
  - thankyou
social:
    highlight_image: /_posts/images/phpnw17.jpg
    highlight_image_credits:
        name: Stefan Koopmanschap
    summary: PHP NorthWest 2017 was the last PHPNW (for now). The conference was important, so it is time to say thank you.
    
---

The past ten years, the [PHP NorthWest conference](http://conference.phpnw.org.uk/) in Manchester has had a huge impact on the Manchester PHP scene, but also on the rest of Europe (and perhaps the world). Last weekend during the closing of the conference, Jeremy Coates announced that PHPNW conference is going on a hiatus. They're not saying they're quitting, but for now, there will be no more PHPNW conference. A sad moment for sure, but I'm proud of all those involved in organizing PHPNW for 10 years.

## Inspiration as a developer

PHPNW has ensured a constant inspiration for me as a developer. They've always had a nice and varried schedule both expanding on existing topics and bringing new topics that are interesting to developers. I've been incredibly inspired by many talks at PHPNW, for instance the keynote by Lorna Mitchell and Ivo Jansch and the keynote by Meri Williams. 

## Inspiration as an organizer

A little and perhaps unknown fact: When the Dutch PHP Usergroup merged with PHPBelgium to form PHPBenelux and we started considering organizing a conference, we contacted Jeremy and Priscilla about this. They were kind enough to give us a boatload of information about organizing a conference, ranging from how to pick the right schedule to how to try and get sponsors. Their help was invaluable in this early process of organizing a conference. Also, how PHPNW was set up, the atmosphere it had, was a huge inspiration for the early PHPBenelux Conference. 

## Friends, so many friends

I have met up with so many old friends and made so many new friends at PHPNW Conference. I couldn't list them all even if I wanted to, but for instance Lorna, Jenny, Jeremy, Priscilla, Mark, Matt, Kat, Mike and Rick. I've had countless conversations and discussions with people I know, people I did not know yet or people who were close friends already. PHPNW was also the conference where I finally met Khayrattee, who came over from Mauritius. That is one memory I will never forget.

## A big thank you

So this is a big thank you to everyone involved in organizing PHPNW Conference those ten amazing years. You have given me and a lot of other people a lot of fun, opportunities and many lessons learned. You are an inspiration to me and I'm sure to countless others. I hope to see you at some point, somewhere in the future. Thank you.