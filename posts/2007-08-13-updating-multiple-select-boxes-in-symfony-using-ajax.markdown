---
layout: post
title: "Updating multiple select boxes in Symfony using AJAX"
date: 2007-08-13 13:39
comments: true
categories: 
  - symfony 
---
For a client, I had to present a bunch of select boxes where users could select a genre, then a sub-genre based on your genre of choice, then a second level of sub-genres based on the selected sub-genre. My first choice was to use the symfony AJAX options, and updating the sub-genre select box with the observe_field function. This worked, the sub-genre got updated as I selected the main genre. However, for some reason, when selecting a sub-genre, the list with the second level of sub-genres was not updated. Weirdness!

And then, in some bright moment, I decided to try and only update the options inside the sub-genre selectbox. And behold, it now worked.

So, I am using observe_field to observe the genre selectbox, and another observe_field to observe the sub-genre selectbox. As a genre is selected, the options of the sub-genre are being reloaded. As a sub-genre is selected, the 2nd level sub-genres are being reloaded. It's a little trick, and I'm still not sure why my initial approach fails (as I took care that the replacing selectbox had the same name/id as the original one with no options). However, it works and it's a clean solution I guess.
