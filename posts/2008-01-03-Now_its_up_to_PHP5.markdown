---
layout: post
title: "Now it's up to PHP5"
date: 2008-01-03 7:31
comments: true
categories: 
  - technology 
  - php 
---
<p>And this is good news. I&#39;ve quite regularly on this weblog called for a better adoption for PHP5. PHP5 is in just about every aspect better than PHP4. Unfortunately adoption has not been very good in the past. It was a bit of a chicken/egg situation, where hosting providers didn&#39;t want the hassle of upgrading because so many of the popular PHP software was still in PHP4, and on the other end a lot of the popular PHP software didn&#39;t want to make the jump because so many hosting providers were only offering PHP4. </p><p>Well, guess what? Now is the time to make the jump. With more and more hosting providers offering some kind of support for PHP5, it&#39;s now possible to make the jump. Do it! Go for it! Any serious developer won&#39;t regret it.&nbsp;</p>
