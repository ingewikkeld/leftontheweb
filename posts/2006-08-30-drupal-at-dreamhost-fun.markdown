---
layout: post
title: "Drupal at DreamHost: Fun"
date: 2006-08-30 15:18
comments: true
categories: 
  - technology 
---
<a href="http://www.drupal.org/">Drupal</a> at <a href="http://www.dreamhost.com/r.cgi?116006">DreamHost</a> is something fun, or so I've experienced tonight. I had prepared a full site configuration including modules, tweaked configuration and even content. I did this all on my local machine. Usually, when I do these things, it's a matter of copying over the files and the database, change the database credentials in the settings file, and it's working.

Not this time. After doing the above, I got all kinds of weird URL's that were starting with system-cgi/ in the path. Not useful. After reading about many successful Drupal installations on Dreamhost, though, I thought I'd try a fresh install: TADA! That solved the whole problem! It's a hassle, because I had to go through all the configurations again, compare them with my local version, etc etc etc, but at least it works. I'm happy :)

So, now <a href="http://www.onlinemusicworld.com/">Online Music World</a> is running Drupal, adding some features to the previously available discussion forum!
