---
layout: post
title: "TomTom HOME!"
date: 2006-05-24 14:28
comments: true
categories: 
  - technology 
---
I've been a bit busy lately, so I nearly forgot to announce that finally, <a href="http://www.tomtom.com/plus/service.php?ID=17&Language=1">TomTom HOME</a> is now available! The software that, together with my colleagues, I've been working on for the past months (I've done the server component with some of my colleagues from the PLUS department) is now available as a free download. Check it out. Enjoy :)
