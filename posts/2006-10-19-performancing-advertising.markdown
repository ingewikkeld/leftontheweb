---
layout: post
title: "Performancing advertising"
date: 2006-10-19 14:03
comments: true
categories: 
  - weblogging 
---
Performancing is "Helping bloggers succeed..." so they say. After their statistics tool, their Firefox blogging tool, and their 'Exchange' market place for paid bloggers. 

This week, I read <a href="http://seekingrevenue.com/161/performancing-introduces-new-revenue-source/">A post on Seeking Revenue</a> which was the first time I heard about the latest Performancing offering: An advertising network. I signed up immediately and as you might have noticed have added the code of the network to my sidebar. Right now, it's not possible yet to buy advertising as Performancing is still in "warming up" mode, but soon, people will be able to advertise on Left on the Web. Now ain't that cool? ;)
