---
layout: post
title: "Center of Expertise"
date: 2008-09-11 20:58
comments: true
categories: 
  - ibuildings 
  - php 
  - expertise 
  - opensource 
---
<p>Purpose of the Center of Expertise will be, and I will quote the announcement, amongst others:</p><ul><li>Contributing to/supporting open source projects<br /> </li><li>Supporting user groups/communities<br /> </li><li>Organizing conferences/seminars<br /> </li><li>Developing training material<br /> </li><li>Forming and maintaining partnerships<br /> </li><li>Developing professional services</li></ul><p>This is a step not a lot of commercial organizations dare to make, and I am very proud to be working for one of the companies that has the balls to do it. Sure, there&#39;s commercial stuff in there, but there&#39;s also big investments in the open source community in there.</p><p>Once again I am proud to work for Ibuildings, and am very happy that I made the choice to go for Ibuildings last year. </p><p>Now, last but not least, if you want to work for such a cool company as well, there are still openings for (senior) developers at Ibuildings. Please feel free to <a href="/who" target="_blank">contact me</a>  for more info or to pass on your information to HR. You&#39;ll have to put up with working for the same company as me ;) but you will also be working with lots of other cool people, like <a href="http://www.jansch.nl/" target="_blank">Ivo</a>, <a href="http://www.lornajane.net/" target="_blank">Lorna</a>, <a href="http://valokuva.org/" target="_blank">Mikko</a>, <a href="http://www.helgi.ws/" target="_blank">Helgi</a>, and <a href="http://www.ibuildings.com/ibuildings/theprofessionals" target="_blank">lots of others</a>. You&#39;ll be working in a company that is a <a href="http://www.zend.com/" target="_blank">Zend</a>  and <a href="http://www.phparch.com/" target="_blank">PHP|architect</a>  partner, that delivers high-quality software and services, and is already a big player on the PHP market with the potential to become much, much bigger. So contact me if you&#39;re interested! </p><p>&nbsp;</p>
