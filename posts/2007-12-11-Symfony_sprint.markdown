---
layout: post
title: "Symfony sprint"
date: 2007-12-11 7:37
comments: true
categories: 
  - symfony 
---
<p>When I first read the information on the sprint, I was a bit disappointed that it was happening on a weekday. I wanted to be part of the sprint, but it&#39;s workday... but then I thought: what the heck, I&#39;ll just e-mail my boss to see if I can simply spend my workday on this!</p><p>And thanks to the fact that the people at <a href="http://www.ibuildings.com/" target="_blank">Ibuildings.nl</a>   are simply very cool people, I will be able to join the symfony team and enthousiasts on december 20 to work on symfony. What I&#39;ll be doing I don&#39;t know, we&#39;ll see what comes up. But it&#39;s great to be able to spend a whole day on contributing to symfony! </p>
