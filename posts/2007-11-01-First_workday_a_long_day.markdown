---
layout: post
title: "First workday, a long day"
date: 2007-11-01 13:44
comments: true
categories: 
  - personal 
---
<p>I only got back home at about 8PM, but that was because I had a car on my way back and was somehow able to avoid all traffic jams. I must&#39;ve had some kind of lucky trip today. Let&#39;s hope this continues for the coming two weeks or so that I will actually be working in Vlissingen before starting to work from home.</p><p>My first impression is that Ibuildings is a pretty cool company to work for. Aside from a fantastic team of developers, they have some good ideas and are well-organized. Even though in my previous job the lack of organization was one of the challenges I really liked, it&#39;s pretty nice to again work for a company where things are already arranged in a good manner.</p><p>Today, I&#39;ve already been playing a bit (or mostly reading myself into) the <a href="http://www.achievo.org/atk/" target="_blank">ATK framework</a>  that Ibuildings has been developing. At least until Ibuildings is able to get some <a href="http://www.symfony-project.com/" target="_blank">symfony</a>  work in, I&#39;ll be assigned to a project where they use ATK and so I&#39;ll need to know it. This may result in some nice posts comparing ATK with symfony, which I may write for this weblog or for the <a href="http://www.ibuildings.nl/blog/" target="_blank">Ibuildings weblog</a>  (or both of course). We&#39;ll see.</p><p>For now, I think I&#39;ve made the right decision to make this move. Time will tell if I still feel the same way in a few weeks or months ;) </p>
