---
layout: post
title: "Be Yourself, Stay Yourself"
date: 2014-05-08 16:00
comments: true
categories: 
  - php 
  - private 
  - personal 
  - identity
---
Today I spoke a short bit at the funeral of my grandmother. I told my family about an important lesson I learned from my grandmother. I'd also like to share this lesson with you, as I think it is an important lesson for everyone.

When I was a teenager, I suffered a lot from acne. This became a huge issue for me. My grandmother kept telling me one thing: Ignore other people making fun of you. Just be yourself and things will get better. Now, many people told me similar things, but the message was much stronger from my grandmother: She had a skin disease in her face that everyone could easily see, so clearly she knew about this.

The thing is: I did stay myself. I tried to ignore other people making fun as much as possible and I tried to stay myself as much as possible. Whether or not I completely succeeded I will never be able to say, but one thing I know: Until this day, I am myself. I don't listen to people telling me to change. I only try to change when I feel this is a good thing to do. So no, I don't walk around in a suit while running my business. If you hire me, you hire me for one reason only: My knowledge and experience with software projects, with working with the community, etc. I say no to clients expecting me to dress smart or wear a suit.

I've been accused of being unprofessional by behaving this way. I feel I just am me. I am Stefan Koopmanschap, and this is me.

And thus I am here to say to you: Keep being you. This is why your friends like you. This is who you are. And of course you can change. You can change who you are, but only do so when it feels good to you. If you're being asked to wear a suit and it feels good to you, there is no problem. If you are asked to wear a suit and you hate it: DON'T. If you're asked to learn Java and it feels good to you, go ahead! If it doesn't, DON'T. You are you.
