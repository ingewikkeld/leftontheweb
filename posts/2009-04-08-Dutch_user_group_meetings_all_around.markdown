---
layout: post
title: "Dutch user group meetings all around"
date: 2009-04-08 20:33
comments: true
categories: 
  - php 
  - symfony 
  - phpgg 
  - linux 
  - php 
  - usergroup 
  - event 
---
<p>With the <a href="http://phpgg.nl/" target="_blank">dutch PHP usergroup</a>  we usually organize a meeting every two months. We have one or two speakers, and the possibility to talk to eachother and network a bit. Last week we had a meeting in Amersfoort and invited Lambert Beekhuis to introduce symfony to attendees. Despite an unforunate low amount of attendees, we had a lot of fun and Lambert&#39;s presentation was probably one of the best symfony introduction talks I have seen so far.</p><p>We all expected that out next meeting would be at our TestFest event in may (to be announced soon). However, talking to <a href="http://www.lornajane.net/" target="_blank">Lorna</a>  online, we recognized her coming to the Netherlands next week was an excellent opportunity for her to speak and for us to have her speak, so after some arranging we quickly announced <a href="http://phpgg.nl/april2009" target="_blank">another meeting</a>!</p><p>And so I am very happy to announce (mainly for dutch developers) that next week on thursday, Lorna will be doing her Linux-Fu for PHP Developers talk in the Ibuildings office in Utrecht! If you want to attend, you&#39;ll have to be quick to register though, since we have limited space. Registering is possible through <a href="http://www.joind.in/event/view/37" target="_blank">joind.in</a>, <a href="http://events.linkedin.com/phpGG-Meeting/pub/55733" target="_blank">LinkedIn</a>  and <a href="http://upcoming.yahoo.com/event/2376168" target="_blank">Upcoming.org</a>. </p>
