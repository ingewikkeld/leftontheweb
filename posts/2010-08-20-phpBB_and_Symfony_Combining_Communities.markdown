---
layout: post
title: "phpBB and Symfony: Combining Communities"
date: 2010-08-20 15:17
comments: true
categories: 
  - php 
  - phpbb 
  - Symfony2 
  - symfony 
  - communities 
  - opensource 
---
<p>phpBB. Software I'll always have a love for. It's not only awesome forum software (probably the de facto standard for discussion forums right now) but it's also the first Open Source project that I truely contributed to. As a member of the Support Team and as the Support Team Leader I learned a lot about PHP I didn't yet know, but also a lot about communities, personalities and dealing with the issues at hand. </p>

<p>Some of this knowledge I'm definitely using now, as the Community Manager for Symfony. I'm again in touch with people from the community, though this time around also far more often in real life as well. And the product at hand is of course also very different from what phpBB was. I also notice that this means the community itself is quite different. Different type of people, different type of questions, different type of concerns.</p>

<h2>Different communities join forces</h2>

<p>It was fun to see though that these different communities will soon be joining forces as phpBB will adopt Symfony2 as the basis of their new version 4. It's two products I've involvement in, that I've loved for years, where I've been active in the community for years. Having this insight into both communities (though one insight is a couple of years old), let's look at these communities.</p>

<h3>phpBB</h3>

<p>PhpBB is software for discussion forums. It's mainly aimed at end-users looking to at discussion functionality to their website or simply starting a discussion forum website. It is easy to run stand-alone without any technical knowledge, and if the installer is too technical for you, there's several hosting systems that have automatic installers for the software. It can't get any easier than that. </p>

<p>The phpBB community therefore exists of mostly end-users with little technical knowledge. Within the community however there is a small but active group of PHP developers that work on either phpBB itself or extensions (MODs) of the software. In addition to this, there is also a group of developers working on skins (styles) for phpBB. And then there's of course the moderators of the phpBB.com forum as well as the support team, both having a challenging task in the community-related area. </p>

<p>In the time I was active in phpBB, I found out that the phpBB community can be extremely tough to work with. Because of the overwhelming percentage of non-technical users of the software it was sometimes hard to explain solutions in a way that it was understood by the people asking the questions, and the percentage of <a href="http://en.wikipedia.org/wiki/Troll_(Internet)">trolls</a> is relatively high as well which does not make the work any easier. Having said all this, in the end most of the work we did was still helping people who were grateful for the help, making the job a really fun one to do.</p>

<h3>Symfony</h3>

<p>Symfony is a PHP framework. It is not a finished product that end-users can take, install and run. Instead, it is used as the basis for PHP software to be written on top of. There are some hosting providers that provide one-click installations of Symfony, but that's mostly for people who only want a single installation of the framework and have several applications that run on top of the framework. </p>

<p>This means the community of Symfony is quite different from the phpBB community. There is very little end-users, and the couple of designers that are part of the community also have technical knowledge, since they have to work with Symfony itself as well. Out of the relatively big group of developers, a small group of users contribute to the project on a regular basis (for instance by writing plugins, or submitting patches to the core) and a lot of people contribute in a smaller form. </p>

<p>With some occasional exception the community seems very friendly and devoid of trolls. It helps that many of the members of the community are very like-minded (since they are all developers). The difference in level of developers is big though, so the occasional "you could've found this yourself" happens, but most of the times questions are answered in a helpful and serious way.</p>

<h2>Coming together</h2>

<p>Due to the different nature of the communities it will probably mean change for both communities. It might also mean adapting to a different kind of people. But in the end I see many benefits for both communities.</p>

<h3>More (skilled) developers</h3>

<p>With these two communities joining forces, both communities get a bigger pool of developers, and skilled developers at that, to their disposal. Assuming of course that these developers are and will want to stay active in one way or another, their work will now benefit both projects. </p>

<h3>A strong, tested basis</h3>

<p>Until now, phpBB has always been written from scratch. Though they did well, the phpBB developers usually reinvented the wheel. Using a framework (in this case Symfony2) will offer phpBB a strong basis to build upon. A basis that has been tested, both by unit tests and by many developers also building upon the same basis. There is less chance for bugs in such a codebase than in a codebase you've written from scratch yourself. Less bugs, and less code to write, means more time to focus on the functionality in the forum software, and also more time for making sure this code is stable.</p>

<h3>A good use case</h3>

<p>On the other hand, Symfony2 will benefit from having an excellent use case for the framework. The phpBB team will surely encounter situations where a certain approach is not optimal or perhaps does not make sense at all. This is feedback that can be very useful for the framework team, as it will display a problem point for many other developers as well. </p>

<h3>Better and easier integration</h3>

<p>So far, if you had a phpBB forum, it was very hard to integrate this into a website. Similarly, if you were running a Symfony-based website and you wanted a full-featured forum, you usually ran the forum seperate from the website. With both being based on the same technology, it will be much easier to integrate a website and the accompanying forum. Or you can start with a forum, and then build a whole website around it using Symfony2. </p>

<h3>Explore new markets</h3>

<p>PhpBB and Symfony both cater to very different markets. Where phpBB is usually used in end-user environments and community websites (I'm not saying it isn't used elsewhere, I just think this is the primary market of phpBB), Symfony as a framework aims more at the market of custom websites for small, medium and even large companies. With phpBB being based on Symfony2, phpBB may be employed much more in the business market where the usage of Symfony2 for end-user environments and community websites may grow.</p>

<h2>Helping out</h2>

<p>So with these two communities that are quite different from eachother, how can we help eachother to ensure that we all benefit a lot? There's several things of course that we can do.</p>

<h3>Support eachother</h3>

<p>This is probably the most obvious but clearly the most important as well. As the phpBB team starts working on the new version, the Symfony community should be supportive of their efforts and help them by answering any questions they have. Likewise though, anyone from the Symfony community that is trying to support the phpBB team should be helped in understanding the concepts of phpBB and what it is that needs to be created. </p>

<h3>Help with code</h3>

<p>Of course with two coding projects, another obvious way to help out is by contributing code. Since both projects are hosted on Github and using git, it will be very easy to make your own fork of the project code and send pull requests for your changes to the developers of the respective projects. So a developer who is working on phpBB4 may be able to spot something in Symfony2 that might be better implemented in a slightly different way can make the change in their fork, and then send a pull request to the Symfony team to ask if the change can be included in the Symfony2 core. And the other way around, a developer who is experienced with Symfony2 and is looking at the phpBB4 code may spot a bug or something that can be improved, make the changes in their local repository and then send a pull request to the phpBB team to have it included in the phpBB4 core.</p>

<h3>Other ways</h3>

<p>There are more ways of contributing, but I spent a <a href="https://skoop.dev/message/Contributing_to_Open_Source">whole article on that recently</a>, so I'm going to go into that here. </p>

<h2>Concluding</h2>

<p>I see the move of phpBB to Symfony2 as a very good one for both projects, and I am looking forward to helping out with the move. I will definitely try to add my two cents to the movement, and I hope you will too!</p>
