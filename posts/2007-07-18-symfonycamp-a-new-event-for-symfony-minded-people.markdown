---
layout: post
title: "SymfonyCamp - a new event for Symfony-minded people"
date: 2007-07-18 5:57
comments: true
categories: 
  - symfony 
---
Earlier this year we organized PHP Bootcamp, a one-day event in which we had discussions about several frameworks. It was a huge success. It was an afterthought after another idea though.

The original idea is now being realized. <a href="http://www.symfonycamp.com/">SymfonyCamp</a> is my little baby, and I'm gonna make sure this baby will be having a good time :)

It looks to become a great event. Two of the three biggest Symfony guru's out there, Fabien Potencier and Dustin Whittle, have already agreed to be present and do some sessions. Francois Zaninotto, the other guru, will hopefully also be there. And we're going to get some more people in on it as well.

Anyway, the first incarnation of the site is <a href="http://www.symfonycamp.com/">now online</a>. There's still some glitches to be fixed and the information there is still lacking a lot, but things will get better over time. Early bird discounts are available, so if the idea of SymfonyCamp sounds good to you, register soon! :)
