---
layout: post
title: "Collection"
date: 2005-02-14 2:54
comments: true
categories: 
  - leftontheweb 
---
As some of you may know, I'm slightly... uhm... geeky, in my hobbies. Aside from scripting php (which is also my work), I also collect Transformers. Yesterday, I re-organized my collection a bit and decided to also make some pictures. Here are some.Click for a bigger image.

<a href="http://pix.stefankoopmanschap.com/transformers/01_collectie" target="_blank"><img src="http://pix.stefankoopmanschap.com/albums/transformers/01_collectie.thumb.jpg" border="0"/></a>

<a href="http://pix.stefankoopmanschap.com/transformers/02_uniscreamermore" target="_blank"><img src="http://pix.stefankoopmanschap.com/albums/transformers/02_uniscreamermore.thumb.jpg" border="0" /></a>

<a href="http://pix.stefankoopmanschap.com/transformers/03_divebomb" target="_blank"><img src="http://pix.stefankoopmanschap.com/albums/transformers/03_divebomb.thumb.jpg" border="0" /></a>

<a href="http://pix.stefankoopmanschap.com/transformers/04_skyblast" target="_blank"><img src="http://pix.stefankoopmanschap.com/albums/transformers/04_skyblast.thumb.jpg" border="0" /></a>

<a href="http://pix.stefankoopmanschap.com/transformers/05_screamers" target="_blank"><img src="http://pix.stefankoopmanschap.com/albums/transformers/05_screamers.thumb.jpg" border="0" /></a>

<a href="http://pix.stefankoopmanschap.com/transformers/06_armadathrust" target="_blank"><img src="http://pix.stefankoopmanschap.com/albums/transformers/06_armadathrust.thumb.jpg" border="0" /></a>

<a href="http://pix.stefankoopmanschap.com/transformers/07_collectie" target="_blank"><img src="http://pix.stefankoopmanschap.com/albums/transformers/07_collectie.thumb.jpg" border="0" /></a>

<a href="http://pix.stefankoopmanschap.com/transformers/08_needlenosebrothers" target="_blank"><img src="http://pix.stefankoopmanschap.com/albums/transformers/08_needlenosebrothers.thumb.jpg" border="0" /></a>

<a href="http://pix.stefankoopmanschap.com/transformers/09_hooligan" target="_blank"><img src="http://pix.stefankoopmanschap.com/albums/transformers/09_hooligan.thumb.jpg" border="0" /></a>
