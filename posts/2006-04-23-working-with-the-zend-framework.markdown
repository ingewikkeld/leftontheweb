---
layout: post
title: "Working with the Zend Framework"
date: 2006-04-23 3:28
comments: true
categories: 
  - technology 
---
While the <a href="http://framework.zend.com/">Zend Framework</a> is still in an early state, I've been working on a site based on the Zend Framework. I'm extending the framework with my own logic, and so far I like what I see. Using the PDO database interface, the Zend base class for loading classes and interfaces, Zend_Filter_Input for input filtering, and I'm planning on using Zend_Feed, Zend_Service_Amazon and maybe some others.

So far I'm quite happy with how things work. It's easy to extend the functionality, it's easy to use the database classes and using the Zend base class with it's loadClass, loadInterface and it's registry functionality are wonderful.

The site I'm working on is by far not finished (it's the new back-end for <a href="http://www.fantasylibrary.net/">FantasyLibrary.net</a>) so by the time I'm finished the Zend Framework has probably progressed a lot, become more stable. Maybe, by working on using the framework, I'll be able to contribute to it's testing and maybe find/fix a bug here and there. For now, I'm just really happy with how the Zend Framework works.
