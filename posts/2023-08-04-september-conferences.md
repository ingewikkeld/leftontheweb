---
layout: post
title: "September 2023 conferences"
date: 2023-08-04 11:30:00
comments: true
categories: 
  - php
  - conferences
  - talks
  - cakefest
  - apiplatformcon
social:
    highlight_image: /_posts/images/customeralwaysright.png
    highlight_image_credits:
        name: Mike van Riel
        url: https://mikevanriel.com
    summary: While it's currently still vacation time, I'm looking forward to September. These are the conferences where you can run into me.
    
---

Yes, we're still in the middle of the summer vacation period (at least in the northern hemisphere) but I'm going to look ahead a bit towards september. A month where I have two conferences scheduled. Will I see you there?

## API Platform Con

On September 21 and 22 you can find me in Lille where I'll be at [API Platform Con](https://api-platform.com/con/2023/). On September 22 I'll me doing my PHP Kitchen Nightmares talk, which promises to be fun! I'll draw a parallel between kitchens of restaurants and our work as developers. 

## Cakefest

A week later I'll travel to Los Angeles for [CakeFest](https://cakefest.org), which takes place on September 29 and September 30. I hear it's going to be an exciting conference. At that conference I'll introduce the concept of Domain-Driven Design to the attendees. 

I hope to see you at one of these events! 