---
layout: post
title: "Online Music World - the weblog"
date: 2005-02-01 7:19
comments: true
categories: 
  - leftontheweb 
---
<a href="http://www.onlinemusicworld.com/">Online Music World</a>, which was only a <a href="http://www.onlinemusicworld.com/forum/">discussion forum</a> so far, has now added a <a href="http://log.onlinemusicworld.com/">weblog</a> to it's site. On the weblog, a team of representatives from the online and independent music scene will be writing about, well, the independent and online music scene.

The design is not finished yet, but the first post is now there. More will follow.
