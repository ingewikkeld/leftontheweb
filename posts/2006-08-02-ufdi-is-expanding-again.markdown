---
layout: post
title: "Ufdi is expanding... AGAIN!"
date: 2006-08-02 13:16
comments: true
categories: 
  - technology 
---
The <a href="http://www.ufdi.net/">Ufdi Network</a> has recently been expanding with various new members, a very good sign. Still, after the recent expansion, we were still having pure linux and mac in our ranks. Now, even though this is not a bad thing of course, it was time for some counterweight.

<a href="http://www.gideonmarken.com/">Gideon Marken</a> is, how he calls it, a Web Technologist. I like this term a lot. What I like even more is that he single-handedly set up an amazing site called <a href="http://www.artistserver.com/">ArtistServer</a>, he makes very good music as <a href="http://www.sonicwallpaper.com/">Sonic Wallpaper</a>, and is an allround good guy. He has accepted my invitation into the network and so you can tap into <a href="http://www.gideonmarken.com/">his vast base of knowledge</a> as well as that of the other network members by checking the <a href="http://www.ufdi.net/">Ufdi Network</a> on a regular basis.

Welcome to the network Gideon!
