---
layout: post
title: "Now it's for real"
date: 2006-10-25 14:39
comments: true
categories: 
  - technology 
---
My initial implementation of Zend_Http_Client_Adapter_Curl has <a href="http://framework.zend.com/fisheye/changelog/Zend_Framework?cs=1357&csize=1">just been committed</a>! Where before I only committed some small fixes to Zend_Http_Client_Adapter_Socket, now I've actually committed a real contribution in the form of real, new, self-written code. Yay! :)
