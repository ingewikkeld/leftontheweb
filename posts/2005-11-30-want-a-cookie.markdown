---
layout: post
title: "Want a cookie?"
date: 2005-11-30 2:18
comments: true
categories: 
  - technology 
---
Yesterday I left work at 8PM. Usually I leave at around 5:30PM. The cause of this? Some friggin unbelievable bug, or just plain weird behaviour, of Internet Explorer.

For some of the bigger file downloads, we cooperate with a distributed download network. To authorize users to download certain paid content, we need to set cookies.

For some inexplicable reason, setting the cookies failed on certain versions of Internet Explorer 6. We kept narrowing down the problem until we had a working testscript which did exactly the same thing as our live script, except for one thing. The expiry time of our testscript was 1500 seconds in the future, and the expiry time of our live script was 300 seconds in the future. You'd think this could not cause this problem (which is why we ignored that part of the script for so long I guess) but when we changed the expiry time on our live script to 1500 seconds, it all of a sudden started working.

So, for the record: If you are having trouble with cookies on Internet Explorer 6, check your expiry time.
