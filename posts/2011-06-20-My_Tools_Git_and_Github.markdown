---
layout: post
title: "My Tools: Git and Github"
date: 2011-06-20 6:29
comments: true
categories: 
  - tools 
  - git 
  - github 
  - php 
  - symfony 
---
<h2>Git</h2>
<p>When I started with PHP and version control, CVS was the main VCS to use. Not just because it was the best (this is actually not a fact), but mostly because all the hosting services out there supported it, most specifically, <a href="http://sf.net/">SourceForge</a> did. It worked in terms of version control, but it was not that easy to use. So when Subversion came around and became the major version control tool in the PHP world, this was a welcome change. I've used it for years and I've loved it and hated it at times.</p>

<p>Then some years ago, this new VCS started making it's rounds in the PHP community. Git quickly became the talk of the town and when project after project started switching to Git, it was time to have a good look at it. The main difference was clear: Git was a <em>distributed</em> VCS as opposed to the centralized VCS'es I was used to at the time. And while this was mostly an advantage for (distributed) teams of developers, even when working on a project by yourself it was quite useful.</p>

<p>One of the immediately advantages I saw was the fact that you can commit and thereby create a history while disconnected from the Internet. Since I travel quite a bit and don't always have a data connection available, this is an awesome feature for me. Now I didn't have to save up my commits until I got to a connection, and it was much easier for me to adhere to the atomic commits best practice.</p>

<p>There are definitely many more advantages to me though. The easy branching for instance, making it much easier for me to work on functionality and then merge it back when done. This makes it much easier to work on different functionality in parallel as well, and even also do bugfixes in the meantime.</p>

<p>And of course the team functionality. You only have to look at, for instance, the Symfony2 repository to see how easy it becomes to contribute to an open source project. And it's no different for private projects. Additionally, if you set up your processes correctly, there is much more quality control, making the code of a higher quality.</p>

<h2>Github</h2>
<p>I think <a href="http://github.com/">Github</a> contributed a lot to the success of Git in the PHP world. I've heard a lot of stories from people I trust that Mercurial is actually a much better, easier dVCS than Git. However, at the point where dVCSes started to become popular amongst PHP developers, Github actually made the difference. The collaboration features Github adds to the Git base enhances Git to a level where collaboration and contribution becomes so easy it would be stupid not to contribute and collaborate. Pull requests, the ease of setting up private repositories, easy integration with other tools, clear and well-maintained documentation, it all helps.</p>

<p>I personally use Github for most of my projects these days. Whether it is private projects I do either for myself or my company or it is public projects, Github is my main hub for VCS-hosting these days.</p>
