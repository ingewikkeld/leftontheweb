---
layout: post
title: "The Lazy Developer"
date: 2008-03-20 20:16
comments: true
categories: 
  - worldofwarcraft 
  - development 
  - bestpractices 
---
<p>I used to have a PC laptop. It had a dual boot for a single reason: World of Warcraft. I played it quite a lot last year but after a while I got a bit bored and didn&#39;t have enough time to play. Lately though, I felt like playing again, so this week I actually reactivated everything and started playing. At least I hoped it would work like that. I didn&#39;t even have to switch operating system anymore, since these days I have a Mac and OSX is supposed natively for World of Warcraft. I was wrong.</p><p>I insert the install CD, start the installer, run through the screens of the instaler up until the point where I have to select the volume where I want to install the game. I select the default disk, and then get an error. World of Warcraft can not be installed to a case sensitive volume. What?!</p><p>I blame this on lazy developers. I mean, what is so hard to either keep track of the filenames you are using, or even better, have a good naming convention so that everyone knows what files are being used? I realize that most people on a Mac have a case insensitive volume, because that is the default option. But being a developer myself, and one who deploys to (case sensitive) linux systems usually, I find it important to catch case sensitivity problems during development phase. So I conciously choose to run my Mac with a case sensitive volume.</p><p>I got it installed eventually, by formatting one of the partitions on my external USB harddrive to a case insensitive Mac volume, but I don&#39;t like this a bit. This could&#39;ve been prevented, and very easily as well. I also feel that if you sell software, and you decide to support a certain platform, you should support the different default configuration options of that platform.</p><p>Developers: be proud of the code you write. Problems like this can be easily prevented. Prevent them!&nbsp;</p>
