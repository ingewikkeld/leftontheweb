---
layout: post
title: "Introducing MediTerra"
date: 2010-04-08 18:14
comments: true
categories: 
  - php 
  - winphp 
  - azure 
  - microsoft 
  - mediterra 
---
<div>First of all, I&#39;ve decided to name the app MediTerra. I didn&#39;t want to use the standard <em>php*</em> type of naming. Since I&#39;m going to be building on the Windows Azure technology, I let my thoughts go a bit. Now, in the mediterranean, the color of the sea is the sort of blue that some might call azure, which led me to consider variations on that, which ended with MediTerra. It sounds nice, and has absolutely nothing to do with the application that I&#39;m going to build other than the technology it is built on ;)</div><div><br /></div><div>So what is it going to do? In short, one could look at this application as the phpMyAdmin for Azure Storage. Basically, what I want to do is build an application that can help you manage your Azure Storage entries. List them, show them, edit them, delete them, add them. And any functionality around it that might be useful to you. It will mostly be a tool for developers obviously, so if you have any feedback on what you think would be useful for this, I would love to hear it!</div><div><br /></div><div>During the development (and afterwards) I will be sharing the code on&nbsp;<a href="http://github.com/skoop/MediTerra" target="_blank">Github</a>. So at any point, you will be able to pull the code or even fork it if you want to.</div><div><br /></div><div>So there you go. Now it&#39;s time for me to set up my Windows development environment, to see how I can get all of this up and running. I&#39;ll report back on that experience later.</div>
