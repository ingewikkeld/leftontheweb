---
layout: post
title: "PHPStorm & OSX: Mouse Stopped Working"
date: 2016-10-03 22:00:00
comments: true
categories: 
  - php 
  - phpstorm
  - mouse
  - zoom
  - osx
  
---

This is just as much a post for future me as it is for the rest of the world.

While I was working on preparing a training for the end of the week and was using PHPStorm to create some example code, all of a sudden my PHPStorm stopped responding to my clicks. I could click whatever I wanted, it just would not work anymore. As you can imagine, this is quite frustrating when you're trying to get some work done. The weird thing was: My trackpad worked anywhere else in OSX, just not in PHPStorm.

I took to Twitter to vent my frustration, then tried the  more constructive approach of tweeting to @PHPStorm with this problem:

> @phpstorm somehow my PHPStorm does not respond to mouseclicks anymore, just to keyboard strokes. What can I do to fix this?

Kevin Boyd [quickly responded](https://twitter.com/Beryllium9/status/783029329118638080) with a [link to the Jetbrains forum](https://intellij-support.jetbrains.com/hc/en-us/community/posts/207277409-Mouse-interactions-not-working-also-updates-not-working). The problem seemed similar, but aside from "reinstall the IDE" there was not really a good solution there. While I was searching for other people that may have the same problem, [another tweet](https://twitter.com/damon__jones/status/783030194017800192) came in, from Damon Jones:

> @skoop @phpstorm Mac? You've zoomed the window slightly (pinch to zoom). Zoom back to normal and you should be okay.

OK, that seems very weird, but let's see what happens if I "unzoom" while in PHPStorm. WHOA! This was it! Apparently, while using my trackpad in PHPStorm I had accidentally pinched to zoom _just a little bit_. Not enough to be noticable, but enough to make PHPStorm stop responding to any mouse interaction.

Thank you Damon. I owe you a beer at a conference some time.