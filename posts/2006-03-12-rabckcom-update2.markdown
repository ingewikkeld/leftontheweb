---
layout: post
title: "RABCK.com update"
date: 2006-03-12 10:34
comments: true
categories: 
  - books 
---
 I've just updated <a href="http://www.rabck.com/">RABCK.com</a> with a slightly altered design and some new features.

<li> It is now possible to add your birthday to your profile. This was a feature request that I've received often, it seems a lot of people like to send a RABCK for people's birthdays :)</li>
<li> Search has been expanded. You can now search for people that prefer a certain genre within a certain country.</li>

Luckily, I've only had a few reports of people with login trouble, and most were easily resolved.

Enjoy!
