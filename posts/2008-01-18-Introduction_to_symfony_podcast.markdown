---
layout: post
title: "Introduction to symfony podcast"
date: 2008-01-18 15:22
comments: true
categories: 
  - symfony 
  - phpabstract 
  - php 
  - technology 
  - podcast 
---
It actually is a very fun thing to do, so if you have a topic you can tell the world about, you really should contact Cal (info on <a href="http://devzone.zend.com/article/2046-Announcing-PHP-Abstract-DevZones-new-PodCast-for-PHP-Developers" target="_blank">this page</a>). Anyway, back to my podcast. I am introducing symfony with this podcast, so I give a basic overview of some of it&#39;s features and characteristics. But, why am I explaining this, while the podcast can do so much better. <a href="http://devzone.zend.com/article/2981-PHP-Abstract-Podcast-Episode-32-Introduction-to-symfony" target="_blank">Check it out here</a>!
