---
layout: post
title: "AJAX with OpenRico"
date: 2005-07-13 8:42
comments: true
categories: 
  - leftontheweb 
---
The last week I've been playing around a lot with the javascript xmlHttpRequest functionality that is these days better know as AJAX (what a <a href="http://www.ajax.nl/">name</a>!). A client requested me to implement it, or at least look at the possibilities of implementing it, in their intranet. And I must say, there are tons of possibilities where using AJAX could highly enhance the user experience. No need for reloading your page anymore. AJAX will take care of it.

I've been using a very specific AJAX implementation called <a href="http://www.openrico.org/" rel="tag">OpenRico</a>, which is a very useful library with lots of possibilities. Very flexible. Very useful.

Using AJAX, I must admit that I've seen a renewed need to get into javascripting. Indeed, OpenRico provides an implentation that ensures I won't have to do all the very complicated javascripting, but still implementing it into a system will still require you to at least know some basic javascripting. Most of it is pretty basic stuff, with sometimes slightly more complicated stuff for specific situations. But still, it was a good reason to get my some basic javascript skills.
