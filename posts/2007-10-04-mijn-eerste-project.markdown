---
layout: post
title: "Mijn Eerste Project"
date: 2007-10-04 15:23
comments: true
categories: 
  - symfony 
---
In a terrible spur of symfony community productivity, I just also finished the <a href="http://trac.symfony-project.com/wiki/Documentation/nl_NL/my_first_project">dutch translation</a> of the <a href="http://www.symfony-project.com/tutorial/1_0/my-first-project">My First Project tutorial</a> for symfony. For people looking to get introduced to symfony, this is probably the best starting point. After that, <a href="http://www.symfony-project.com/askeet/1_0/">Askeet</a> will get you more in-depth into the framework, but the one-hour tutorial is the best place to get introduced. I hope that by translating this tutorial, dutch developers will have an easier time getting to know the symfony basics.
