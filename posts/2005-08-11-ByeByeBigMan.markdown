---
layout: post
title: "Bye Bye Big Man"
date: 2005-08-11 8:20
comments: true
categories: 
  - personal 
---
If you&#39;ve seen <a href="http://www.imdb.com/title/tt0319061/">Big Fish</a>, then you will remember the huge guy. That guy was <strong>Matthew McGrory</strong>. Unfortunately, <a href="http://www.foxnews.com/story/0,2933,165355,00.html">he passed away</a>, apparently of natural causes. He was only 32. :(
