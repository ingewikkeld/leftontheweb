---
layout: post
title: "Forcing a date on your Vagrant box"
date: 2013-10-08 16:30
comments: true
categories: 
  - vagrant 
  - date 
  - debugging
---
I recently received a bugreport from a client that was very hard to debug: There were only very specific moments where the bug was reproducable, and those moments were usually in the weekend. So I decided to change the date on my Vagrant-boxes for the API and website to last Sunday to try and reproduce the problem. 

```
[01:24 PM]-[vagrant@dev]-[~]
$ sudo date --set="6 OCT 2013 15:20:00"
Sun Oct  6 15:20:00 UTC 2013
```

After updating the date and refreshing the page, the bug did not actually occur. Odd! Going back to the Vagrant shell, I found out, however, that the box had switched back to today's date. A quick Google search hinted at stopping the ntp service, so I did:

```
sudo /etc/init.d/ntp stop
```

I tried again, but again, it didn't work. After more Googling around I could not find the solution, so I decided to ask my friends at [Dutch Web Alliance](http://www.dutchweballiance.nl/) to see if anyone knew. After some discussion, we concluded that it was probably VirtualBox automatically updating the time. That sort of makes sense of course. Now, how to configure Vagrant to not do this. [Joshua](http://twitter.com/JayTaph) came up with the solution:

``` ruby Configure VirtualBox to block it from automatically updating the time on the VM
    v.customize ["setextradata", :id, "VBoxInternal/Devices/VMMDev/0/Config/GetHostTimeDisabled", 1]
```

Now, after reloading my Vagrant boxes and again setting the date, I was able to reproduce the bug my client reported! 
