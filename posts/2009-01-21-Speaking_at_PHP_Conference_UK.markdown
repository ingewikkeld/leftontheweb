---
layout: post
title: "Speaking at PHP Conference UK"
date: 2009-01-21 9:32
comments: true
categories: 
  - symfony 
  - php 
  - conference 
  - london 
---
<p>This talk will focus on the different myths and prejudices that people have against symfony. Symfony is supposed to be slow, extremely coupled and overengineered. To work with symfony you don&#39;t do PHP anymore but only work with YAML configuration files. It&#39;s things like this which I will test during this talk. I will either have a myth BUSTED, a PLAUSIBLE verdict or even confirm myths... we&#39;ll see.</p><p>For those not in the London area: I will be giving this talk also during PHPCon Italia in Rome in March, and I may also propose it for an uncon session at PHP|Tek if there is interest in this topic stateside. </p>
