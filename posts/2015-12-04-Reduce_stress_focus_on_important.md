---
layout: post
title: "Reduce stress: Focus on what is important"
date: 2015-12-04 09:30:00
comments: true
categories: 
  - php 
  - productivity
  - email
  - tweetbot  
---

Last Friday I had a great, long meeting with [Mike van Riel](http://blog.naenius.com/). There we many things to discuss, but one of the things we discussed was productivity, or rather the feeling of stress you can have even if you shouldn't, and how to counter that.

Mike's done quite a bit of research on this topic (he promised he would publish some of that research once he can), and during that meeting (and afterwards as well) we came up with a couple of lifehacks that would hopefully make our life a bit easier.

## E-mail

I have many e-mailaddresses. Many, many e-mailaddresses. While going through my e-mail does not take me a whole lot of time, it can sometimes be distracting. Then again, I definitely need my company e-mailaddress for communication about the project(s) I'm working on, so I can't (and won't) just close my e-mailclient and ignore it for a couple of hours.

A simple change I made, though, was to seperate my Ingewikkeld e-mailaddress from the other adresses by using a different e-mailclient. I now have Airmail for all my personal mail and Thunderbird for my work e-mail. This split up works surprisingly well in removing the feeling of stress because "you have so many things going on". Simple, but effective.

## Twitter

On a regular basis, I close my Tweetbot to get some work done. Sometimes it works really well in terms of focus. However, often, Twitter is actually an integral part of my job. Talks I have with people because I've in into a problem, helping other people, or just discussing things to clear my mind off a problem I'm stuck on. 

Mike came up with a great question though: 

  > When you look at Twitter, what is most important to you? Is that the order of the columns you've set your Tweetdeck up with?
  
I had to admit that the columns were set up in the most standard way possible (timeline @skoop, mentions @skoop, timeline @skoopnl, mentions @skoopnl). When I started thinking about it I basically came to the conclusion that tweets by PHPeople were the most important, as well as tweets by some other people that are important. Then the mentions, and after that, my general timeline. Mike suggested I set up my Tweetbot in a way to accomodate these priorities. So this morning, I've sat down to re-organize my Tweetbot to accomodate this. I've created two private lists (PHPeople for PHP-related tweets, Personal stuff for non-PHP people of whom I value the tweets). Those are my first two columns. Then the mentions columns for both accounts, and after that the main timelines.

It takes some getting used to the different layout, but already I notice two things:

- My main "timelines" (the first two columns) are much slower. I like that.
- I have a better feeling of not missing out on important tweets

![Tweetbot](/_posts/images/tweetbot.png) 

## I'm sure there's more

Sometimes small changes can have a big impact and I've already noticed the impact of these two relatively small changes. Look at the things you do often and see how they can be improved. And if you've found some, feel free to share them as well!