---
layout: post
title: "Kratarknathrak"
date: 2005-11-05 9:55
comments: true
categories: 
  - music 
---
I know it's been silent around my <a href="http://www.laidbackelectronica.com/">netlabel</a> recently, but finally there's a new release available. The artist that was responsible for the <a href="http://www.laidbackelectronica.com/release.php?id=le000">very first release on the label</a> is back! His new release, <a href="http://www.laidbackelectronica.com/release.php?id=le010">Dogun Teatime</a>, is beautiful! Slightly darker than the first release. Check it out!
