---
layout: post
title: The value of the external developer
date: 2024-02-13 17:00:00
comments: true
categories: 
  - php
  - externaldevelopers
  - contracting
  - freelancers
  - consulting
social:
    highlight_image: /_posts/images/puzzle.png
    highlight_image_credits:
        name: Sigmund
        url: https://unsplash.com/photos/brown-and-black-jigsaw-puzzle-B-x4VaIriRc
    summary: External developers are expensive. At least, that seems to be a bias existing in the minds of a lot of managers. But are they? And what is the value you can get out of an external developer on your team. In this blogpost, I go into some of the important value you get out of hiring an external developer.
    
---

*Full disclosure*: As owner of a company that supplies external developers and as an external developer myself, I have a lot of experience and I also obviously earn my money this way. Keep that in mind when reading this. Also: I speak from my own experience in this blogpost, but there are a lot of freelancers and consulting agencies. You don't have to hire me or one of the [Ingewikkeld](https://ingewikkeld.dev/) people (of course **you can!**). The same goes for a lot of other external developers that you can hire.

---

Over the years I have worked for a variety of different companies as an external developer both as a part of existing teams or the builder of new ones. Sometimes to help just with the software development, but often to be what I call a "developer++": Aside from helping with software development I am involved with all aspects of development, from architecture to coaching individual developers or even the organization. In some cases I was there for a year or even longer, in other cases it was just a few weeks, in some situations it was simply an hour to brainstorm.

There is not one single thing that is the value of the external developer, so in this blogpost I want to go through several different advantages you as a hiring organization can get out of using external developers in your organization.

## A fresh look

This is one of the most underrated advantages of getting in external developers: They bring in a fresh look at the things you are doing and the way you are doing it. If you're fully immersed in a certain way of doing things then it can really help to get someone from the outside to give you feedback on that. From the inside, you might not see everything clearly anymore. 

As an example, at one of my previous customers they had made the decision to build on top of an e-commerce framework for their headless e-commerce solution. Their software development was grinding to a halt, but they did not understand why. As I came in, I noticed that the e-commerce framework they had chosen was holding them back more than it was helping them. It would be a bad choice to throw away what they had built, but going forward for the new functionality it would be more productive to leave the framework where it was, and focus on pure Laravel development instead. After we made that decision, development became much more efficient and the rest of the functionality was implemented quickly.

## Temporary help or ramping up to your new developer

A big percentage of the projects we do, we simply provide extra development help. Now it isn't always productive to add extra developers to a project that is already late and in the acquisition process I am often critical about this, but there are definitely situations where the temporary extra help can be a benefit to your project. I've found that many external developers are very used to quickly being up-and-running and understanding the problem at hand. 

Additionally, if you have a vacancy open, it can sometimes help to get in an external developer for the period until you've found someone to fill the vacancy on a permanent basis. The external developer can ensure there is already some clarity about the exact role of the new hire, and also can make sure that the new developer can hit the ground running because a lot of the setup work has already been done for the new position.

## The amount of experience

External developers switch companies and projects on a regular basis. This results in an incredible amount of experience with different situations and different people. The result of that is that there's a lot of different situations and technical challenges that they have already encountered. They bring that combination of experiences to the table on any new project, resulting in one or even more possible great solutions for any challenge you run into. 

Dealing with all kinds of different people also really helps with the communication skills. This is why on average external developers are more skilled at identifying and bringing to light possible communication issues in a team. 

## Get it documented

Based on our experiences, internal developers are less likely to correctly document decisions and technical setup. This causes issues for instance with on-boarding new developers or figuring out the reasoning behind certain technical decisions after a few months or years. 

External developers know that they're temporary, that they will leave at some point. The result of that is that they're more focussed on making sure decisions and knowledge about the codebase is documented so that those who come after them can continue the work. This rubs off on the internal team as well usually, triggering them to be better at documenting. The result is code that is better documented, developer on-boarding that can be done much more efficiently and overall more knowledge among team members about the codebase and the problems that are being solved.

## Smells like team spirit

Building software is one thing, but building a team is another. That's not easy. And it's really important to have a good team. A good team is able to act on changes faster and is more predictable in the quality of the work they deliver. A good team needs a good leader to keep the team together and make sure it works well together. Often when teams don't work together well there is no leader, or the leader is not able to really keep the team together. External developers can be that glue for the team, and can coach one of the permanent team members to take that role.

Talking from my own experience a lot of places where I've worked in the past decade did not have a team. They had a group of individual developers. And while the individual developers can get a lot of work done, making them a team, where the total is more than the sum of its parts, is something that needs attention. Because of external developers working for a lot of different companies, projects and teams, they usually have a good overview of what needs to be done to truly have a team. They can have that leadership role, or do the coaching of the leader to take on that role.

## Get rid of your bias

OK, story time. Some years ago I started at a new customer. The small team of internal developers were reporting to their boss, a former software developer turned entrepreneur. He had built the company himself on top of his own software, but he had not been developing for years as the company took off. 

The team came up with several suggestions to improve the software. But they were rejected by the boss because it was unnecessary, or too much work, or there was another reason why it was not a good idea. When I came in, I started talking to the internal developers and I actually quite liked their ideas. They were ideas based on current knowledge of good software development practices. They were definitely improvements. So I started proposing the exact same ideas to the manager. And with the proposals coming from me, the manager said "oh, that sounds like a good idea, let's do that!"

While it is not good that there was somehow no trust in the internal developers, that situation sometimes happens after years and years of being stuck in a certain flow. Sometimes it's good to have external developers challenge your bias against your own people. Especially when you were a developer before so you think you have good knowledge of the matter at hand. 

## So challenge yourself

So, challenge yourself and your team and get an external developer on board. You don't have to permanently have an external developer in your team, but having one in your team for a few months on a regular basis will already help you and your whole team improve and challenge you to think about the way you do things and your focus on both the technical and the human level. And as I said at the start, you can surely hire one of the experts of [Ingewikkeld](https://ingewikkeld.dev/), but there's a lot more talent out there on freelance, contract or consultancy basis. **Need help selecting the right developer?** Feel free to [get in touch with me](https://ingewikkeld.dev/contact/). Even if you don't choose an Ingewikkeld developer, I'm happy to help you with your selection process.