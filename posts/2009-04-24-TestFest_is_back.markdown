---
layout: post
title: "TestFest is back!"
date: 2009-04-24 19:55
comments: true
categories: 
  - php 
  - testfest 
  - symfony 
  - zendframework 
  - qa 
---
<p>In the Netherlands, this year&#39;s TestFest instance is in Utrecht at the Hogeschool Domstad. We have the pleasure of having Pierre Joye over to the Netherlands to mentor the TestFest (thanks to Microsoft for supporting us in this). Pierre will, together with all attendees, go through the steps of how to write tests and then we&#39;ll go and actually write tests for PHP. Last year, the combined Dutch/Belgian TestFest instance was one of the most successful instances worldwide, so of course we will seek to continue the success this year! So if you&#39;re from the Netherlands, feel free to come by on may 9th! Please do register though (see <a href="http://phpgg.nl/testfest2009" target="_blank">info here</a>).</p><p>However, if you&#39;re not from the Netherlands, feel free to check the <a href="http://wiki.php.net/qa/testfest" target="_blank">PHP wiki</a>  for a TestFest in your neighbourhood. One more TestFest I want to highlight is the one I will be involved in over in Chicago during <a href="http://tek.mtacon.com/" target="_blank">PHP|Tek</a>. With the help of Matthew Turland and his friends and the Chicago PHP Usergroup, we&#39;ll be doing a PHP TestFest during the PHP <a href="http://tek.mtacon.com/c/s/hackathon" target="_blank">Hackathon</a>  which is part of the PHP|Tek Unconference. A great opportunity to contribute to PHP while at a conference! We&#39;re working on the details, so more info will follow later.</p><p>Most important, no matter where you attend, is that you contribute to the stability of the language we all love, as well as have fun in the process! So head on over to one of the TestFests! </p>
