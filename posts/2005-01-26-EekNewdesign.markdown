---
layout: post
title: "Eek! New design!"
date: 2005-01-26 8:56
comments: true
categories: 
  - leftontheweb 
---
Don't be scared. This is still the same site. I just changed the design... uhm, slightly? ;)

The default Pivot template is, well, a bit boring of course. And one is expected to alter it. And so I did. I based this design on <a href="http://www.chrisjdavis.org/persian/">Persian for WordPress</a>. Of course, with Pivot's slightly different setup, I had to alter it slightly. But it works well imho. 

Only thing I know is that in Internet Explorer something is not going well, but that might also be related to the Calvin &amp; Hobbes comic I posted earlier, because the image is a bit too wide for this design. I'll tweak a bit and see how it works :)

Anyway, enjoy :)
