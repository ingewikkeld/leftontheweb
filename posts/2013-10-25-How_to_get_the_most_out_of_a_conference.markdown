---
layout: post
title: "How to get the most out of a conference"
date: 2013-10-25 16:00
comments: true
categories: 
  - php 
  - community 
  - conference 
  - learning
---
At the most excellent [PHPNW conference](http://conference.phpnw.org.uk), [Kat](http://twitter.com/binarykitten) convinced me to deliver the first unconference talk of the day. It took me a while to get the right topic. I ended up with a topic I felt everyone at the conference could use for the rest of the two days that they were there: [How to get the most out of a conference](http://joind.in/talk/view/9469). For those that were not there, I want to try and put my unconference talk into a blogpost, so that everyone can use this information for their next conference.

Learn from the best
-------------------
This is the most obvious part of visiting any conference: Attend the talks of the conference. Make a list upfront based on the published schedule of the talks you want to see. It's good to give these talks some kind of priority-grade: Make a list of the talks you really want to see, the talks you think might be good and the talks you could attend if there is nothing else to do. Why? Because there's so much more going on at conferences. I regularly skip talks that I marked as talks I wanted to see because of a variety of reasons, many of which I'll talk about later in this blogpost.

One of the most important: Keep an eye on the unconference schedule if there is one. Usually, the unconference schedule isn't available before the conference starts, but there might be a talk in the unconference that you really want to see. If it is scheduled against one of your "I really want to see this" talks, then you could skip the unconference talk, but if it is scheduled against a "if there's nothing else, then I could visit this"-talk, then it makes the decision to skip that talk and go to the unconference a lot easier.

Learn and meet the best
-----------------------
Talk to any regular conference-delegate and you'll find out one of the most important aspects of every conference is the hallway track. This is an informal track taking place in the hallway, which usually consists of people meeting eachother, talking about their work, their open source projects, or any other topic that might be interesting. It's the best place to meet up with the speakers or the other delegates. Meeting them can be useful because you might have an interesting discussion with them, but it's also useful when you get back to your office. People are much more inclined to help people through Twitter, e-mail or IRC when they've met someone in real life. So even just meeting someone for a couple of minutes may already be really useful.

Sponsors are your friends
-------------------------
Any conference with a sponsor exhibit has a place to meet awesome people, discover new technologies and most of all get you free stuff. Sponsors are your friends. In the first place because their financial contribution to the conference means your ticket price stays low. But aside from that, the sponsor exhibit is where you can see what cool tech your sponsors work with, what products they have to offer, and also what job openings they have. Even if it doesn't directly benefit you, it may at some later point: You might find a cool new job, encounter new software that may help you solve a future problem, or find your new favorite shirt to wear.

Find your new colleagues (or new friends)
-----------------------------------------
If you are hiring (or your boss is), chances are you might meet your new colleague at a conference. The first time I met [Mike van Riel](http://twitter.com/mvriel) was at Dutch PHP Conference. Back then, his employer was hiring and I was looking for a job, and I ended up working for [that company](http://unet.nl/). Now, it's the other way around, he is working for [my company](http://php.ingewikkeld.net/). We also regularly play D&D and have become good friends. So don't just stick with people you know already, but go out and meet new people.

Socials!
--------
If you're looking to meet new people (or hang out with your friends), be sure not to skip the social events. Whether it's drinks, a game night, a beer tasting, or whatever other social events a conference organizer can think of, attend it to both have some fun and meet people. 

The backchannels
----------------
While at the conference, don't forget the backchannels. Twitter and IRC are both valuable channels for communication while at a conference. You might find out about cool technology by people who are attending talks you aren't watching, you might find out about cool talks later in the day that get recommended by other people, or just talk about the cool stuff you are discovering at the conference. Be sure to be an active participant in the backchannels.

Feedback is important
---------------------
The benefit of leaving feedback on [joind.in](http://joind.in) is not a direct benefit. Some conference might raffle goodies to people who left feedback, but leaving feedback is more of a long-term benefit: It allows the speaker to improve their talks and it allows conference organizers to pick the good talks out of all the submissions they receive for their conference. Also, if you give feedback to the conference organizers, next year's conference might be even better because they may be able to act upon your feedback. Don't just leave positive feedback. Constructive criticism is just as valuable (or possibly even more valuable) than just praise. Don't be afraid to leave such constructive criticism: Both speakers and conference organizers are usually quite happy to receive this kind of feedback because it helps them improve. If you're really scared, you can leave the feedback anonymously on joind.in, or leave it in private on joind.in. Of course, you can also just talk to the speaker or organizer and give them the feedback directly.

Hack away!
----------
Some conferences organize hackathons, either seperately or as part of a social event. Hackathons are a great way of improving your own skills by becoming part of a (temporary) team of developers working on open source projects. It is also good for your reputation in the community, since you actually contribute to open source projects and show off your own skills.  Don't be afraid: project developers won't bite, and they'll gladly help you get up-and-running with their project. Every little bugfix is welcome!

Start speaking yourself
-----------------------
If there is an unconference and you would like to get started with speaking yourself, the unconference is a great way of doing just that. The unconference is much more informal, and people will give you good and honest feedback on your talk. Usually, unconferences take a slightly wider range of talk subjects than the main conference, and most of the time it's possible not to have to fill a complete 50-minute slot, usually there's room for short (lightning) talks or have talk slots, so you can even prepare a short talk while you're at the conference. If you're interested in speaking, be sure to be on the lookout for the unconference.

Be active and involved
----------------------
Most important is to not just show up and sit in the talks. Be active and involved as a delegate. Ask questions, talk to people, be an active part of the conference. That makes the conference a better place and will benefit you as well! I'll see you at the next conference!
