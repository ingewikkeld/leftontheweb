---
layout: post
title: "Surface Book 2 Nvidia card not recognized"
date: 2018-09-28 13:20:00
comments: true
categories: 
  - microsoft
  - surfacebook2
  - nvidia
  - troubleshooting
social:
    highlight_image: /_posts/images/nvidia-not-found.png    
    summary: While I'm very happy still with my Surface Book 2 one annoying thing has popped up: The Nvidia GPU is sometimes "lost".
    
---

So I love my Surface Book 2. It is an amazing laptop and tablet hybrid. There's very little issues I have with it. But since a couple of weeks I keep having issues with the video card. The Surface Book 2 has 2 video cards: an Intel UHD 620 card for the tablet, and an Nvidia GeForce GTX 1050 when the Surface is connected to the keyboard section for _stunning_ graphics. Especially when playing games I recently have a very low FPS, and I had no idea what could be causing it.

When I opened my device manager, I was shocked to see that the Nvidia card was not there. I started searching the Internet and apparently, this is an issue that has been plaguing more Surface Book users. Eventually I found a solution [as posted by Philip Aaron](https://answers.microsoft.com/en-us/surface/forum/surfbook2-surfupdate/gpu-not-detected-in-surface-book-2/774e1993-4bd4-4e5b-a0b6-0b3197fedb42) that worked for me!

1. Open the device manager so you can see which display adapters Windows sees
2. Disconnect power from the laptop
3. Detach the Surface from the keyboard base
4. Wait until the device manager has reloaded its devices
5. Attach the Surface back to the keyboard base
6. Wait until the device manager has reloaded its devices
7. Now the Nvidia card should be recognized again, connect the power again

Having to do this every time I start the computer is rather annoying, so I've contacted Microsoft to see if there is a permanent solution, but for now, this at least solves my FPS issues.