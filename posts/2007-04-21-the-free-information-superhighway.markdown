---
layout: post
title: "The free information superhighway"
date: 2007-04-21 11:57
comments: true
categories: 
  - world 
---
Knowledge is power, is what they always say. Until now, knowledge has always been a privilege for those with at least enough money to get through education. And even there, I'm sure there's a lot of people that are way more intelligent and capable of more than they have been educated for.

Slowly, bit by bit, the world of knowledge, of information, is changing. Sites like <a href="http://www.wikipedia.org/">Wikipedia</a> have been working on this for a very long time already. It's a community effort, and though not all information is guaranteed to be 100% accurate, the community validation effort does ensure that most information is more or less correct.

Up until now, map data has been "owned" by commercial companies like <a href="http://www.teleatlas.com/">TeleAtlas</a> and <a href="http://www.navteq.com/">Navteq</a>, who make it their living to gather geographical data and to sell this information to the world. This information costs quite a bit of money. There is a reason, beside the fact that the shareholders want to earn some money, that you pay quite a sum of money for a map for your TomTom. 

Meet <a href="http://www.openstreetmap.org/">Open Street Map</a>, another community effort. Open Street Map is a community effort to gather map data similar to that gathered by TeleAtlas and Navteq. Using your GPS device, you make tracks of GPS data. Then, with special software, you have to edit this into actual information about the roads you tracked. Then, you can upload this data onto the OSM servers, and out comes a nice new map using the data you just uploaded.

It takes a bit of time to understand how to work with this, but it's a matter of just trying and it will work. I've already made a very small preliminary <a href="http://informationfreeway.org/?lat=6815582.45041&lon=602378.59144&zoom=12&layers=B000">map of Woudenberg</a>, and I intend to expand this with as much info as I can gather.

I have the weird idea, that given enough time, all information that is currently being sold by commercial companies, will end up with a community effort that is available for free.  
