---
layout: post
title: "Lessons learned from organizing an online conference"
date: 2020-08-25 20:20:00
comments: true
categories: 
  - php
  - conferences
  - comference
social:
    highlight_image: /_posts/images/comference-blog.png
    summary: Last week we organized Comference, an online conference from the comfort of your own home. Or office. Or wherever. This post contains some lessons learned.
    
---

Earlier this year we [made the tough decision to cancel WeCamp 2020](https://skoop.dev/blog/2020/06/05/Cancelling-WeCamp-2020/) due to the on-going COVID-19 pandemic. It was not an easy decision but looking back at it now, I think we made the right decision. We honestly could not have justify letting people travel to an island. WeCamp was supposed to have happened last week. We (at [Ingewikkeld](https://ingewikkeld.dev) immediately came up with a new idea however, because we didn't want to sit still and do nothing. One of the things that makes us proud is to facilitate the sharing of knowledge. So we came up with [Comference](https://comference.online).

Comference took place last week, and organizing the event was very different from WeCamp. As such, I learned a few lessons organizing the event that I hadn't with WeCamp. This blogpost is an attempt to document some of those lessons.

## Organizing an online event is still a lot of work

Even if an online event makes a lot less logistical planning than a physical event, it is still a lot of work to organize an online event. Of course, getting sponsors is a lot of work, but that's about the same as for a physical event. But there's so much to do. Some of the things we needed to do was:

- Figuring out the tech stack for streaming the event
- Inviting speakers and creating the schedule
- Thinking of fun activities that you can do online

Especially in a world where physical meetings are just about impossible, the communication for all of this also had to happen 100% online.

Also, since this was our first online event, I kept looking for things I had not considered. Our TODO sometimes kept growing instead of getting smaller because of the things that we thought of at a later point.

## Testing has to be realistic

Luckily, most of Comference went pretty smooth, but we had some weird issues, mostly with our audio. This turned out to be due to the (perhaps slightly amateuristic) tech setup we had for our event. As far as we have been able to analyse so far, it had to do with the fact that we used two [Elgato HD60 S](https://www.elgato.com/en/gaming/game-capture-hd60-s) devices connected through a USB hub to our streaming computer. The device itself is fantastic and works really well, but it seems that if you have two of them connected to the same computer, _and_ you use them for hours on end, either the device or Windows has an issue with that.

Which leads me to the lesson: Yes, we tested the whole setup before the event. We tested all aspects: Local speakers, remote speakers, getting local speakers' slides into the stream, the audio from our local mics, the audio from remote speakers, etc. But we only tested for 10-15 minutes. We did not do a longer test run.

When we first encountered an issue where the audio dropped, we started analysing the problem and could not find it. Big heads up to [my son Tomas](https://www.twitch.tv/ysiontwitch) for helping us out here. He was the one that came up with the idea of disconnecting one of the Elgato devices and connecting it directly to the computer. After that, OBS had input again and we could move on. 

I should've known this from my development work, but ah well. It wasn't a major issue (it happened 2 or 3 times and was fixed in a matter of minutes) and our attendees were very forgiving ♥.

## A good community tool adds a lot of value to your event

Now, I had learned this lesson at [The Online PHP Conference](https://thephp.cc/dates/2020/05/the-online-php-conference) by the awesome people of [The PHP.cc](https://thephp.cc/) already. They combined Zoom with Slack for their conference and that worked really well. During a talk there was a lot of interaction by attendees in the Slack. For us it was mostly a matter of: which of the available tools do we want to use?

Because we were also organizing a game night and a Dungeons & Dragons session, one of our main requirements was to have a system that allowed both text chat and group voice chat. Since Slack only supports group voice chat for their paid plan and Discord is very gamer-oriented we decided to go for Discord. This turned out to be an extra good choice once one of our speakers decided to have break out sessions with smaller groups doing exercises. The voice chat could be used for that as well.

The choice for a good community tool worked out really well. During the talks there were excellent discussions going on, that in some cases lasted until after the talk was already done for some time.

## OBS is awesome software

[OBS](https://obsproject.com/) is awesome software. It's as simple as that. It is amazing that the open source community is able to come up with such a professional video streaming tool. I will be sure to [make a donation](https://obsproject.com/contribute).

## Online conferences do work

I have never really been very excited about the idea of online conferences. But in the current situation with no or very little physical conferences in the foreseeable future and with me really liking conferences and learning a lot while attending them, we need to find something that works.

When organized well, online conferences really work well. With enough breaks (we did a 15 minute break between each talk, slightly longer if a speaker didn't use their 60 minute slot fully) you don't get tired that much. With a good community system set up to accommodate both the hallway track and solid discussions on the subject at hand, an extra dimension is added that physical conferences don't even really have (I would consider it rude to start talking to each other while a speaker is on stage ;) ). Now, I'm actually quite excited about the concept of online conferences, and I may attend more in the future, or maybe even organize more in the future...