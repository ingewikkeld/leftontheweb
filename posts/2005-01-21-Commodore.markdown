---
layout: post
title: "Commodore!"
date: 2005-01-21 8:07
comments: true
categories: 
  - leftontheweb 
---
Retro just got a bit better. Finally, it's been released! <a href="http://www.commodoreshop.com/commodoreshop/">The C64 Direct-to-TV unit</a>! Finally we can play all those old games again, straight on our TV, with this simple unit. And for a quite acceptable price.
