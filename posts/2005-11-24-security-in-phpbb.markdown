---
layout: post
title: "Security in phpBB"
date: 2005-11-24 2:15
comments: true
categories: 
  - technology 
---
The following is a reply I wrote to <a href="http://www.filipdewaard.com/archives/34-Real-life-PHP-Security-Breach.html">an article by Filip de Waard</a>. Unfortunately, commenting was turned off on his weblog, so I could not comment on his weblog. Here is my comment:

_buggy PHP scripts like PhpBB_

Of course, being part of the phpBB team, I want to respond to this. Though I definitely agree that there have been bugs (hard to deny that), I must say I've never seen it being blamed on PHP. On the other hand, I have seen, a year ago or so, a PHP bug being blamed on phpBB.

It's easy to bash phpBB for containing bugs, but I have yet to encounter software that is completely bugfree. And especially when software is as popular as phpBB, people start actively looking for the bugs. This helps, when the bugs get reported to the team before being made public, but more often than not people who find bugs are more interested in their own 15 minutes of fame than the security of users worldwide, and they publish the issues without reporting them to the phpBB team first, so the team gets no opportunity to release a patch.

And of course, there's a whole shitload of users that don't regularly update their phpBB software, rendering them open to all kinds of attacks that they need not be open for if only they had kept their software up-to-date.

That said, security *is* an issue and I try to write my code as secure as possible, but sometimes when I see code I wrote 5 years ago, I shiver.
